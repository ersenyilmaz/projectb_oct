﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp5_0
{
    //public partial class Form1 : Form
    //{

    public enum Dict
    {
        WORDS = 0,
        WORDS_REVERSE = 1,
        STRUCTURES = 2,
        STRUCTURES_REVERSE = 3,
        SENTENCES = 4,
        SENTENCES_REVERSE = 5,
        ARTIKELS = 6,
        NOTES = 7
    }


    public class Word : INotifyPropertyChanged
    {
        private static int idCounter = 0; // Static counter to generate unique IDs

        public int Id { get; private set; } // Unique ID for each Word instance

        public Dict DictType { get; set; }

        public string Name { get; set; }
        public string Content0 { get; set; }
        public string Content1 { get; set; }
        public string Content2 { get; set; }


        private string _nameScore; // { get; set; }
                                   //public string Meaning { get; set; }
        public string RelatedName { get; set; }

        private List<Skor> skors;

        public string Scores { get; set; }   
                                             
        private List<string> Labels;

        private List<string> links;

        public int Xscore { get; set; }
        public int WeightRandom { get; set; }
        public int Index { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public string NameScore
        {
            get => _nameScore;
            set
            {
                if (_nameScore != value)
                {
                    _nameScore = value;
                    OnPropertyChanged(nameof(NameScore));
                }
            }
        }

        // Public property to encapsulate the list of Skor instances
        public List<Skor> Skors
        {
            get { return skors; }
            set { skors = value; }
        }

        // Constructor to initialize the Word object
        public Word()
        {
            Id = GetNextId(); // Assign unique ID
            skors = new List<Skor>();
            links = new List<string>();
        }


        public Word(Dict dictType, string name, string content, string namescore, string relatedName, string scores, List<Skor> skors, int xscore, int weightRandom,
            List<string> labels, DateTime updated_at, DateTime created_at)   // IEnumerable<string> labels
        {
            Id = GetNextId(); // Assign unique ID
            DictType = dictType;
            Name = name;
            Content0 = content;

            _nameScore = namescore;
            Skors = skors;

            RelatedName = relatedName;
            Scores = scores;
            Xscore = xscore;
            WeightRandom = weightRandom;

            Labels = labels;  // this.Labels = new List<string>(labels);

            UpdatedAt = updated_at;
            CreatedAt = created_at;
        }

        //-----------------------------
        public Word(Dict dictType, string name, string content, string namescore, string relatedName, string scores, List<Skor> skors, int xscore, int weightRandom,
            List<string> labels)   // IEnumerable<string> labels
        {
            Id = GetNextId(); // Assign unique ID

            DictType = dictType;
            Name = name;
            Content0 = content;

            _nameScore = namescore;
            Skors = skors;

            RelatedName = relatedName;
            Scores = scores;
            Xscore = xscore;
            WeightRandom = weightRandom;

            Labels = labels;  // this.Labels = new List<string>(labels);

            DateTime now = DateTime.Now;
            CreatedAt = now;
            UpdatedAt = now;
        }

        //-----------------------------
        public Word(Dict dictType, string name, string content, string relatedName, List<string> labels)   
        {
            Id = GetNextId(); // Assign unique ID

            DictType = dictType;
            Name = name;
            Content0 = content;
            RelatedName = relatedName;

            Labels = labels;  // this.Labels = new List<string>(labels);


            string namescore = name;
            string score_ = "";

            var Sonuc = Common2.ScoreHsb(score_, namescore, dictType);
            Xscore = Sonuc.Item1;
            Scores = Sonuc.Item2;
            _nameScore = Sonuc.Item3;

            int rweight = Common1.CalcRandomWeight(Xscore, Scores);
                        
            WeightRandom = rweight;

            Skors = new List<Word.Skor>();

            DateTime now = DateTime.Now;
            CreatedAt = now;
            UpdatedAt = now;
        }

        //-----------------------------
        // Static method to get the next unique ID
        private static int GetNextId()
        {
            return ++idCounter; // Increment and return the static counter
        }
        //-----------------------------
        // DateTime now = DateTime.Now;
        // DateTime specificDate = new DateTime(2024, 7, 8, 12, 30, 0);
        // string formattedDate = now.ToString("dd-MM-yyyy HH:mm:ss");

        //-----------------------------
        // static void Main(string[] args)
        // {
        // Initialize instances of the Word class
        // Word word1 = new Word(Dict.Type1, "ExampleName1", "Score1");
        // Word word2 = new Word(Dict.Type2, "ExampleName2", "Score2");
        // Word word3 = new Word(Dict.Type3, "ExampleName3", "Score3");

        // Display properties of each Word instance
        // DisplayWordDetails(word1);
        // DisplayWordDetails(word2);
        // DisplayWordDetails(word3);
        // }

        // static void DisplayWordDetails(Word word)
        // {
        // Console.WriteLine($"DictType: {word.DictType}, Name: {word.Name}, NameScore: {word.NameScore}");
        // }


        //-----------------------------
        // Create instances of Ball with different colors and labels
        // Ball redBall = new Ball(Color.RED, new string[] { "Fast", "Bouncy" });
        // Ball blueBall = new Ball(Color.BLUE, new string[] { "Heavy", "Solid" });
        // Ball orangeBall = new Ball(Color.ORANGE, new string[] { "Light", "Soft" });


        //-----------------------
        // Add a label to the Label_list
        public void AddLabel(string label)
        {
            if (!Labels.Contains(label))
            {
                Labels.Add(label);
            }
        }

        // Remove a label from the list
        public void RemoveLabel(string label)
        {
            Labels.Remove(label);
        }

        // Clear all labels
        public void ClearLabels()
        {
            Labels.Clear();
        }

        // Get the labels
        public string[] GetLabels()
        {
            return Labels.ToArray();
        }

        // Get label by index
        public string GetLabelByIndex(int index)
        {
            if (Labels == null)
                return null;


            if (index >= 0 && index < Labels.Count)
            {
                return Labels[index];
            }
            return null;
        }

        // Method to merge labels from another Word instance
        public void MergeWords(Word other)
        {
            //for (int i = 0; i < 3; i++)
            {
                Content0 += string.Join("\n\n============\n\n", other.Content0);
                Content1 += string.Join("\n\n============\n\n", other.Content1);
                Content2 += string.Join("\n\n============\n\n", other.Content2);
            }

            foreach (var label in other.Labels)
            {
                AddLabel(label);
            }
        }


        public void ReadAllLabels()
        {
            string[] labelsArray = GetLabels();
            for (int i = 0; i < labelsArray.Length; i++)
            {
                Console.WriteLine("Label " + (i + 1) + ": " + labelsArray[i]);
            }
        }

        // Method to seek a string in all the labels
        public bool ContainsLabel(string searchString)
        {
            foreach (string label in Labels)
            {
                if (label.Contains(searchString))
                {
                    return true;
                }
            }
            return false;
        }

        // Create instances of Ball with different colors, labels, and content
        // Ball redBall = new Ball(Color.RED, new string[] { "Fast", "Bouncy" }, new string[] { "Red1", "Red2", "Red3" });

        // Modify labels
        // word.AddLabel("Shiny");
        // word.RemoveLabel("Fast");
        // Console.WriteLine("Red ball labels after modification: " + string.Join(", ", redBall.GetLabels()));

        // Clear labels
        // word.ClearLabels();

        // List<string> labels = new List<string> { "example", "test", "sample" };
        // Word word = new Word(labels);

        // bool found = word.ContainsLabel("test");  // returns true
        // bool notFound = word.ContainsLabel("absent");  // returns false

        // Read the first label
        // string firstLabel = word.GetLabelByIndex(0);


        //-------------------------
        /*
        public string[] Content
            {
                get { return content; }
                set
                {
                    if (value.Length != 3)
                    {
                        throw new ArgumentException("Content array must consist of exactly 3 strings.");
                    }
                    content = value;
                }
            }

            // Indexer for reading and writing individual content elements
            public string this[int index]
            {
                get
                {
                    if (index < 0 || index >= 3)
                    {
                        throw new ArgumentOutOfRangeException("Index must be between 0 and 2 (inclusive).");
                    }
                    return content[index];
                }
                set
                {
                    if (index < 0 || index >= 3)
                    {
                        throw new ArgumentOutOfRangeException("Index must be between 0 and 2 (inclusive).");
                    }
                    content[index] = value;
                }
            }  */

        // Word word = new Word();
        // Set the entire content array using the Content property
        // word.Content = new string[] { "Red", "Blue", "Orange" };

        // Get and display the entire content array
        //foreach (var item in word.Content)
        //{
        //    Console.WriteLine(item);
        //}

        // Set individual content elements using the indexer
        //word[0] = "NewRed";
        //word[1] = "NewBlue";
        //word[2] = "NewOrange";

        // Get and display individual content elements using the indexer        
        //for (int i = 0; i< 3; i++)
        //{
        //    Console.WriteLine(word[i]);
        //}

        // Update content
        // redBall.Content = new string[] { "NewRed1", "NewRed2", "NewRed3" };

        // Update specific content index
        // word.SetContentAtIndex(0, "NewRed1");
        // word.SetContentAtIndex(1, "NewRed2");
        // word.SetContentAtIndex(2, "NewRed3");
        // Console.WriteLine("Red ball content after update: " + string.Join(", ", redBall.Content));


        //----------------------
        // Public property to encapsulate the list of links
        public List<string> Links
        {
            get { return links; }
            set { links = value; }
        }

        // Constructor to initialize the Word object


        // Method to add a link to the collection
        public void AddLink(string link)
        {
            links.Add(link);
        }

        // Method to remove a link from the collection
        public void RemoveLink(string link)
        {
            links.Remove(link);
        }

        // Method to clear all links from the collection
        public void ClearLinks()
        {
            links.Clear();
        }

        // Create instances of PictureLink with different picture URLs
        // PictureLink picture1 = new PictureLink("https://example.com/picture1.jpg");
        // PictureLink picture2 = new PictureLink("https://example.com/picture2.jpg");

        // Access and print the picture links
        //Console.WriteLine("Picture 1 URL: " + picture1.Link);
        //Console.WriteLine("Picture 2 URL: " + picture2.Link);

        //  word.AddLink("https://example.com/picture1.jpg");

        // Update a picture link
        // picture1.Link = "https://example.com/updated_picture1.jpg";

        // foreach (var link in word.Links)
        //     Console.WriteLine(link)

        // word.RemoveLink("https://example.com/picture2.jpg");
        //  word.ClearLinks();
        //----------------------

        // Method to add a Skor to the collection
        public void AddSkor(int score, DateTime dateTime)
        {
            skors.Add(new Skor(score, dateTime));
        }

        // Method to update the last Skor in the collection
        public void UpdateLastSkor(int newScore, DateTime newDateTime)
        {
            if (skors.Count > 0)
            {
                Skor lastSkor = skors[skors.Count - 1];
                lastSkor.Score = newScore;
                lastSkor.DateTime = newDateTime;
            }
            else
            {
                skors.Add(new Skor(newScore, newDateTime));
            }
        }

        // Inner class Skor
        public class Skor
        {
            // Members of the inner class
            public int Score { get; set; }
            public DateTime DateTime { get; set; }

            // Constructor to initialize the Skor object
            public Skor(int score, DateTime dateTime)
            {
                Score = score;
                DateTime = dateTime;
            }

            public override string ToString()
            {
                return $"{Score},{DateTime:yyMMdd-HHmmss}";
            }
        }

        // Create an instance of Word
        //Word word = new Word();

        // Add Skor instances to the Word object
        //word.AddSkor(10, DateTime.Now);
        //word.AddSkor(20, DateTime.Now.AddHours(1));

        // Display all Skor details
        //Console.WriteLine("All Skor Details:");
        //foreach (var skor in word.Skors)
        //{
        //    Console.WriteLine($"Score = {skor.Score}, DateTime = {skor.DateTime}");
        // }
        
        /*
        // Placeholder for the ParseSkors method
        public static List<Word.Skor> ParseScores(string value)
        {
            // Implementation of the ParseSkors method
            return new List<Word.Skor>();
        }  */


        //-----------
        public override bool Equals(object obj)
        {
            if (obj is Word word)
            {
                return Id == word.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //=================================
        // Encode method
        /*
        public string Encode()
        {
            string encoded = $"name: {Name}\n" +
                             $"id: {Id}\n" +
                             $"dict_type: {(int)DictType}\n" +
                             $"created: {CreatedAt:dd.MM.yyyy}\n" +
                             $"updated: {(UpdatedAt != DateTime.MinValue ? UpdatedAt.ToString("dd.MM.yyyy") : "")}\n" +
                             $"namescore: {NameScore}\n" +
                             $"skors: {string.Join(" | ", Skors)}\n" +
                             $"rel_name: {RelatedName}\n" +
                             $"labels: {string.Join(", ", Labels)}\n" +
                             $"pics: {string.Join(", ", links)}\n" +
                             $"content_1: {Content0}\n\n" +
                             $"content_2: {Content1}\n\n" +
                             $"content_3: {Content2}\n";

            return encoded;
        }  */
        // Encode method
        public string Encode()
        {
            string encoded = $"name: {Name ?? ""}\n" +
                             $"id: {Id}\n" +
                             $"dict_type: {(int)DictType}\n" +
                             $"created: {CreatedAt:dd.MM.yyyy}\n" +
                             $"updated: {(UpdatedAt != DateTime.MinValue ? UpdatedAt.ToString("dd.MM.yyyy") : "")}\n" +
                           //$"namescore: {NameScore ?? ""}\n" +
                             $"skors: {string.Join(" | ", Skors ?? new List<Skor>())}\n" +
                             $"rel_name: {RelatedName ?? ""}\n" +
                             $"labels: {string.Join(", ", Labels ?? new List<string>())}\n" +
                             $"pics: {string.Join(", ", links ?? new List<string>())}\n" +
                             $"scores: {Scores ?? ""}\n" +
                             $"content_1: {Content0 ?? ""}\n" +
                             $"content_2: {Content1 ?? ""}\n" +
                             $"content_3: {Content2 ?? ""}\n";

            return encoded;
        }


        // Decode method
        public static Word Decode(string encodedString)
        {
            var lines = encodedString.Split(new[] { '\n' }, StringSplitOptions.None);
            string name = string.Empty, nameScore = string.Empty, relatedName = string.Empty, scores = string.Empty;
            string content0 = string.Empty, content1 = string.Empty, content2 = string.Empty;
            int id = 0;
            Dict dictType = Dict.WORDS;
            DateTime createdAt = DateTime.MinValue, updatedAt = DateTime.MinValue;
            List<Word.Skor> skors = new List<Word.Skor>();
            List<string> labels = new List<string>();
            List<string> links = new List<string>();

            bool readingContent0 = false, readingContent1 = false, readingContent2 = false;

            foreach (var line in lines)
            {
                if (readingContent0)
                {
                    if (line.StartsWith("content_2:"))
                    {
                        readingContent0 = false;
                        readingContent1 = true;
                        content1 = line.Substring("content_2:".Length).Trim();
                        continue;
                    }
                    content0 += "\n" + line;
                    continue;
                }

                if (readingContent1)
                {
                    if (line.StartsWith("content_3:"))
                    {
                        readingContent1 = false;
                        readingContent2 = true;
                        content2 = line.Substring("content_3:".Length).Trim();
                        continue;
                    }
                    content1 += "\n" + line;
                    continue;
                }

                if (readingContent2)
                {
                    content2 += "\n" + line;
                    continue;
                }

                if (!line.Contains(": ")) continue; // Skip lines without ": "

                var parts = line.Split(new[] { ": " }, 2, StringSplitOptions.None);
                if (parts.Length < 2) continue; // Skip lines that don't have a valid key-value pair

                var key = parts[0].Trim();
                var value = parts[1].Trim();

                switch (key)
                {
                    case "name":
                        name = value;
                        break;
                    case "id":
                        int.TryParse(value, out id);
                        break;
                    case "dict_type":
                        if (int.TryParse(value, out var dictTypeInt))
                            dictType = (Dict)dictTypeInt;
                        break;
                    case "created":
                        DateTime.TryParse(value, out createdAt);
                        break;
                    case "updated":
                        if (!string.IsNullOrEmpty(value))
                            DateTime.TryParse(value, out updatedAt);
                        break;
                    //case "namescore":
                    //    nameScore = value;
                    //    break;
                    case "skors":
                        skors = ParseSkors(value);
                        break;
                    case "scores":
                        scores = value;
                        break;
                    case "rel_name":
                        relatedName = value;
                        break;
                    case "labels":
                        labels = new List<string>(value.Split(new[] { ", " }, StringSplitOptions.None));
                        break;
                    case "pics":
                        links = new List<string>(value.Split(new[] { ", " }, StringSplitOptions.None));
                        break;
                    case "content_1":
                        readingContent0 = true;
                        content0 = value;
                        break;
                    case "content_2":
                        readingContent1 = true;
                        content1 = value;
                        break;
                    case "content_3":
                        readingContent2 = true;
                        content2 = value;
                        break;
                }
            }

            // Trim leading newlines from the content strings
            content0 = content0.TrimStart('\n');
            content1 = content1.TrimStart('\n');
            content2 = content2.TrimStart('\n');

             var word = new Word
            {
                Name = name,
                Id = id,
                DictType = dictType,
                CreatedAt = createdAt,
                UpdatedAt = updatedAt,
                NameScore = nameScore,
                Skors = skors,
                Scores = scores,
                RelatedName = relatedName,
                Labels = labels,
                Links = links,
                Content0 = content0,
                Content1 = content1,
                Content2 = content2
            };

            //word.Skors.AddRange(ParseScores(scores)); // Use the instance to call AddRange

            return word;
        }

        public static List<Word.Skor> ParseSkors(string skorString)
        {
            var skors = new List<Word.Skor>();
            if (string.IsNullOrEmpty(skorString)) return skors;

            var skorParts = skorString.Split(new[] { " | " }, StringSplitOptions.None);
            foreach (var part in skorParts)
            {
                var parts = part.Split(',');
                if (parts.Length == 2)
                {
                    if (int.TryParse(parts[0], out var score) &&
                        DateTime.TryParseExact(parts[1], "yyMMdd-HHmmss", null, System.Globalization.DateTimeStyles.None, out var dateTime))
                    {
                        skors.Add(new Word.Skor(score, dateTime));
                    }
                }
            }
            return skors;
        }

        public static List<Skor> ParseScores(string scores)
        {
            List<Skor> skors = new List<Skor>();
            foreach (var ch in scores)
            {
                if (char.IsDigit(ch) && ch >= '0' && ch <= '5')
                {
                    skors.Add(new Skor(int.Parse(ch.ToString()), Consts.InitialDate));
                }
                else if (ch == '-') // Handle empty score
                {
                    skors.Add(new Skor(10, Consts.InitialDate));
                }
            }
            return skors;
        }

    }





    //==================================
    //==================================

    public static class WR
    {
        public static List<Word> listWords = new List<Word>();   //[6];

        // Public static array of BindingList<Word> for each Dict type
        public static BindingList<Word>[] DictBindingLists = new BindingList<Word>[Enum.GetValues(typeof(Dict)).Length];

        static WR()
        {
            // Initialize each BindingList in the array
            for (int i = 0; i < DictBindingLists.Length; i++)
            {
                DictBindingLists[i] = new BindingList<Word>();
            }
        }

        // list_words yerine listWords[x] var.
        public static BindingList<Word> list_words_orig = new BindingList<Word>();

        public static List<Word> list_words_orig_tmp = new List<Word>();

        public static BindingList<Word> list_words_search = new BindingList<Word>();
        public static List<Word> list_words_search_tmp = new List<Word>();

        public static BindingList<Word> list_words_search2 = new BindingList<Word>();

        public static BindingList<Word> list_words_etiket = new BindingList<Word>();
        public static List<Word> list_words_etiket_tmp = new List<Word>();

        public static BindingList<Word> list_words_history = new BindingList<Word>();

        public static void AddWordV1toV2(Dict dictType, string name, string content, string nameScore, string relatedName_, string scores_, int xscore, int rweight, List<string> labels, DateTime updated_at_, DateTime created_at_)
        {
            List<Word.Skor> skors_ = Word.ParseScores(scores_);

            Word newWord = new Word(dictType, name, content, nameScore, relatedName_, scores_, skors_, xscore, rweight, labels, updated_at_, created_at_);

            // Check if a Word with the same name already exists in the list
            Word existingWord = listWords.Find(w => w.Name == name);
            if (existingWord != null)
            {
                // Merge the labels if a Word with the same name is found
                existingWord.MergeWords(newWord);
            }
            else
            {
                // Add the new Word to the list if no match is found
                listWords.Add(newWord);
                DictBindingLists[(int)dictType].Add(newWord);
            }
        }

        public static void AddWord(Word newWord)
        {
            //Word newWord = new Word(dictType, name, content, nameScore, relatedName_, scores_, xscore, labels, updated_at_, created_at_);
            string name = newWord.Name;
            Dict dictType = newWord.DictType;

            // Check if a Word with the same name already exists in the list
            Word existingWord = listWords.Find(w => w.Name == name && w.DictType == dictType);
            if (existingWord != null)
            {
                // Merge the labels if a Word with the same name is found
                existingWord.MergeWords(newWord);
            }
            else
            {
                // Add the new Word to the list if no match is found
                listWords.Add(newWord);
                DictBindingLists[(int)dictType].Add(newWord);
            }
        }

        //----------------------------------------------
        public static void SaveWordsToFile(string filePath, string title)
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine(title);

                if (WR.listWords.Count() == 0)
                {
                    writer.WriteLine("\n-\n");
                }
                else
                {
                    foreach (var word in listWords)
                    {
                        writer.WriteLine("______");   // Separator between words
                        writer.WriteLine(word.Encode());
                    }
                }
            }
        }
        // Save the words to a text file
        // WR.SaveWordsToFile("words.txt");
        //============================================
        /*
        public static void LoadWordsFromFile(string filePath)
        {   
            if (!File.Exists(filePath))
            {
                string title = "Error 102";
                string message = "Dictionary txt was not found! Please find and show it from File - Open Dictionary Menu!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                return;
            }

            string[] lines = File.ReadAllLines(filePath);


            var wordData = new List<string>();

            foreach (var line in lines)
            {
                if (line == "______")
                {
                    if (wordData.Count > 0)
                    {
                        var encodedString = string.Join("\n", wordData);
                        listWords.Add(Word.Decode(encodedString));
                        wordData.Clear();
                    }
                }
                else
                {
                    wordData.Add(line);
                }
            }

            if (wordData.Count > 0)
            {
                var encodedString = string.Join("\n", wordData);
                listWords.Add(Word.Decode(encodedString));
            }
        }   */

        // WR.WordList.Clear();  // Clear the current WordList
        // WR.LoadWordsFromFile("words.txt"); // Load the words from the text file


        //============================================
        //============================================
        public static Word GetWordById(int id)
        {
            return listWords.Find(w => w.Id == id);
        }

        // Find a Word by its unique ID
        //int searchId = 1;
        // Word wordById = WordRepository.GetWordById(searchId);
        // if (wordById != null)
        //    Console.WriteLine($"Found Word with ID {searchId}: Name: {wordById.Name}, Name Score: {wordById.NameScore}");
        //else
        //    Console.WriteLine($"No Word found with ID {searchId}");        

        public static int FindWordIndex(Dict dictType, Word word)
        {
            return WR.DictBindingLists[(int)dictType].IndexOf(word);
        }

        public static int FindWordIndex2(Dict dictType, Word word)
        {
            return DictBindingLists[(int)dictType]
                .Select((w, index) => new { Word = w, Index = index })
                .FirstOrDefault(x => x.Word.Name == word.Name)?.Index ?? -1;
        }

        // Initially display the first BindingList
        // DisplayBindingList(Dict.WORDS);

        // Example of finding the index of a Word
        // Word wordToFind = WordRepository.WordList[0]; // Example: finding the first word
        // int index = WordRepository.FindWordIndex(Dict.WORDS, wordToFind);

        // Method to check if a Word with a specific name exists in a BindingList
        public static bool WordExistsInBindingList(Dict dictType, string name)
        {
            return WR.DictBindingLists[(int)dictType].Any(w => w.Name == name && w.DictType == dictType);
        }

        // Check if a Word with a specific name exists in a BindingList
        // bool exists = WordRepository.WordExistsInBindingList(Dict.WORDS, "FirstWord");

        //=======================================
        //=======================================

        public static void SortDictBindingListsAlphabetically()
        {
            for (int i = 0; i < DictBindingLists.Length; i++)
            {
                var sortedList = DictBindingLists[i].OrderBy(word => word.Name).ToList();
                DictBindingLists[i] = new BindingList<Word>(sortedList);
            }
        }

        public static void SortDictBindingListsByIndex()
        {
            for (int i = 0; i < DictBindingLists.Length; i++)
            {
                var sortedList = DictBindingLists[i].OrderBy(word => listWords.IndexOf(word)).ToList();
                DictBindingLists[i] = new BindingList<Word>(sortedList);
            }
        }






    }

    //==================================
    //==================================
    /*
    
    public class NameNameScoreIndex
    {
        private string myName;
        private string myNameScore;
        private int myXscore;
        private int myIndex;
        //public int[] scores;
        //public DateTime[] dates;

        public NameNameScoreIndex(string name, string namescore, int xscore, int index)
        {
            this.myName = name;
            this.myNameScore = namescore;
            this.myXscore = xscore;
            this.myIndex = index;
        }

        public string Name
        {
            get { return myName; }
            set { myName = value; }
        }

        public string NameScore
        {
            get { return myNameScore; }
            set { myNameScore = value; }
        }

        public int Xscore
        {
            get { return myXscore; }
            set { myXscore = value; }
        }

        public int Index
        {
            get { return myIndex; }
            set { myIndex = value; }
        }
    }   */




    public class Textc
    {
        public string name;
        public int ind;
        public int len;

        public Textc(string name, int ind, int len)
        {
            this.name = name;
            this.ind = ind;
            this.len = len;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Ind
        {
            get { return ind; }
            set { ind = value; }
        }

        public int Len
        {
            get { return len; }
            set { len = value; }
        }
    }


    public enum Mod
    {
        Dictionary = 0,
        Test = 1,
        Random_Test = 2,
        RandomW_Test = 3
    }

    public enum Sozluk
    {
        ENGLISH = 0,
        DEUTSCH = 1,
        C = 2,
        CPP = 3
    }





    public enum Sort
    {
        Database = 0,
        Alphabet = 1,
        Reverse_Alphabet = 2,
        Score = 3,
        Random_Score = 4,
        Random_Score_Weighted = 5,
        Random = 6,
        History = 7,
        Label = 8
    }



    public static class DICTI
    {
        public const int WORDS = 0;
        public const int WORDS_REVERSE = 1;
        public const int STRUCTURES = 2;
        public const int STRUCTURES_REVERSE = 3;
        public const int SENTENCES = 2;
        public const int SENTENCES_REVERSE = 3;
        public const int ARTIKELS = 4;
        public const int NOTES = 5;
    }
    //}
}
