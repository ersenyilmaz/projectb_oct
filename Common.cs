﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.InteropServices;


namespace WindowsFormsApp5_0
{

    public static class Common1
    {
        //===========================
        public static int CalcRandomWeight(int puan, string score)
        {
            int lenScore = score.Length;
            if (puan > 100 || puan < 0)
                puan = 0;

            double lSc = 1 + 0.2 * lenScore;
            puan = 100 - puan;

            double sonuc = lSc * puan * puan;

            return (int)sonuc;
        }

        //===========================
        public static bool AddDeyim(string deyim)
        {
            char[] charSplits2 = { ':', '|' };
            string[] dym = deyim.Split(charSplits2, StringSplitOptions.RemoveEmptyEntries);

            for (int w = 0; w < dym.Count(); w++)
            {
                dym[w] = dym[w].Trim();
            }

            string deyim_ad = "";
            string deyim_aciklama = "";
            string deyim_word_name = "";
            string deyim_name_score;
            string deyim_str_score = "";

            int deyim_xscore;

            if (dym.Count() > 0 && dym[0].Length > 0)
                { deyim_ad = dym[0]; }  // .ToLower()

            if (deyim_ad.Length < 3)
                return false;

            if (dym.Count() > 1 && dym[1].Length > 0)
                { deyim_aciklama = dym[1]; }

            if (dym.Count() > 2 && dym[2].Length > 0)
                { deyim_word_name = dym[2]; }

            if (dym.Count() > 3 && dym[3].Length > 0)
            { deyim_str_score = dym[3]; }

            //deyim_name_score = deyim_word_name;

            var Sonuc2 = Common2.ScoreHsb(deyim_str_score, deyim_ad, Dict.WORDS_REVERSE);
            deyim_xscore = Sonuc2.Item1;
            deyim_str_score = Sonuc2.Item2;
            deyim_name_score = Sonuc2.Item3;

            int rweight = CalcRandomWeight(deyim_xscore, deyim_str_score);

            //if (WR.DictBindingLists[(int)Dict.STRUCTURES] != null)
            {   /* 4NOW Ver7.2
                int indeks = WR.DictBindingLists[(int)Dict.WORDS_REVERSE].FindIndex(r => r.Name == deyim_ad);
                
                if (indeks != -1)
                    return false;
                */
            }

            //int ind = WR.DictBindingLists[(int)Dict.WORDS_REVERSE].Count();
            //DateTime initialDate = new DateTime(2023, 1, 1, 00, 00, 00);
            WR.AddWordV1toV2(Dict.WORDS_REVERSE, deyim_ad, deyim_aciklama, deyim_name_score, deyim_word_name, deyim_str_score, deyim_xscore, rweight, null, Consts.InitialDate, Consts.InitialDate);
            //listWords.Add(new Word(deyim_ad, deyim_name_score, deyim_aciklama, deyim_word_name, deyim_str_score, deyim_xscore, "", 0, ind ));

            return true;
        }
        //===================================
        public static bool AddWordRaw(string word_raw)
        {
            //char[] charSplits = { ' ', '\n' };
            //string[] split_txt = word_raw.Split(charSplits);
            // //string[] split_txt2 = word_raw.Split('\n');
            //char[] charSplits = { ' ', '\t' };
            word_raw = word_raw.TrimStart(' ', '\t', '\n');

            if (!string.IsNullOrEmpty(word_raw) && word_raw.Length >= 3 )               // word_raw.Length > 0)
            {
                string txt1;
                if (word_raw.IndexOf('\n') > 0)
                {
                    txt1 = word_raw.Substring(0, word_raw.IndexOf('\n') + 1);   // first line

                    if (!string.IsNullOrEmpty(txt1) && Char.IsLetter(txt1[0]))
                    {
                        int z, w;
                        char y;
                        for (z = 0; z < txt1.Length; z++)
                        {
                            y = txt1[z];
                            if (Char.IsLetter(y) == false && y != ' ')
                            {
                                break;
                            }
                        }
                        if (z == txt1.Length) { z--; }
                        string name_ = txt1.Substring(0, z).Trim();     //.ToLower();
                        string score_ = "";
                        string etiket = "";

                        int v = txt1.IndexOf('#');

                        if (Char.IsDigit(txt1[z]) != false)
                        {
                            w = z;
                            do
                            {
                                z++;
                            }
                            while (Char.IsDigit(txt1[z]) != false && z < txt1.Length);
                            if (z == txt1.Length) { z--; }
                            score_ = txt1.Substring(w, z - w).Trim();
                        }

                        if (v > 0)
                        {
                            v++;
                            etiket = txt1.Substring(v, txt1.Length - v).Trim();
                        }

                        string meaning_ = word_raw.Substring(word_raw.IndexOf('\n') + 1);
                        meaning_ = meaning_.TrimEnd('_');

                        if( AddWord6(name_, meaning_, score_, etiket) )
                            return true;
                        else
                            return false;

                        //string[] str_temp;
                        //str_temp = word_raw.Split(charSplits);
                        //list_words[i].Name = word_raw.Substring(0, word_raw.IndexOf(charSplits));
                        //list_words[i].Name = str_temp[0];
                        //list_words[i].Meaning = word_raw;
                    }
                    
                }
            }
            return false;
        }
        //===================================
        //===================================
        public static bool AddWord6(string name_, string meaning_, string score_, string etiket )
        {
            Dict dictx = Dict.WORDS;
            if (name_ == "EKLE NAMEN" || name_ == "EKLE VERBEN" || name_ == "GENEL EKLENECEK" || name_ == "EKLE SÄTZE")
            {
                dictx = Dict.NOTES;
            }
            else if (name_ == "EKLE ANDEREN" || name_ == "GENEL EKLENECEK" || name_ == "BEISPIEL SÄTZE")
            {
                dictx = Dict.STRUCTURES;
            }
            else if (name_ == "ARTIKEL ELEKTRONIK" || name_ == "ARTIKEL ARBEIT" || name_ == "ARTIKEL WICHTIGE" || name_ == "ARTIKEL")
            {
                dictx = Dict.ARTIKELS;
            }


            if (!meaning_.EndsWith("\n"))
            {
                meaning_ += "\n";
            }

            string namescore = name_;

            var Sonuc = Common2.ScoreHsb(score_, namescore, dictx);
            int xsc = Sonuc.Item1;
            score_ = Sonuc.Item2;
            namescore = Sonuc.Item3;

            int rweight = CalcRandomWeight(xsc, score_);

            List<string> labels = new List<string> { etiket };

            WR.AddWordV1toV2(dictx, name_, meaning_, namescore, null, score_, xsc, rweight, labels, Consts.InitialDate, Consts.InitialDate);

            return true;        

        }
       
    }
    //======================================
    
    public static class Common
    {
            public static Font rTextBoxFont;

            //===============================
            public static int InitEditor(RichTextBox rTB)
            {
                //rTB.AppendText("Hi");
                //rTB.SuspendPainting();

                //rTB.CanUndo;
                //rTB.ResumeLayout();
                rTB.Invalidate();
                int _curLoc = rTB.SelectionStart;

                rTB.SelectAll();
                rTB.SelectionFont = Common.rTextBoxFont;
                rTB.SelectionColor = Color.Black;
                //rTB.SelectionBackColor = Color.White;
                rTB.DeselectAll();

                return _curLoc;
            }

            //===============================
            public static List<Textc> WordPartitioner(RichTextBox rTB, int beg, int end)
            {
                List<Textc> wordz = new List<Textc>();

                string Xstring = rTB.Text.ToString();

                int wlen;
                int wind = 0;
                bool wstarted = false;
                wordz.Clear();

                int Endx;
                if (beg == 0 && end == 0)
                {
                    Endx = Xstring.Length;
                }
                else
                {
                    Endx = end;
                    if (end < Xstring.Length)
                        ++Endx;                    
                }

                for (int ind2 = beg; ind2 < Endx; ind2++)
                {
                    if (wstarted == false)
                    {
                        if (Char.IsLetter(Xstring[ind2]) != false)
                        {
                            wstarted = true;
                            wind = ind2;
                        }
                    }
                    else // word wstarted
                    {
                        if (Char.IsLetter(Xstring[ind2]) == false)
                        {
                            wstarted = false;
                            wlen = ind2 - wind;
                            if (wlen > 0)
                            {
                                wordz.Add(new Textc(Xstring.Substring(wind, wlen), wind, wlen));
                            }
                        }
                    }

                }

                //ShowTextc(wordz);
                return wordz;
            }
            //==========================
            public static void LineMarker(RichTextBox rTB, int beg, int end)
            {
                string str = rTB.Text.ToString();
                string Xstring = str;
                
                int lStart = beg;
                int lEnd;
                bool lstarted = true;

            //int Endx = Xstring.Length;

            //if (end >= Xstring.Length)
            //    return;

                if (end < Xstring.Length && Xstring[end] == '\n' )
                {
                        end++;
                }  

                //if(end == Xstring.Length)
                {
                    Xstring += "\n";
                }

                int Endx = end;


                for (int ind = beg; ind < Endx; ind++)
                {
                    if (lstarted == false)
                    {
                        if (Xstring[ind] != '\n')
                        {
                            lstarted = true;
                            lStart = ind;
                        }
                    }
                    else // line lstarted
                    {
                        if (ind == Xstring.Length-1 || Xstring[ind] == '\n')
                        {
                            lstarted = false;

                            if (ind == Xstring.Length - 1)
                                ++ind;

                            int llen = ind - lStart;
                            if (llen > 2)
                            {
                                lEnd = ind;
                                
                                string tempStr = Xstring.Substring(lStart, llen).TrimStart(' ', '\t', '\n', '\r');

                                if (!string.IsNullOrEmpty(tempStr))
                                {
                                    if ((tempStr[0] == '-') ||
                                        (tempStr[0] == '"' && tempStr[tempStr.Length - 1] == '"'))
                                    {
                                        rTB.Select(lStart, lEnd - lStart);
                                        rTB.SelectionColor = Color.Blue;
                                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Italic);

                                        if(tempStr.Contains('<'))
                                        {
                                            int indox = lStart + tempStr.IndexOf('<');
                                            rTB.Select(indox, lEnd - indox);
                                            rTB.SelectionColor = Color.DimGray;   //.DarkGray;          // .Gray;
                                            rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Italic);
                                        }
                                    }
                                    else if (tempStr.Contains(':') && tempStr[0] == '>')
                                    {
                                        rTB.Select(lStart, lEnd - lStart);
                                        rTB.SelectionColor = Color.Purple;
                                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style);  // | FontStyle.Italic
                                    }
                                    else if (tempStr[0] == '>')
                                    {
                                        rTB.Select(lStart, lEnd - lStart);
                                        rTB.SelectionColor = Color.DarkViolet;
                                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style); // | FontStyle.Italic
                                    }
                                    else if (tempStr[0] == '<')
                                    {
                                        rTB.Select(lStart, lEnd - lStart);
                                        rTB.SelectionColor = Color.DimGray;  // .DarkMagenta;      // .DarkGray;   // .Gray;
                                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style); // | FontStyle.Italic
                                    }
                                    else if (tempStr.Contains(':'))
                                    {
                                        rTB.Select(lStart, lEnd - lStart);
                                        rTB.SelectionColor = Color.DarkBlue;
                                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style ); // | FontStyle.Italic
                                }
                                    else
                                    {
                                        rTB.Select(lStart, lEnd - lStart);
                                        rTB.SelectionColor = Color.Black;
                                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style);
                                    }
                            }
                            }
                        }

                    }
                }

            }
        //===============================

        public static string WordNameMarkCommonDE(string namex_, List<String> listDE)
        {
            string testStr = "\\b";
            if (listDE.Count() == 0)
            {                
                testStr += namex_;
            }
            else
            {
                foreach (string strx in listDE)
                {
                    if (string.IsNullOrEmpty(strx)) continue;
                    if (testStr != "\\b")
                    {
                        testStr += '|' + "\\b";
                    }

                    testStr += strx + "\\b";

                }
                // testStr += "|" + "\\b" + strY;
                //testStr += "|" + "\\b" + irregularVerbs.En[verb_ind, v];
            }
            return testStr;
        }


        //===============================
        public static string WordNameMarkCommon(string namex_)
        {
            int verb_ind;
            string name1 = namex_;
            int verb_indMax = irregularVerbs.En.GetLength(0);

            string testStr = "";

            if (!namex_.Contains(' '))  // sözcük tek kelime ise 
            { 
                string str_verb;
                for (verb_ind = 0; verb_ind < verb_indMax; verb_ind++)
                {
                    str_verb = irregularVerbs.En[verb_ind, 0];
                    if (name1 == irregularVerbs.En[verb_ind, 0] || str_verb.StartsWith(name1, StringComparison.CurrentCultureIgnoreCase))
                        break;
                }

                testStr = "\\b" + namex_;

                if (verb_ind != verb_indMax)
                {
                    //testStr += "|" + "\\b" + irregularVerbs.En[verb_ind, 0];                    
                    for (int v = 1; v < 4; v++)
                    {
                        if (irregularVerbs.En[verb_ind, v] == "")
                            break;

                        testStr += "|" + "\\b" + irregularVerbs.En[verb_ind, v];
                    }
                }

                if (!string.IsNullOrEmpty(namex_))
                {
                    if (namex_[namex_.Length - 1] == 'y' || namex_[namex_.Length - 1] == 'Y'
                        || namex_[namex_.Length - 1] == 'e' || namex_[namex_.Length - 1] == 'E')
                    {
                        char[] strAsChars = namex_.ToCharArray();

                        strAsChars[namex_.Length - 1] = 'i';
                        string strY = new string(strAsChars);
                        testStr += "|" + "\\b" + strY;
                    }
                }

            }

            return testStr;
        }

        //===============================
        public static void WordNameMarkerDEU(RichTextBox rTB, string str, string namex_, List<String> listDE, int beg, int end)
        {
            if (namex_ == null) return;

            if (!namex_.Contains(' '))  // sözcük tek kelime ise 
            {
                string testStr = WordNameMarkCommonDE(namex_, listDE);
                MakeBoldOneWord(rTB, str, testStr, beg, end);                 // 0, str.Length);
            }
            else  // sözcük birden fazla kelimeden oluşuyorsa...
            {
                // sonra devam et!
                
            }

        }

        //===============================
        public static void WordNameMarker2(RichTextBox rTB, string str, string namex_, List<Textc> wordx, int beg, int end)
        {
            if (Form1.sozluk == Sozluk.DEUTSCH)
            {
                int indx = find_next_newline(str, 0);
                if(indx == 0) { indx = 1; }
                string strx = str.Substring(0, indx - 1);

                int xcount = 0;
                foreach (char ch in strx)
                {
                    if (ch == ';') ++xcount;
                }

                if ( xcount < 2)
                {
                    onek = "";
                    string testStr = WordNameMarkCommonDE(namex_, listDE);
                    MakeBoldOneWord(rTB, str, testStr, 0, 0);
                }
                else
                {
                    listDE = FindTokenWords(str, namex_);  // IndexOf
                    WordNameMarkerDEU(rTB, str, namex_, listDE, beg, end);
                }
                
            }
            else
            {
                if (!namex_.Contains(' '))  // sözcük tek kelime ise 
                {
                    string testStr = WordNameMarkCommon(namex_);

                    MakeBoldOneWord(rTB, str, testStr, beg, end);
                }
                else  // sözcük birden fazla kelimeden oluşuyorsa...
                {
                    // sonra devam et!

                }
            }
            
            }

            //===============================
            public static void WordNameMarker(RichTextBox rTB, string str, string namex_, List<Textc> wordx)
            {
            int beg = 0;
            int end = str.Length - 1;
            if (Form1.sozluk == Sozluk.DEUTSCH)
            {
                int indx = find_next_newline(str, 1);
                string strx = str.Substring(0, indx );

                int xcount = 0;
                foreach (char ch in strx)
                {
                    if (ch == ';') ++xcount;
                }

                if (xcount < 2)
                {
                    listDE.Clear();
                    onek = "";
                    string testStr = WordNameMarkCommonDE(namex_, listDE);
                    MakeBoldOneWord(rTB, str, testStr, 0, 0);
                }
                else
                {
                    listDE = FindTokenWords(str, namex_);  // IndexOf
                    WordNameMarkerDEU(rTB, str, namex_, listDE, beg, end);
                }

                /*rTB.Text += "\n\n" + "+++" + '\n';  // 4SIM
                foreach (string strx in listDE)  // 4SIM
                {
                    if (string.IsNullOrEmpty(strx)) continue;
                    rTB.Text += strx + '\n';
                } */

            }
            else
            {
                string[] str_sp2;
                if(namex_ == null)
                {
                    return;
                }
                else if (!namex_.Contains(' '))  // sözcük tek kelime ise 
                {
                    string testStr = WordNameMarkCommon(namex_);
                    MakeBoldOneWord(rTB, str, testStr, 0, 0);
                }
                else  // sözcük birden fazla kelimeden oluşuyorsa...
                {
                    string name1 = namex_;
                    int verb_ind;
                    int verb_indMax = irregularVerbs.En.GetLength(0);
                    char[] charSpl = { ' ' };
                    str_sp2 = namex_.Split(charSpl, StringSplitOptions.RemoveEmptyEntries); //);, 
                    name1 = str_sp2[0];

                    if (!String.IsNullOrEmpty(name1) && !Char.IsLetter(name1[0]))
                    {
                        name1 = name1.Substring(1);
                    }

                    int kelime_sayisi = str_sp2.Count();

                    for (verb_ind = 0; verb_ind < verb_indMax; verb_ind++)
                    {
                        if (name1 == irregularVerbs.En[verb_ind, 0] || name1.StartsWith(irregularVerbs.En[verb_ind, 0], StringComparison.CurrentCultureIgnoreCase))
                            break;
                    }

                    string testStr = "\\b" + name1;

                    if (verb_ind != verb_indMax)
                    {
                        //testStr += "|" + "\\b" + irregularVerbs.En[verb_ind, 0];                    
                        for (int v = 1; v < 4; v++)
                        {
                            if (irregularVerbs.En[verb_ind, v] == "")
                                break;

                            testStr += "|" + "\\b" + irregularVerbs.En[verb_ind, v];
                        }
                    }

                    if (name1[name1.Length - 1] == 'y' || name1[name1.Length - 1] == 'Y'
                     || name1[name1.Length - 1] == 'e' || name1[name1.Length - 1] == 'E')
                    {
                        char[] strAsChars = name1.ToCharArray();

                        strAsChars[name1.Length - 1] = 'i';
                        string strY = new string(strAsChars);
                        testStr += "|" + "\\b" + strY;
                    }

                    string bilesik = name1.Trim();

                    if (kelime_sayisi > 1)
                        bilesik += str_sp2[1].Trim();

                    //if (bilesik[0] == '(')
                    //    bilesik = bilesik.Substring(1, bilesik.Length);

                    //if (bilesik[bilesik.Length - 1] == ')')
                    //    bilesik = bilesik.Substring(0, bilesik.Length - 2);

                    testStr += "|" + "\\b" + bilesik;

                    testStr = testStr.Replace("(", "");
                    testStr = testStr.Replace(")", "");

                    MakeBoldOneWord(rTB, str, testStr, 0, 0);

                    var rx = new Regex(testStr, RegexOptions.Compiled | RegexOptions.IgnoreCase);  // tam uyanlar!
                    Match match = rx.Match(str);
                    while (match.Success)
                    {
                        int ind_ = match.Index;
                        //GetNextWordIndex(str, ind_ );
                        bool islem_basarisiz = false;

                        int last_ind = 0;
                        int ind_in_wordx = wordx.FindIndex(r => r.ind == ind_);
                        if (ind_in_wordx != -1)
                        {
                            int indK = ind_in_wordx;

                            if (kelime_sayisi == 2)
                            {
                                int k;
                                for (k = 0; k < 4; k++)
                                {
                                    if (++indK >= wordx.Count)
                                        break;

                                    if (wordx[indK].name == str_sp2[1])
                                        break;
                                }

                                if (indK == wordx.Count)
                                    break;

                                if (k == 4)
                                {
                                    islem_basarisiz = true;
                                }

                                last_ind = wordx[indK].ind + wordx[indK].len;

                            }
                            else  // kelime sayısı 3
                            {
                                int k;
                                for (k = 0; k < 4; k++)
                                {
                                    if (++indK >= wordx.Count)
                                        break;

                                    if (wordx[indK].name == str_sp2[1])
                                        break;
                                }

                                if (indK == wordx.Count)
                                    break;

                                if (k != 4)
                                {
                                    int n;
                                    for (n = 0; n < 4; n++)
                                    {
                                        if (++indK >= wordx.Count)
                                            break;

                                        if (wordx[indK].name == str_sp2[2])
                                            break;
                                    }

                                    if (indK == wordx.Count)
                                        break;

                                    if (n == 4)
                                    {
                                        islem_basarisiz = true;
                                    }

                                }
                                else
                                {
                                    islem_basarisiz = true;
                                }

                                last_ind = wordx[indK].ind + wordx[indK].len;

                            }

                            if (islem_basarisiz == false && last_ind > match.Index)
                            {
                                rTB.Select(match.Index, last_ind - match.Index);
                                if (rTB.SelectionFont.Italic)
                                    rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold | FontStyle.Italic);
                                else
                                    rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold);
                            }
                            match = match.NextMatch();
                        }
                    }
                }
            }

            }
            //================================
            public static void MakeBoldOneWord(RichTextBox rTB, string str, string testStr, int beg, int end)
            {
            if (string.IsNullOrEmpty(testStr))
                return;

            testStr = testStr.Replace("(", "");
                testStr = testStr.Replace(")", "");

            string substrx;

            if (beg == 0 && end == 0)  // tüm str metni demek!
            {
                substrx = str;
            }
            else if (end > str.Length)
            {
                return;
            }
            else
            {
                substrx = str.Substring(beg, end - beg);
            }

            

                var rx = new Regex(testStr, RegexOptions.Compiled | RegexOptions.IgnoreCase);  // tam uyanlar!
                Match match = rx.Match(substrx);
                while (match.Success)
                {
                    int ind_ = match.Index;
                    while (ind_ < substrx.Length)
                    {
                        if (substrx[ind_] == '\r' || substrx[ind_] == '\n' || substrx[ind_] == ' '  || substrx[ind_] == ',' 
                         || substrx[ind_] == '.'  || substrx[ind_] == ':'  || substrx[ind_] == '\t' || substrx[ind_] == '-'
                         || substrx[ind_] == '?' || substrx[ind_] == ';' || substrx[ind_] == '!')
                        break;

                        ++ind_;
                    }
                    //if (ind_ == substrx.Length)
                    //    ind_ = substrx.Length - 1;

                    int len_ = ind_ - match.Index;
                    rTB.Select(match.Index + beg, len_);
                

                    if ( rTB.SelectionFont.Italic)
                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold | FontStyle.Italic);
                    else
                        rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold);

                string strw = str.Substring(match.Index, len_);

                if (Form1.sozluk == Sozluk.DEUTSCH
                    && onek != ""
                    && !strw.StartsWith(onek))
                {
                    // int wordstartIndex = str.   .Find(strHigh, startindex, RichTextBoxFinds.None);
                    // onek
                    string onek2 = " " + onek;
                    int endl = find_next_newline(str, ind_);
                    if (endl > ind_)
                    {
                        int indexo;
                        string subx_str = str.Substring(ind_, endl - ind_);
                        if (subx_str.Contains(onek2))
                        {
                            indexo = subx_str.LastIndexOf(onek2);       //.IndexOf(onek2);

                            rTB.Select(ind_ + indexo, onek2.Length);

                            if (rTB.SelectionFont.Italic)
                                rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold | FontStyle.Italic);
                            else
                                rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold);

                        }

                    }
                }

                match = match.NextMatch();
                }
            }

            //--------------------------------
            public static void MarkTokens(RichTextBox rTB, List<Textc> wordx)
            {
                string[] tokens = Consts.Tokens;

                foreach (string token in tokens)
                {
                    for (int i = 0; i < wordx.Count; i++)
                    {
                        if (wordx[i].Name == token)
                        {
                            int xlen = wordx[i].len;
                            if ((wordx[i].ind + xlen < rTB.Text.Length) && ((rTB.Text[wordx[i].ind + xlen]) == ','))
                                xlen += 1;

                            rTB.Select(wordx[i].ind, xlen);
                            rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Italic);
                            rTB.SelectionColor = Color.DimGray;     // DarkGray;       // Color.Gray;
                        }
                    }
                }

            if (Form1.sozluk == Sozluk.DEUTSCH)
            {
                tokens = Consts.Tokens_DEU;

                foreach (string token in tokens)
                {
                    int xlen;
                    char[] charSpl = { ' ' };
                    string[] str_sp2 = token.Split(charSpl, StringSplitOptions.RemoveEmptyEntries);

                    if (str_sp2.Count() == 1)
                    {
                        for (int i = 0; i < wordx.Count; i++)
                        {
                            if (wordx[i].Name == token)
                            {
                                int indx = wordx[i].ind;
                                xlen = wordx[i].len;
                                if ((wordx[i].ind + xlen < rTB.Text.Length) &&
                                                       (rTB.Text[wordx[i].ind + xlen] == ','
                                                        || rTB.Text[wordx[i].ind + xlen] == ';'
                                                        || rTB.Text[wordx[i].ind + xlen] == ':'))
                                {
                                    xlen += 1;
                                }
                                 
                                if(wordx[i].Name == "Akk" || wordx[i].Name == "Dat" || wordx[i].Name == "Gen")
                                {
                                    if( indx > 0 && rTB.Text[indx-1] == '('
                                       && indx + xlen < rTB.Text.Length && rTB.Text[indx + xlen] == ')')
                                    {
                                        --indx;
                                        xlen += 2;
                                    }
                                }

                                rTB.Select(indx, xlen);
                                rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Italic);
                                rTB.SelectionColor = Color.DimGray;  //  Color.Gray;
                            }

                        }
                    }
                    else // 2 elemanlı token
                    {
                        for (int i = 0; i < (wordx.Count - 1); i++)
                        {
                            if (wordx[i].Name == str_sp2[0]
                               && wordx[i + 1].Name == str_sp2[1])
                            {
                                xlen = wordx[i + 1].ind - wordx[i].ind + wordx[i + 1].len;


                                if ((wordx[i + 1].ind + wordx[i + 1].len < rTB.Text.Length) &&
                                                          (rTB.Text[wordx[i+1].ind + wordx[i + 1].len] == ','
                                                           || rTB.Text[wordx[i+1].ind + wordx[i + 1].len] == ';'
                                                           || rTB.Text[wordx[i+1].ind + wordx[i + 1].len] == ':'))
                                {
                                    xlen += 1;
                                }

                                rTB.Select(wordx[i].ind, xlen);
                                rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Italic);
                                rTB.SelectionColor = Color.DimGray;  // Color.Gray;
                            }
                        }
                    }
                }
            }
        }

        //================================

        public static int find_prev_newline(string str, int cur_loc)
        {
            int beg = cur_loc;

            if (beg < 0)
            {
                beg = 0;
            }
            else if (beg >= str.Length)
            {
                beg = str.Length - 1;
            }

            if (beg > 0 && str[beg] == '\n')
            {
                beg--;
            }

            while (beg > 0 && str[beg] != '\n')
            {
                --beg;
            }

            return beg;
        }


        public static int find_next_newline(string str, int cur_loc)
        {
            int end = cur_loc;

            if (end < 0)
            {
                end = 0;
            }

            while (end < str.Length && str[end] != '\n')
            {
                ++end;
            }

            return end;

        }

        //================================
        public static void PartialEditor2(RichTextBox rTB, string namex)
        {
            int curLoc = rTB.SelectionStart;
                
            string str = rTB.Text.ToString();

            //if (curLoc > 0){ rTB.SelectionStart = curLoc - 1; }
            //rTB.SelectionLength = 1;

            //rTB.SelectionColor = Color.DarkBlue;
            //rTB.SelectionFont = new Font(rTextBoxFont, rTextBoxFont.Style | FontStyle.Bold );

            int beg = find_prev_newline(str, curLoc);
            int end = find_next_newline(str, curLoc);

            if (beg == end){ return; }

            //while (++pos < end && str[pos] != '\n') ;            

            Common.LineMarker(rTB, beg, end);

            List<Textc> wordx = Common.WordPartitioner(rTB, beg, end);

            Common.MarkTokens(rTB, wordx);

            if (namex != "")
            {
                
                if (Form1.sozluk == Sozluk.DEUTSCH)
                {

                    // str'de ilk satırı bul!
                     //if (namex.Count() > 0)
                    {
                        listDE = FindTokenWords(str, namex);  // IndexOf
                        Common.WordNameMarkerDEU(rTB, str, namex, listDE, beg, end);
                    }

                }
                else
                {
                    Common.WordNameMarker2(rTB, str, namex, wordx, beg, end);
                }
            }

            rTB.SelectionStart = curLoc;
            rTB.SelectionLength = 0;
        }
        //===================================

        public static string findPlural(string input)
        {        
                // Kontrol: String hem "<" ile başlamalı hem de ">" ile bitmeli
                //if (input.StartsWith("<") && input.EndsWith(">"))
                {
                    // Son kelimeyi ayır
                    int lastSpaceIndex = input.LastIndexOfAny(new char[] { ',', ';', ' ' });
                    string word = input;

                    if (lastSpaceIndex != -1 && lastSpaceIndex < input.Length - 1)
                    {
                        word = input.Substring(lastSpaceIndex + 1);  // , input.Length - lastSpaceIndex - 2);
                    }
                        // Kelimenin uzunluğu en az 3 olmalı
                        if (word.Length >= 3 && word !="Pl.")
                        {
                            return word;
                        }

                }

                return null; // Şartlar sağlanmadıysa null döndür
        }


    //===================================
    public static List<string> listDE = new List<string>();
        public static string onek = "";

        public static List<string> FindTokenWords(string str, string namex)
        {
            //List<string> listDE = new List<string>();
            listDE.Clear();
            onek = "";

            int indx = find_next_newline(str, 0);
            string strx = str.Substring(0, indx );

            char[] charSplits2 = { ':', '|', ';', '.', ',', '<','>','\n','\r' };
            string[] dym = strx.Split(charSplits2, StringSplitOptions.RemoveEmptyEntries);

            for (int w = 0; w < dym.Count(); w++)
            {
                dym[w] = dym[w].Trim();                             
            }

            int dym_adet = dym.Count();

            if ( dym_adet == 0  )
            {
                return listDE;
            }
            else if(dym[0] == "der" || dym[0] == "die" || dym[0] == "das")
            {
                if (dym.Count() > 1)
                {
                    string str_plural = findPlural(dym[dym.Count() - 1]);

                    if (!string.IsNullOrEmpty(str_plural))
                    {
                        listDE.Add(str_plural);
                    }
                }

                return listDE;
            }
            else if ( dym[0].Length < 3 )
            {

                return listDE;
            }

            if (String.IsNullOrWhiteSpace(dym[dym_adet - 1]) || dym[dym_adet - 1].Length < 5)
            {
                --dym_adet;
            }

            for (int w = 0; w < dym_adet; w++)
            {
                if( dym[w].StartsWith("Adj") || dym[w].StartsWith("Adv"))
                    return listDE;
            }

            for (int w = 1; w < (dym_adet - 1); w++)
            {
                if (dym[w].Contains(' '))
                {
                    onek = dym[w].Substring(dym[w].IndexOf(' ') + 1).Trim();
                    break;
                }
            }

            string kokz = dym[0];

            if(kokz.Length <= onek.Length)
            {
                onek = "";
            }
            else if (onek != "" )
            {
                kokz = namex.Substring(onek.Length);
            }

            string kok = BulKok1Alm(kokz);
            EkleListTaki(kok);

            if( onek != "")
            {
                EkleListTaki(onek + kok);
            }

            int mem_cnt = dym_adet;

            if (mem_cnt < 2)
            {
                return listDE;
            }
            else
            {
                string estr = dym[mem_cnt - 1];

                if (estr.Contains("ge")
                        || (estr.EndsWith("en") || estr.EndsWith("t") || estr.EndsWith("an")))
                {
                    estr = estr.Trim();

                    if (estr.Contains(" "))
                    {
                        estr = estr.Substring(estr.IndexOf(' ') + 1);
                    }

                    listDE.Add(estr);

                }

                if (mem_cnt == 2) return listDE;

                kok = BulKok1Alm(dym[mem_cnt - 2]);
                EkleListTaki2(kok);

                if (onek != "")
                {
                    string kokx = dym[0].Substring(onek.Length);
                    kok = BulKok1Alm(kokx);
                    EkleListTaki(kok);
                    string kokzu = onek + "zu" + kokx;
                    listDE.Add(kokzu);
                }

                if (mem_cnt == 3)
                {
                    listDE.Select(x => x).Distinct();
                    return listDE;
                }

                string kokw = dym[1];

                if( kokw.Contains(' ') )
                {
                    kokw = kokw.Substring(0, kokw.IndexOf(' '));
                }

                if(kokw.EndsWith("t") )
                {
                    kokw = kokw.Substring(0, kokw.Length - 1);
                }
                
                kok = BulKok1Alm(kokw);
                EkleListTaki(kok);

                if (onek != "")
                {
                    string kokx = onek + kokw;     // dym[1].Substring(onek.Length);
                    kok = BulKok1Alm(kokx);
                    EkleListTaki(kok);
                }
            }

            listDE.Select(x => x).Distinct();
            return listDE;
        }
        //================================
        public static void EkleListTaki(string kok)
        {
            if (kok == "") return;

            listDE.Add(kok);

            if (kok.Contains(' '))
            {
                kok = kok.Substring(0, kok.IndexOf(' ') );
                listDE.Add(kok);
            }            

            if (kok.EndsWith( "t")
               || kok.EndsWith( "d" )
               || kok.EndsWith( "n")
               || kok.EndsWith( "m"))
            {
                listDE.Add(kok + "e");
                listDE.Add(kok + "en");

                if (!kok.EndsWith("n") && !kok.EndsWith("m"))
                {
                    kok = kok + "e";
                }
            }
            else if (!kok.EndsWith("e") )
            {
                listDE.Add(kok + "e");

                if( n_only ) listDE.Add(kok + "n");
                else listDE.Add(kok + "en");
            }
            else
            {
                listDE.Add(kok + "n");
            }

            listDE.Add(kok + "st");
            listDE.Add(kok + "t");
                        
        }
        //================================
        public static void EkleListTaki2(string kok)
        {
            if (kok == "") return;
            
            listDE.Add(kok);

            if( kok.Contains(' ') )
            {
                //onek = kok.Substring(kok.IndexOf(' ') + 1).Trim();
                kok = kok.Substring(0, kok.IndexOf(' ') );
                listDE.Add(kok);                
            }

            ekle_past(kok);

            if (onek != "")
            {
                ekle_past(onek + kok);
            }

        }
        //================================
        public static void ekle_past(string kok)
        {
            if (kok.EndsWith("te"))
            {
                listDE.Add(kok + "n");
                listDE.Add(kok + "st");
                listDE.Add(kok + "t");
            }
            else if (kok.EndsWith("t"))
            {
                listDE.Add(kok + "en");
                listDE.Add(kok + "est");
                listDE.Add(kok + "et");
            }
            else
            {
                listDE.Add(kok + "en");
                listDE.Add(kok + "st");
                listDE.Add(kok + "t");
            }
        }

        //================================
        public static bool n_only;
        public static string BulKok1Alm(string kok)
        {
            n_only = false;
            int xlen = kok.Length;
            string kokx; 
            string onex;
            if (kok.Contains(' '))
            {
                int len = kok.IndexOf(' ');
                //if (len > 0) --len;

                onex = kok.Substring(len + 1);
                kokx = kok.Substring(0, len);
                kok = kokx;
                xlen = kok.Length;
            }
            
                if (!kok.EndsWith("n"))
                    return kok;

                if (kok.EndsWith("en"))
                {
                    kok = kok.Substring(0, xlen - 2);
                }
                else
                {
                    kok = kok.Substring(0, xlen - 1);
                    n_only = true;
                }
            
            return kok;
        }
       
         //================================
        public static string ReplaceStr(string myString, string oldValue, string newValue)
        {               
            string working = myString;
            int index = working.IndexOf(oldValue, StringComparison.CurrentCulture);   // InvariantCulture); 

            while (index != -1)
            {
                working = working.Remove(index, oldValue.Length);
                working = working.Insert(index, newValue);
                index += newValue.Length;
                index = working.IndexOf(oldValue, index, StringComparison.CurrentCulture);                    
            }

            return working;
        }

        //================================
        //================================
    }  // end of Class Common



}

