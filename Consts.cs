﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApp5_0
{
        
    static class Consts
    {
        public const string Version = "7.2.26";
        public const string DosyaYolu = "C:\\Users\\user\\source\\sozluk1.txt";
        public const string DosyaYolu2 = "C:\\Users\\user\\source\\export.txt";
        public const string Dosya2Yolu = "C:\\Users\\user\\source\\temp.txt";        
        public const string ImagePath = "C:\\Users\\user\\source\\repos\\Octopus\\Oct4.png";  //  C:\Users\user\source\repos\Octopus\Oct4.png

        public const string SozlukAdiDeutsch = "DEUTSCH WORTERBUCH ";
        public const string SozlukAdiEnglish = "ENGLISH DICTIONARY ";

        public const int PadLen1 = 20;
        public const int PadLen_wide = 48;

        public static DateTime InitialDate = new DateTime(2023, 1, 1, 00, 00, 00);


        public static Font TextBoxFont = new Font("Consolas", 9); 
        //C:\Users\user\source\repos\Octopus\Oct4.png
        public static readonly string[] Tokens = { "İsim", "Fiil", "Biyoloji", "Askerlik", "Askeri", "Jeoloji", "Mimarlık",
        "Sıfat", "Geçişsiz", "Coğrafya", "Felsefe", "Tıp","Patoloji", "Zooloji", "Zarf", "Edat", "Tekstil Sanayii", "Tekstil", "Sanayii","Ekonomi",
        "Bilgi Teknolojileri", "Felsefe", "Mantık", "Meteoroloji", "Fizyoloji", "Psikoloji", "Fizyoloji", "Makine Mühendisliği", "Makine",
        "Ulaşım", "İstihdam", "Elektrik-Elektronik", "Ticaret", "Reklamcılık", "Botanik", "Uluslararası Hukuk", "Hukuk", "Sosyoloji",
        "Denizcilik", "Geçişli Fiil", "Geçişli", "Tiyatro", "Botanik", "Gıda ve Mutfak", "Astronomi", "Gramer", "Müzik", "Matematik",
        "Elektrik-Elektronik", "İnşaat", "İstatistik", 
        "LITERARY", "TECHNICAL", "HISTORICAL", "INFORMAL", "BRITISH", "NORTH AMERICAN", "ARCHAIC","DATED",
        "INFORMAL", "FORMAL", "US", "LAW", "NORTH AMERICAN", "PHYSICS", "ELECTRONICS", "PSYCHOLOGY", "GRAMMAR", "BIOLOGY",
         "Geometri", "Kimya", "İşletme", "mec. ", "k.d. ", "argo "
        };

        public static readonly string[] Tokens_DEU = { "nur attr", "nur adv", "nur Sg", "Pl", "ohne Steigerung", "nicht adv", "INFORM", "gespr", "geschr", "Akk", "Dat", "Gen", 
        "opp", "mst", "ID", "NB", "Abb", "hum", "hist", "lit", "pej", "iron", "veraltend", "präd", "südd", "nordd", "attr", "UMG", "umg" };

        public static readonly string[,] TrimWords = new string[,]
        {
            { "GİZLE", "" },
            { "Fiil\n", "\n" },
            { "İsim\n", "\n" },
            { "Zarf\n", "\n" },
            { "Muhasebe\n", "\n" },
            { "Sıfat\n", "\n" }, 
            { "Fizik\n", "\n" },
            { "Medeni Hukuk\n", "\n" },
            { "Hukuk\n", "\n" },
            { "Dil ve Edebiyat\n", "\n" },
            { "3rd person present: ", "" },
            { "past tense: ", ""},
            { "past participle: ", ""},
            { "gerund or present participle: ", ""},
            { "Ingilizce-Türkçe çeviriler: Atalay Sözlügü, 1. Basim", "" },
            { "Ingilizce-Türkçe Ilgili Terimler", "" },
        };
    }

    
    static class irregularVerbs
    {
        public static readonly string[,] En = new string[,]
    {
            {   "bear",   "bore",   "borne",  "born" },
            {   "beat",   "beat",   "beaten", "" },
            {   "become", "became", "become", "" },
            {   "befall", "befell", "befallen", "" },
            {   "begin",  "began",  "begun", "" },
            {   "bend",   "bent",   "bent", "" },
            {   "bet",    "bet",    "bet", "" },
            {   "bid",    "bade",   "bid",  "bidden" },
            {   "bind",   "bound",  "bound", "" },
            {   "bite",   "bit",    "bitten", "" },
            {   "bleed",  "bled",   "bled", "" },
            {   "blow",   "blew",   "blown", "" },
            {   "break",  "broke",  "broken", "" },
            {   "breed",  "bred",   "bred", "" },
            {   "bring",  "brought","brought", "" },
            {   "broadcast",  "broadcast",  "broadcast", "" },
            {   "build",  "built",  "built", "" },
            {   "burn",   "burnt",  "burnt", "" },
            {   "burst",  "burst",  "burst", "" },
            {   "bust",   "bust",   "bust", "" },
            {   "buy",    "bought", "bought", "" },
            {   "cast",   "cast",   "cast", "" },
            {   "catch",  "caught", "caught", "" },
            {   "choose", "chose",  "chosen", "" },
            {   "cling",  "clung",  "clung", "" },
            {   "come",   "came",   "come", "" },
            {   "cost",   "cost",   "cost", "" },
            {   "creep",  "crept",  "crept", "" },
            {   "cut",    "cut ",   "cut", "" },
            {   "deal",   "dealt",  "dealt", "" },
            {   "dig",    "dug ",   "dug", "" },
            {   "dive",   "dived",  "dove",   "dived" },
            {   "do",     "did ",   "done", "" },
            {   "draw",   "drew",   "drawn", "" },
            {   "dream",  "dreamt", "dreamt", "" },
            {   "drink",  "drank",  "drunk", "" },
            {   "drive",  "drove",  "driven", "" },
            {   "eat",    "ate",    "eaten", "" },
            {   "fall",   "fell",   "fallen", "" },
            {   "feed",   "fed ",   "fed", "" },
            {   "feel",   "felt",   "felt", "" },
            {   "fight",  "fought", "fought", "" },
            {   "find",   "found",  "found", "" },
            {   "flee",   "fled",   "fled", "" },
            {   "fling",  "flung",  "flung", "" },
            {   "fly",    "flew",   "flown", "" },
            {   "forbid", "forbade", "forbad", "forbidden" },
            {   "forecast", "forecast", "forecast", "" },
            {   "forget", "forgot", "forgotten", "" },
            {   "forsake","forsook", "forsaken", ""    },
            {   "freeze", "froze",  "frozen", "" },
            {   "get",    "got", "gotten", ""  },
            {   "give",   "gave",   "given", "" },
            {   "grind",  "ground", "ground", "" },
            {   "go",     "went",   "gone", "" },
            {   "grow",   "grew",   "grown", "" },
            {   "hang",   "hung",   "hung", "" },
            {   "have",   "had",    "had", "" },
            {   "hear",   "heard",  "heard", "" },
            {   "hide",   "hid",    "hidden", "" },
            {   "hit",    "hit",    "hit", "" },
            {   "hold",   "held",   "held", "" },
            {   "hurt",   "hurt",   "hurt", "" },
            {   "keep",   "kept",   "kept", "" },
            {   "know",   "knew",   "known", "" },
            {   "lay",    "laid",   "laid", "" },
            {   "lead",   "led",    "led", "" },
            {   "learn",  "learnt", "learnt", ""  },
            {   "leave",  "left",   "left", "" },
            {   "lend",   "lent",   "lent", "" },
            {   "let",    "let",    "let", "" },
            {   "lie",    "lay",    "lain", "" },
            {   "light",  "lit",    "lit", ""  },
            {   "lose",   "lost",   "lost", "" },
            {   "make",   "made",   "made", "" },
            {   "mean",   "meant",  "meant", "" },
            {   "meet",   "met",    "met", "" },
            {   "pay",    "paid",   "paid", "" },
            {   "prove",  "proved", "proven", "" },
            {   "put",    "put",    "put", "" },
            {   "quit",   "quit",   "quit", "" },
            {   "read",   "read",   "read", "" },
            {   "rid",    "rid",    "rid", "" },
            {   "ride",   "rode",   "ridden", "" },
            {   "ring",   "rang",   "rung", "" },
            {   "rise",   "rose",   "risen", "" },
            {   "run",    "ran",    "run", "" },
            {   "saw",    "sawed",  "sawed",  "sawn" },
            {   "say",    "said",   "said", "" },
            {   "see",    "saw",    "seen", "" },
            {   "seek",   "sought", "sought", "" },
            {   "sell",   "sold",   "sold", "" },
            {   "set",    "set",    "set", "" },
            {   "sew",    "sewed",  "sewn", "" },
            {   "shake",  "shook",  "shaken", "" },
            {   "shear",  "sheared","shorn", "" },
            {   "shed",   "shed",   "shed", "" },
            {   "shine",  "shone",  "shone", "" },
            {   "shoot",  "shot",   "shot", "" },
            {   "show",   "showed", "shown", "" },
            {   "shut",   "shut",   "shut", "" },
            {   "sing",   "sang",   "sung", "" },
            {   "sink",   "sank",   "sunk", "" },
            {   "sit",    "sat",    "sat", "" },
            {   "slay",   "slew",   "slain", "" },
            {   "sleep",  "slept",  "slept", "" },
            {   "slide",  "slid",   "slid", "" },
            {   "sling",  "slung",  "slung", "" },
            {   "slink",  "slunk",  "slunk", "" },
            {   "slit",   "slit",   "slit", "" },
            {   "sow",    "sowed",  "sown", "" },
            {   "speak",  "spoke",  "spoken", "" },
            {   "speed",  "sped",   "sped", "" },
            {   "spend",  "spent",  "spent", "" },
            {   "spin",   "spun",   "spun", "" },
            {   "spit",   "spat",   "spat", "" },
            {   "split",  "split",  "split", "" },
            {   "spread", "spread", "spread", "" },
            {   "spring", "sprang", "sprung", "" },
            {   "stand",  "stood",  "stood", "" },
            {   "steal",  "stole",  "stolen", "" },
            {   "stick",  "stuck",  "stuck", "" },
            {   "sting",  "stung",  "stung", "" },
            {   "stink",  "stank", "stunk", "" },
            {   "stride", "strode", "stridden", "" },
            {   "strike", "struck", "struck", "" },
            {   "string", "strung", "strung", "" },
            {   "strive", "strove", "striven", "" },
            {   "swear",  "swore",  "sworn", "" },
            {   "sweep",  "swept",  "swept", "" },
            {   "swell",  "swelled","swollen", "" },
            {   "swim",   "swam",   "swum", "" },
            {   "swing",  "swung",  "swung", "" },
            {   "take",   "took",   "taken", "" },
            {   "teach",  "taught", "taught", "" },
            {   "tear",   "tore",   "torn", "" },
            {   "tell",   "told",   "told", "" },
            {   "think",  "thought","thought", "" },
            {   "thrive", "throve", "thrived", "" },
            {   "throw",  "threw",  "thrown", "" },
            {   "thrust", "thrust", "thrust", "" },
            {   "tread",  "trod",   "trodden", "trod" },
            {   "understand", "understood", "understood", "" },
            {   "wake",   "woke",   "woken", "" },
            {   "wear",   "wore",   "worn", "" },
            {   "weave",  "wove",   "woven", "" },
            {   "weep",   "wept",   "wept", "" },
            {   "wet",    "wet",    "wet", "" },
            {   "win",    "won",    "won", "" },
            {   "wind",   "wound",  "wound", "" },
            {   "wring",  "wrung",  "wrung", "" },
            {   "write",  "wrote",  "written", "" },
            {   "be",     "was",    "were",   "been" },            
    };

    }


}
