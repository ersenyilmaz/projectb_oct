﻿
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Drawing.Text;

namespace WindowsFormsApp5_0
{

    public partial class Form1 : Form
    {


        private void Undo()
        {
            if (undoStack.Count > 1)
            {
                redoStack.Push(undoStack.Pop());
                string previousText = undoStack.Peek();
                isUndoing = true;
                RichTextBox1.Text = previousText;
                isUndoing = false;
                RichTextBox1.SelectionStart = RichTextBox1.TextLength;
            }
        }


        private void Redo()
        {
            if (redoStack.Count > 0)
            {
                string nextText = redoStack.Pop();
                isUndoing = true;
                RichTextBox1.Text = nextText;
                isUndoing = false;
                RichTextBox1.SelectionStart = RichTextBox1.TextLength;
                undoStack.Push(nextText);
            }
        }
        //======================================
        public class RoundButton : Button
    { 
            /*
        public RoundButton()
        {
            this.FlatStyle = FlatStyle.Flat;
            this.FlatAppearance.BorderSize = 0;
            this.Size = new Size(22, 22);
            this.Font = new Font("Arial", 10, FontStyle.Bold);
            this.ForeColor = Color.White;
            this.BackColor = Color.Red;
            this.Text = "X";            
        }   */

            /*
        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);

            using (StringFormat format = new StringFormat())
            {
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                pevent.Graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                pevent.Graphics.DrawString("X", this.Font, new SolidBrush(this.ForeColor), this.ClientRectangle, format);
            }
        }  */

        protected override bool ShowFocusCues
        {
            get { return false; }
        }
    }







}
}

