﻿
namespace WindowsFormsApp5_0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.RichTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_score = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxEtiket = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelSozlukAdi = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.scoreBox2 = new System.Windows.Forms.TextBox();
            this.scoreBox1 = new System.Windows.Forms.TextBox();
            this.scoreBox0 = new System.Windows.Forms.TextBox();
            this.scoreBox5 = new System.Windows.Forms.TextBox();
            this.scoreBox4 = new System.Windows.Forms.TextBox();
            this.scoreBox3 = new System.Windows.Forms.TextBox();
            this.scoreBox8 = new System.Windows.Forms.TextBox();
            this.scoreBox7 = new System.Windows.Forms.TextBox();
            this.scoreBox6 = new System.Windows.Forms.TextBox();
            this.scoreBox11 = new System.Windows.Forms.TextBox();
            this.scoreBox10 = new System.Windows.Forms.TextBox();
            this.scoreBox9 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openUserFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.MakeTrimToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllScoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.learnStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.DictionarytoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reverseDictionaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thirdDictionaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.structuresReverseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sentencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sentencesReverseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artikelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ModeStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dictionaryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.randomTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomWeightedTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alphabetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reverseAlphabetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomScoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomScoreWeightedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.LightBlue;
            this.listBox1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 19;
            this.listBox1.Location = new System.Drawing.Point(6, 84);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(317, 593);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listBox1_KeyPress);
            // 
            // RichTextBox1
            // 
            this.RichTextBox1.BackColor = System.Drawing.Color.LightBlue;
            this.RichTextBox1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.RichTextBox1.Location = new System.Drawing.Point(326, 126);
            this.RichTextBox1.Name = "RichTextBox1";
            this.RichTextBox1.Size = new System.Drawing.Size(757, 577);
            this.RichTextBox1.TabIndex = 1;
            this.RichTextBox1.Text = "";
            this.RichTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RichTextBox1_MouseClick);
            this.RichTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cambria", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.DarkBlue;
            this.label1.Location = new System.Drawing.Point(646, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // textBox_score
            // 
            this.textBox_score.BackColor = System.Drawing.Color.LightBlue;
            this.textBox_score.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBox_score.ForeColor = System.Drawing.Color.Maroon;
            this.textBox_score.Location = new System.Drawing.Point(461, 29);
            this.textBox_score.Margin = new System.Windows.Forms.Padding(0);
            this.textBox_score.Name = "textBox_score";
            this.textBox_score.Size = new System.Drawing.Size(24, 29);
            this.textBox_score.TabIndex = 2;
            this.textBox_score.Text = "3";
            this.textBox_score.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_score.Click += new System.EventHandler(this.textBox_score_Click);
            this.textBox_score.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_score_MouseClick);
            this.textBox_score.TextChanged += new System.EventHandler(this.textBox_score_TextChanged);
            this.textBox_score.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_score_KeyDown);
            this.textBox_score.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_score_KeyPress);
            this.textBox_score.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox_score_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.MenuBar;
            this.label4.Font = new System.Drawing.Font("Segoe UI Historic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(906, 581);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Kelime";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Historic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(432, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Scores";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // textBoxEtiket
            // 
            this.textBoxEtiket.BackColor = System.Drawing.Color.LightBlue;
            this.textBoxEtiket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxEtiket.Font = new System.Drawing.Font("Segoe UI Historic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxEtiket.ForeColor = System.Drawing.Color.Maroon;
            this.textBoxEtiket.Location = new System.Drawing.Point(795, 4);
            this.textBoxEtiket.Name = "textBoxEtiket";
            this.textBoxEtiket.Size = new System.Drawing.Size(154, 36);
            this.textBoxEtiket.TabIndex = 16;
            this.textBoxEtiket.Visible = false;
            this.textBoxEtiket.TextChanged += new System.EventHandler(this.textBoxEtiket_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Historic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(712, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Labels";
            this.label6.Visible = false;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(769, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 27);
            this.label7.TabIndex = 18;
            this.label7.Text = "#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Visible = false;
            // 
            // labelSozlukAdi
            // 
            this.labelSozlukAdi.AutoSize = true;
            this.labelSozlukAdi.BackColor = System.Drawing.SystemColors.MenuBar;
            this.labelSozlukAdi.Font = new System.Drawing.Font("Segoe UI Historic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSozlukAdi.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelSozlukAdi.Location = new System.Drawing.Point(756, 581);
            this.labelSozlukAdi.Name = "labelSozlukAdi";
            this.labelSozlukAdi.Size = new System.Drawing.Size(100, 21);
            this.labelSozlukAdi.TabIndex = 21;
            this.labelSozlukAdi.Text = "DICTIONARY";
            this.labelSozlukAdi.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.richTextBox2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.scoreBox2);
            this.panel1.Controls.Add(this.scoreBox1);
            this.panel1.Controls.Add(this.scoreBox0);
            this.panel1.Controls.Add(this.scoreBox5);
            this.panel1.Controls.Add(this.scoreBox4);
            this.panel1.Controls.Add(this.scoreBox3);
            this.panel1.Controls.Add(this.scoreBox8);
            this.panel1.Controls.Add(this.scoreBox7);
            this.panel1.Controls.Add(this.scoreBox6);
            this.panel1.Controls.Add(this.scoreBox11);
            this.panel1.Controls.Add(this.scoreBox10);
            this.panel1.Controls.Add(this.scoreBox9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.labelSozlukAdi);
            this.panel1.Controls.Add(this.textBoxEtiket);
            this.panel1.Controls.Add(this.listBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.RichTextBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBox_score);
            this.panel1.Location = new System.Drawing.Point(0, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1086, 678);
            this.panel1.TabIndex = 25;
            // 
            // pictureBox1
            // 
            this.pictureBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Cursor;
            this.pictureBox1.Image = global::Octopus.Properties.Resources.Oct4;
            this.pictureBox1.InitialImage = global::Octopus.Properties.Resources.Oct4;
            this.pictureBox1.Location = new System.Drawing.Point(987, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(96, 87);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Historic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(514, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 20);
            this.label9.TabIndex = 52;
            this.label9.Text = "Score";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.Color.LightBlue;
            this.richTextBox2.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.richTextBox2.ForeColor = System.Drawing.Color.DarkBlue;
            this.richTextBox2.Location = new System.Drawing.Point(326, 84);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(757, 36);
            this.richTextBox2.TabIndex = 51;
            this.richTextBox2.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Sitka Banner", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label3.Location = new System.Drawing.Point(3, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 30);
            this.label3.TabIndex = 48;
            this.label3.Text = "label3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Historic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(517, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 20);
            this.label2.TabIndex = 47;
            this.label2.Text = "Total";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox1.Font = new System.Drawing.Font("Cambria", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBox1.ForeColor = System.Drawing.Color.Maroon;
            this.textBox1.Location = new System.Drawing.Point(563, 19);
            this.textBox1.Margin = new System.Windows.Forms.Padding(0);
            this.textBox1.Name = "textBox1";
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox1.Size = new System.Drawing.Size(75, 45);
            this.textBox1.TabIndex = 46;
            this.textBox1.Text = "2";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // scoreBox2
            // 
            this.scoreBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox2.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox2.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox2.Location = new System.Drawing.Point(271, 29);
            this.scoreBox2.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox2.Name = "scoreBox2";
            this.scoreBox2.Size = new System.Drawing.Size(20, 29);
            this.scoreBox2.TabIndex = 45;
            this.scoreBox2.Text = "2";
            this.scoreBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox1
            // 
            this.scoreBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox1.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox1.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox1.Location = new System.Drawing.Point(252, 29);
            this.scoreBox1.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox1.Name = "scoreBox1";
            this.scoreBox1.Size = new System.Drawing.Size(20, 29);
            this.scoreBox1.TabIndex = 44;
            this.scoreBox1.Text = "2";
            this.scoreBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox0
            // 
            this.scoreBox0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox0.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox0.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox0.Location = new System.Drawing.Point(233, 29);
            this.scoreBox0.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox0.Name = "scoreBox0";
            this.scoreBox0.Size = new System.Drawing.Size(20, 29);
            this.scoreBox0.TabIndex = 43;
            this.scoreBox0.Text = "0";
            this.scoreBox0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox5
            // 
            this.scoreBox5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox5.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox5.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox5.Location = new System.Drawing.Point(328, 29);
            this.scoreBox5.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox5.Name = "scoreBox5";
            this.scoreBox5.Size = new System.Drawing.Size(20, 29);
            this.scoreBox5.TabIndex = 42;
            this.scoreBox5.Text = "2";
            this.scoreBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox4
            // 
            this.scoreBox4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox4.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox4.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox4.Location = new System.Drawing.Point(309, 29);
            this.scoreBox4.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox4.Name = "scoreBox4";
            this.scoreBox4.Size = new System.Drawing.Size(20, 29);
            this.scoreBox4.TabIndex = 41;
            this.scoreBox4.Text = "2";
            this.scoreBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox3
            // 
            this.scoreBox3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox3.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox3.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox3.Location = new System.Drawing.Point(290, 29);
            this.scoreBox3.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox3.Name = "scoreBox3";
            this.scoreBox3.Size = new System.Drawing.Size(20, 29);
            this.scoreBox3.TabIndex = 40;
            this.scoreBox3.Text = "2";
            this.scoreBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox8
            // 
            this.scoreBox8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox8.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox8.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox8.Location = new System.Drawing.Point(385, 29);
            this.scoreBox8.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox8.Name = "scoreBox8";
            this.scoreBox8.Size = new System.Drawing.Size(20, 29);
            this.scoreBox8.TabIndex = 39;
            this.scoreBox8.Text = "2";
            this.scoreBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox7
            // 
            this.scoreBox7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox7.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox7.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox7.Location = new System.Drawing.Point(366, 29);
            this.scoreBox7.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox7.Name = "scoreBox7";
            this.scoreBox7.Size = new System.Drawing.Size(20, 29);
            this.scoreBox7.TabIndex = 38;
            this.scoreBox7.Text = "2";
            this.scoreBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox6
            // 
            this.scoreBox6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox6.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox6.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox6.Location = new System.Drawing.Point(347, 29);
            this.scoreBox6.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox6.Name = "scoreBox6";
            this.scoreBox6.Size = new System.Drawing.Size(20, 29);
            this.scoreBox6.TabIndex = 37;
            this.scoreBox6.Text = "2";
            this.scoreBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox11
            // 
            this.scoreBox11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox11.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox11.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox11.Location = new System.Drawing.Point(442, 29);
            this.scoreBox11.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox11.Name = "scoreBox11";
            this.scoreBox11.Size = new System.Drawing.Size(20, 29);
            this.scoreBox11.TabIndex = 36;
            this.scoreBox11.Text = "2";
            this.scoreBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // scoreBox10
            // 
            this.scoreBox10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox10.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox10.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox10.Location = new System.Drawing.Point(423, 29);
            this.scoreBox10.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox10.Name = "scoreBox10";
            this.scoreBox10.Size = new System.Drawing.Size(20, 29);
            this.scoreBox10.TabIndex = 35;
            this.scoreBox10.Text = "2";
            this.scoreBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.scoreBox10.TextChanged += new System.EventHandler(this.scoreBox10_TextChanged);
            // 
            // scoreBox9
            // 
            this.scoreBox9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.scoreBox9.Font = new System.Drawing.Font("Lucida Console", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.scoreBox9.ForeColor = System.Drawing.Color.Maroon;
            this.scoreBox9.Location = new System.Drawing.Point(404, 29);
            this.scoreBox9.Margin = new System.Windows.Forms.Padding(0);
            this.scoreBox9.Name = "scoreBox9";
            this.scoreBox9.Size = new System.Drawing.Size(20, 29);
            this.scoreBox9.TabIndex = 34;
            this.scoreBox9.Text = "2";
            this.scoreBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Historic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label8.Location = new System.Drawing.Point(648, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "randomweight";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label8.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.AllowDrop = true;
            this.comboBox1.BackColor = System.Drawing.Color.LightBlue;
            this.comboBox1.Font = new System.Drawing.Font("Segoe UI Historic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(795, 7);
            this.comboBox1.MaxDropDownItems = 100;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(164, 30);
            this.comboBox1.Sorted = true;
            this.comboBox1.TabIndex = 21;
            this.comboBox1.TabStop = false;
            this.comboBox1.Visible = false;
            this.comboBox1.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_2);
            this.comboBox1.DropDownClosed += new System.EventHandler(this.comboBox1_DropDownClosed);
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            this.comboBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyUp);
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
            this.statusStrip2.Location = new System.Drawing.Point(0, 699);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1085, 22);
            this.statusStrip2.TabIndex = 0;
            this.statusStrip2.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip2_ItemClicked);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(476, 17);
            this.toolStripStatusLabel2.Spring = true;
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(476, 17);
            this.toolStripStatusLabel3.Spring = true;
            this.toolStripStatusLabel3.Text = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.checkBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.checkBox1.Font = new System.Drawing.Font("Segoe UI Historic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.checkBox1.Location = new System.Drawing.Point(774, 8);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 18);
            this.checkBox1.TabIndex = 28;
            this.checkBox1.Text = "Search All";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.openUserFileToolStripMenuItem,
            this.toolStripSeparator,
            this.toolStripMenuItem1,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 26);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(254, 24);
            this.newToolStripMenuItem.Text = "&New";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(254, 24);
            this.openToolStripMenuItem.Text = "&Open Dictionary File";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // openUserFileToolStripMenuItem
            // 
            this.openUserFileToolStripMenuItem.Name = "openUserFileToolStripMenuItem";
            this.openUserFileToolStripMenuItem.Size = new System.Drawing.Size(254, 24);
            this.openUserFileToolStripMenuItem.Text = "Open User File";
            this.openUserFileToolStripMenuItem.Click += new System.EventHandler(this.openUserFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(251, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(254, 24);
            this.toolStripMenuItem1.Text = "Add Word/Phrase";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(254, 24);
            this.saveToolStripMenuItem.Text = "&Save Dictionary";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(251, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(254, 24);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator4,
            this.MakeTrimToolStripMenuItem,
            this.clearAllScoresToolStripMenuItem});
            this.editToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(44, 26);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(168, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(168, 6);
            // 
            // MakeTrimToolStripMenuItem
            // 
            this.MakeTrimToolStripMenuItem.Name = "MakeTrimToolStripMenuItem";
            this.MakeTrimToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.MakeTrimToolStripMenuItem.Text = "Make Trim";
            this.MakeTrimToolStripMenuItem.Click += new System.EventHandler(this.exportToTxtToolStripMenuItem_Click);
            // 
            // clearAllScoresToolStripMenuItem
            // 
            this.clearAllScoresToolStripMenuItem.Name = "clearAllScoresToolStripMenuItem";
            this.clearAllScoresToolStripMenuItem.Size = new System.Drawing.Size(171, 24);
            this.clearAllScoresToolStripMenuItem.Text = "Clear All Scores";
            this.clearAllScoresToolStripMenuItem.Click += new System.EventHandler(this.clearAllScoresToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.learnStatusToolStripMenuItem});
            this.toolsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(52, 26);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.customizeToolStripMenuItem.Text = "&Customize";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // learnStatusToolStripMenuItem
            // 
            this.learnStatusToolStripMenuItem.Name = "learnStatusToolStripMenuItem";
            this.learnStatusToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.learnStatusToolStripMenuItem.Text = "Learn Status";
            this.learnStatusToolStripMenuItem.Click += new System.EventHandler(this.learnStatusToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.contentsToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.helpToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 200, 0);
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(49, 26);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(134, 24);
            this.toolStripMenuItem2.Text = "Settings";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(131, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBox1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(160, 26);
            this.toolStripTextBox1.Click += new System.EventHandler(this.toolStripTextBox1_Click);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.DictionarytoolStripMenuItem,
            this.ModeStripMenuItem,
            this.sortStripMenuItem2,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.toolStripTextBox1,
            this.toolStripButtonToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1085, 30);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // DictionarytoolStripMenuItem
            // 
            this.DictionarytoolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem,
            this.reverseDictionaryToolStripMenuItem,
            this.thirdDictionaryToolStripMenuItem,
            this.structuresReverseToolStripMenuItem,
            this.sentencesToolStripMenuItem,
            this.sentencesReverseToolStripMenuItem,
            this.artikelsToolStripMenuItem,
            this.notesToolStripMenuItem});
            this.DictionarytoolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.DictionarytoolStripMenuItem.Name = "DictionarytoolStripMenuItem";
            this.DictionarytoolStripMenuItem.Size = new System.Drawing.Size(83, 26);
            this.DictionarytoolStripMenuItem.Text = "Dictionary";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.CheckOnClick = true;
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.testToolStripMenuItem.Text = "Words";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // reverseDictionaryToolStripMenuItem
            // 
            this.reverseDictionaryToolStripMenuItem.Name = "reverseDictionaryToolStripMenuItem";
            this.reverseDictionaryToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.reverseDictionaryToolStripMenuItem.Text = "Words_Reverse";
            this.reverseDictionaryToolStripMenuItem.Click += new System.EventHandler(this.reverseDictionaryToolStripMenuItem_Click);
            // 
            // thirdDictionaryToolStripMenuItem
            // 
            this.thirdDictionaryToolStripMenuItem.DoubleClickEnabled = true;
            this.thirdDictionaryToolStripMenuItem.Name = "thirdDictionaryToolStripMenuItem";
            this.thirdDictionaryToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.thirdDictionaryToolStripMenuItem.Text = "Structures";
            this.thirdDictionaryToolStripMenuItem.Click += new System.EventHandler(this.thirdDictionaryToolStripMenuItem_Click);
            // 
            // structuresReverseToolStripMenuItem
            // 
            this.structuresReverseToolStripMenuItem.Name = "structuresReverseToolStripMenuItem";
            this.structuresReverseToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.structuresReverseToolStripMenuItem.Text = "Structures_Reverse";
            this.structuresReverseToolStripMenuItem.Click += new System.EventHandler(this.structuresReverseToolStripMenuItem_Click);
            // 
            // sentencesToolStripMenuItem
            // 
            this.sentencesToolStripMenuItem.Name = "sentencesToolStripMenuItem";
            this.sentencesToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.sentencesToolStripMenuItem.Text = "Sentences";
            this.sentencesToolStripMenuItem.Click += new System.EventHandler(this.sentencesToolStripMenuItem_Click);
            // 
            // sentencesReverseToolStripMenuItem
            // 
            this.sentencesReverseToolStripMenuItem.Name = "sentencesReverseToolStripMenuItem";
            this.sentencesReverseToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.sentencesReverseToolStripMenuItem.Text = "Sentences_Reverse";
            this.sentencesReverseToolStripMenuItem.Click += new System.EventHandler(this.sentencesReverseToolStripMenuItem_Click);
            // 
            // artikelsToolStripMenuItem
            // 
            this.artikelsToolStripMenuItem.Name = "artikelsToolStripMenuItem";
            this.artikelsToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.artikelsToolStripMenuItem.Text = "Artikels";
            this.artikelsToolStripMenuItem.Click += new System.EventHandler(this.artikelsToolStripMenuItem_Click_1);
            // 
            // notesToolStripMenuItem
            // 
            this.notesToolStripMenuItem.Name = "notesToolStripMenuItem";
            this.notesToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.notesToolStripMenuItem.Text = "Notes";
            this.notesToolStripMenuItem.Click += new System.EventHandler(this.notesToolStripMenuItem_Click_1);
            // 
            // ModeStripMenuItem
            // 
            this.ModeStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dictionaryToolStripMenuItem1,
            this.testToolStripMenuItem1,
            this.randomTestToolStripMenuItem,
            this.randomWeightedTestToolStripMenuItem});
            this.ModeStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ModeStripMenuItem.Name = "ModeStripMenuItem";
            this.ModeStripMenuItem.Size = new System.Drawing.Size(57, 26);
            this.ModeStripMenuItem.Text = "Mode";
            // 
            // dictionaryToolStripMenuItem1
            // 
            this.dictionaryToolStripMenuItem1.CheckOnClick = true;
            this.dictionaryToolStripMenuItem1.Name = "dictionaryToolStripMenuItem1";
            this.dictionaryToolStripMenuItem1.Size = new System.Drawing.Size(219, 24);
            this.dictionaryToolStripMenuItem1.Text = "Dictionary";
            this.dictionaryToolStripMenuItem1.Click += new System.EventHandler(this.dictionaryToolStripMenuItem1_Click);
            // 
            // testToolStripMenuItem1
            // 
            this.testToolStripMenuItem1.CheckOnClick = true;
            this.testToolStripMenuItem1.Name = "testToolStripMenuItem1";
            this.testToolStripMenuItem1.Size = new System.Drawing.Size(219, 24);
            this.testToolStripMenuItem1.Text = "Test";
            this.testToolStripMenuItem1.Click += new System.EventHandler(this.testToolStripMenuItem1_Click);
            // 
            // randomTestToolStripMenuItem
            // 
            this.randomTestToolStripMenuItem.CheckOnClick = true;
            this.randomTestToolStripMenuItem.Name = "randomTestToolStripMenuItem";
            this.randomTestToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.randomTestToolStripMenuItem.Text = "Random Test";
            this.randomTestToolStripMenuItem.Click += new System.EventHandler(this.randomTestToolStripMenuItem_Click);
            // 
            // randomWeightedTestToolStripMenuItem
            // 
            this.randomWeightedTestToolStripMenuItem.CheckOnClick = true;
            this.randomWeightedTestToolStripMenuItem.Name = "randomWeightedTestToolStripMenuItem";
            this.randomWeightedTestToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.randomWeightedTestToolStripMenuItem.Text = "Random Weighted Test";
            this.randomWeightedTestToolStripMenuItem.Click += new System.EventHandler(this.randomWeightedTestToolStripMenuItem_Click);
            // 
            // sortStripMenuItem2
            // 
            this.sortStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.databaseToolStripMenuItem,
            this.alphabetToolStripMenuItem,
            this.reverseAlphabetToolStripMenuItem,
            this.scoreToolStripMenuItem,
            this.randomScoreToolStripMenuItem,
            this.randomScoreWeightedToolStripMenuItem,
            this.randomToolStripMenuItem,
            this.historyToolStripMenuItem,
            this.labelToolStripMenuItem});
            this.sortStripMenuItem2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.sortStripMenuItem2.Name = "sortStripMenuItem2";
            this.sortStripMenuItem2.Size = new System.Drawing.Size(46, 26);
            this.sortStripMenuItem2.Text = "Sort";
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.CheckOnClick = true;
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.databaseToolStripMenuItem.Text = "Database";
            this.databaseToolStripMenuItem.Click += new System.EventHandler(this.databaseToolStripMenuItem_Click);
            // 
            // alphabetToolStripMenuItem
            // 
            this.alphabetToolStripMenuItem.CheckOnClick = true;
            this.alphabetToolStripMenuItem.Name = "alphabetToolStripMenuItem";
            this.alphabetToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.alphabetToolStripMenuItem.Text = "Alphabet";
            this.alphabetToolStripMenuItem.Click += new System.EventHandler(this.alphabetToolStripMenuItem_Click);
            // 
            // reverseAlphabetToolStripMenuItem
            // 
            this.reverseAlphabetToolStripMenuItem.CheckOnClick = true;
            this.reverseAlphabetToolStripMenuItem.Name = "reverseAlphabetToolStripMenuItem";
            this.reverseAlphabetToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.reverseAlphabetToolStripMenuItem.Text = "Reverse Alphabet";
            this.reverseAlphabetToolStripMenuItem.Click += new System.EventHandler(this.reverseAlphabetToolStripMenuItem_Click);
            // 
            // scoreToolStripMenuItem
            // 
            this.scoreToolStripMenuItem.CheckOnClick = true;
            this.scoreToolStripMenuItem.Name = "scoreToolStripMenuItem";
            this.scoreToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.scoreToolStripMenuItem.Text = "Score";
            this.scoreToolStripMenuItem.Click += new System.EventHandler(this.scoreToolStripMenuItem_Click);
            // 
            // randomScoreToolStripMenuItem
            // 
            this.randomScoreToolStripMenuItem.CheckOnClick = true;
            this.randomScoreToolStripMenuItem.Name = "randomScoreToolStripMenuItem";
            this.randomScoreToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.randomScoreToolStripMenuItem.Text = "Random Score";
            this.randomScoreToolStripMenuItem.Click += new System.EventHandler(this.randomScoreToolStripMenuItem_Click);
            // 
            // randomScoreWeightedToolStripMenuItem
            // 
            this.randomScoreWeightedToolStripMenuItem.CheckOnClick = true;
            this.randomScoreWeightedToolStripMenuItem.Name = "randomScoreWeightedToolStripMenuItem";
            this.randomScoreWeightedToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.randomScoreWeightedToolStripMenuItem.Text = "Random Score Weighted";
            this.randomScoreWeightedToolStripMenuItem.Click += new System.EventHandler(this.randomScoreWeightedToolStripMenuItem_Click);
            // 
            // randomToolStripMenuItem
            // 
            this.randomToolStripMenuItem.CheckOnClick = true;
            this.randomToolStripMenuItem.Name = "randomToolStripMenuItem";
            this.randomToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.randomToolStripMenuItem.Text = "Random";
            this.randomToolStripMenuItem.Click += new System.EventHandler(this.randomToolStripMenuItem_Click);
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.CheckOnClick = true;
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.historyToolStripMenuItem.Text = "History";
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // labelToolStripMenuItem
            // 
            this.labelToolStripMenuItem.CheckOnClick = true;
            this.labelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.label1ToolStripMenuItem,
            this.label2ToolStripMenuItem});
            this.labelToolStripMenuItem.Name = "labelToolStripMenuItem";
            this.labelToolStripMenuItem.Size = new System.Drawing.Size(228, 24);
            this.labelToolStripMenuItem.Text = "Label";
            this.labelToolStripMenuItem.Click += new System.EventHandler(this.labelToolStripMenuItem_Click);
            // 
            // label1ToolStripMenuItem
            // 
            this.label1ToolStripMenuItem.CheckOnClick = true;
            this.label1ToolStripMenuItem.Name = "label1ToolStripMenuItem";
            this.label1ToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.label1ToolStripMenuItem.Text = "label1";
            // 
            // label2ToolStripMenuItem
            // 
            this.label2ToolStripMenuItem.CheckOnClick = true;
            this.label2ToolStripMenuItem.Name = "label2ToolStripMenuItem";
            this.label2ToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.label2ToolStripMenuItem.Text = "label2";
            // 
            // toolStripButtonToolStripMenuItem
            // 
            this.toolStripButtonToolStripMenuItem.BackColor = System.Drawing.Color.Red;
            this.toolStripButtonToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toolStripButtonToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Red;
            this.toolStripButtonToolStripMenuItem.Name = "toolStripButtonToolStripMenuItem";
            this.toolStripButtonToolStripMenuItem.Size = new System.Drawing.Size(26, 26);
            this.toolStripButtonToolStripMenuItem.Text = "X";
            this.toolStripButtonToolStripMenuItem.Click += new System.EventHandler(this.toolStripButtonToolStripMenuItem_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.Font = new System.Drawing.Font("Segoe UI Historic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(521, 1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 25);
            this.label12.TabIndex = 27;
            this.label12.Text = "Search";
            this.label12.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label12.UseCompatibleTextRendering = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1085, 721);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OCTOPUS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.Form1_PreviewKeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.RichTextBox RichTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_score;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxEtiket;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelSozlukAdi;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem MakeTrimToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ToolStripMenuItem clearAllScoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem learnStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DictionarytoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reverseDictionaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thirdDictionaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ModeStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dictionaryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem randomTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomWeightedTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alphabetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reverseAlphabetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomScoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomScoreWeightedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem label1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem label2ToolStripMenuItem;
        private System.Windows.Forms.TextBox scoreBox2;
        private System.Windows.Forms.TextBox scoreBox1;
        private System.Windows.Forms.TextBox scoreBox0;
        private System.Windows.Forms.TextBox scoreBox5;
        private System.Windows.Forms.TextBox scoreBox4;
        private System.Windows.Forms.TextBox scoreBox3;
        private System.Windows.Forms.TextBox scoreBox8;
        private System.Windows.Forms.TextBox scoreBox7;
        private System.Windows.Forms.TextBox scoreBox6;
        private System.Windows.Forms.TextBox scoreBox11;
        private System.Windows.Forms.TextBox scoreBox10;
        private System.Windows.Forms.TextBox scoreBox9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem structuresReverseToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripMenuItem sentencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sentencesReverseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artikelsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem openUserFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripButtonToolStripMenuItem;
    }
}

