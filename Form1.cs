﻿#undef SIM4  
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Net.Http;



namespace WindowsFormsApp5_0
{
    /*
    public class DoubleBufferedRichTextBox : RichTextBox
    {
        public DoubleBufferedRichTextBox()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.UpdateStyles();
        }
    } */


    public partial class Form1 : Form
    {
        string GetUserTxtFromFile()   // Read entire text file content in one string to txt_raw
        {
            string txt_raw;

            if (!File.Exists(strfilename))
            {
                string title = "Error 102";
                string message = "User txt file was not found! Please find and show it from File - Open User File Menu!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                return "";
            }

            txt_raw = File.ReadAllText(strfilename);

            return txt_raw;

        }

        //=====================================
        //private RoundButton roundButton;
        private Word currentWord; // Pointer-like reference to Word

        string GetTxtFromFile()   // Read entire text file content in one string to txt_raw
        {
            string txt_raw;

            if (!File.Exists(strfilename))
            {
                string title = "Error 102";
                string message = "Dictionary txt was not found! Please find and show it from File - Open Dictionary Menu!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                return "";
            }

            if (!File.Exists(strfilename))
            {
                strfilename = @Consts.DosyaYolu;
            }

            txt_raw = File.ReadAllText(strfilename);

            //using (var input = File.OpenText(@"C:\Users\user\source\repos\sozluk1.txt"));
            //using (var output = new SteamWriter("temp.txt"));
            // File.Delete(@"C:\Users\user\source\repos\sozluk1.txt");
            // File.Move("temp.txt", @"C:\Users\user\source\repos\sozluk1.txt"); 

            //Console.WriteLine("The original string is: \n{txt_raw}\n");
            return txt_raw;

            /*
            using (var sr = new StreamReader(strfilename))
            {
                txt_raw = sr.ReadToEnd();

                if (sr != null)
                {
                    sr.Close();
                    sr.Dispose();
                }
                    
                //
                //Dispose();
            }  */

            //txt_raw = File.ReadAllText(@"C:\Users\user\source\repos\sozluk1.txt");
            txt_raw = File.ReadAllText(strfilename);

            //using (var input = File.OpenText(@"C:\Users\user\source\repos\sozluk1.txt"));
            //using (var output = new SteamWriter("temp.txt"));
            // File.Delete(@"C:\Users\user\source\repos\sozluk1.txt");
            // File.Move("temp.txt", @"C:\Users\user\source\repos\sozluk1.txt"); 

            //Console.WriteLine("The original string is: \n{txt_raw}\n");
            return txt_raw;
        }

        //============================================
        //============================================
        void listboxUpDown(int varx)   // varx = 0 Down, 1 up;
        {
            //bool richtextbox_not_focused = !RichTextBox1.Focused;           
            
            //if (DictMode == Dict.WORDS)
            {
                if (modus == Mod.Random_Test || modus == Mod.RandomW_Test)
                {
                    RandomW_Test_GetNextItem();
                }
                else if (listBox1.Items.Count != 0 )
                {
                    bool actionX = false;
                    int _listBoxindex = listBox1.SelectedIndex;

                    if (varx == 0)
                    {
                        if(listBox1.SelectedIndex < listBox1.Items.Count - 1)
                        {
                            //listBox1.SelectedIndex++;
                            _listBoxindex++;
                            actionX = true;
                        }
                    }
                    else if(varx == 1 ) 
                    {
                        if( listBox1.SelectedIndex > 0 )
                        {
                            //listBox1.SelectedIndex--;
                            _listBoxindex--;
                            actionX = true;
                        }
                    }

                    if (actionX)
                    {
                        listBox1.SetSelected(_listBoxindex, true);
                        n2SelectedIndex = _listBoxindex;

                        //RichTextBox1.Focus();
                        Console.WriteLine("#2a listBox1.SelectedIndex = " + listBox1.SelectedIndex);
                        Console.WriteLine("#2a n2SelectedIndex = " + n2SelectedIndex + "\n");

                        //nSelectedIndex = listBox1.SelectedIndex;      // ((Word)listBox1.SelectedItem).Index;    
                        //currentWord = (Word)listBox1.SelectedItem;

                        //Console.WriteLine("#2b nSelectedIndex = " + nSelectedIndex + "\n");
                        //ignoreNextSelectedIndexChange = true;
                    }
                }
            }

        }
        //============================================




        //============================================
        void ProcessUserFileString(string txt_raw)
        {
            if (string.IsNullOrEmpty(txt_raw))
                return;

            List<Word> UserlistWords = new List<Word>();
            UserlistWords.Clear();

            int index_ilk_satir_sonu = txt_raw.IndexOf('\n');
            if (index_ilk_satir_sonu < 1)
                return;

            int UserSozlukVer = 0;

            if (txt_raw[0] == '+')  // başlık varsa, önce onu al
            {
                UserSozlukVer = 1;
            }
            else
            {
                SozlukAdi = txt_raw.Substring(0, index_ilk_satir_sonu - 1).Trim();
                txt_raw = txt_raw.Substring(index_ilk_satir_sonu + 1);

                string DictVer = "| DICT_VER";
                if (SozlukAdi.Contains(DictVer))
                {
                    int indx = SozlukAdi.IndexOf("| DICT_VER") + DictVer.Length;

                    if (indx < SozlukAdi.Length)
                    {
                        if (SozlukAdi[indx] == '2')
                        {
                            UserSozlukVer = 2;
                        }
                        else if (SozlukAdi[indx] == '3')
                        {
                            UserSozlukVer = 3; // future version of dict
                        }
                    }
                }
            }


            if (UserSozlukVer == 1)
            {
                int ind = txt_raw.IndexOf('\n');
                if (ind > 0)
                {
                    SozlukAdi = txt_raw.Substring(1, ind - 1).Trim();
                    txt_raw = txt_raw.Substring(ind + 1);
                }
            }

            //=======

            if (UserSozlukVer == 1)
            {
                string[] txt_araw;
                int ind2 = txt_raw.IndexOf(">>>>>>");
                if (ind2 != -1)
                {
                    string[] stringSeparators1 = new string[] { ">>>>>>\r\n", ">>>>>>\n", ">>>>>>\r" };
                    txt_araw = txt_raw.Split(stringSeparators1, StringSplitOptions.None);  //  .RemoveEmptyEntries);

                    char[] charSplits = { '\n' };

                    string[] deyim_txt_raw = txt_araw[0].Split(charSplits, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string deyim in deyim_txt_raw)
                    {
                        //Common1.AddDeyim(deyim); 4NOW
                    }

                    txt_raw = txt_araw[1];

                    string[] words_raw;
                    string[] stringSeparators = new string[] { "______\r\n", "______\n", "______\r" };
                    words_raw = txt_raw.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < words_raw.Count(); i++)
                    {
                        Common1.AddWordRaw(words_raw[i]);
                    }

                }

            }
            else if (UserSozlukVer == 2)
            {
                string[] words_raw;
                string[] stringSeparators = new string[] { "______\r\n", "______\n", "______\r" };
                UserlistWords.Clear();

                words_raw = txt_raw.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);  // Substring(index_ilk_satir_sonu + 1).

                for (int i = 0; i < words_raw.Length; i++)
                {
                    var encodedString = string.Join("\n", words_raw[i]);

                    Word wordToAdd = Word.Decode(encodedString);

                    string namescore = wordToAdd.Name;
                    string score_ = wordToAdd.Scores;
                    Dict dictx = wordToAdd.DictType;

                    var Sonuc = Common2.ScoreHsb(score_, namescore, dictx);
                    int xsc = Sonuc.Item1;
                    score_ = Sonuc.Item2;
                    wordToAdd.NameScore = Sonuc.Item3;

                    wordToAdd.Xscore = xsc;

                    wordToAdd.WeightRandom = Common1.CalcRandomWeight(xsc, score_);

                    //UserlistWords.AddWord(wordToAdd);

                    string name = wordToAdd.Name;

                    int addedWords = 0, mergedWords = 0;

                    // Check if a Word with the same name already exists in the list
                    Word existingWord = UserlistWords.Find(w => w.Name == name && w.DictType == dictx);
                    if (existingWord != null)
                    {
                        // Merge the labels if a Word with the same name is found
                        existingWord.MergeWords(wordToAdd);
                        ++addedWords;
                    }
                    else
                    {
                        // Add the new Word to the list if no match is found
                        UserlistWords.Add(wordToAdd);
                        ++mergedWords;
                    }

                    Console.WriteLine(" decode of the UserFile completed\n");
                    Console.WriteLine(" addedWords = " + addedWords + "\nmergedWords = " + mergedWords);
                }

                int changedScores = 0;

                foreach (var wordx in UserlistWords)
                {
                    string name = wordx.Name;
                    Dict dictx = wordx.DictType;

                    Word existingWord = WR.listWords.Find(w => w.Name == name && w.DictType == dictx);

                    if(existingWord == null)
                    {
                        existingWord = WR.listWords.Find(w => w.Name == name);
                        dictx = existingWord.DictType;
                    }

                    if (existingWord != null)
                    {
                        existingWord.Scores = wordx.Scores;
                        existingWord.Skors = wordx.Skors;
                        existingWord.NameScore = wordx.NameScore;
                        existingWord.Xscore = wordx.Xscore;
                        existingWord.WeightRandom = wordx.WeightRandom;

                        int changedIndex = WR.DictBindingLists[(int)dictx].IndexOf(existingWord);
                        if (changedIndex >= 0)
                        {
                            WR.DictBindingLists[(int)dictx].ResetItem(changedIndex);
                        }

                        ++changedScores;
                    }
                }

                string title = "Process Completed";
                string message = "User scores are transferred! ( TOTAL : " + changedScores.ToString() + " Changes )";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                return;
            }


            }

        //============================================
        public static string SozlukAdi;
        public static Sozluk sozluk = new Sozluk();
        public static Dict DictMode = new Dict();
        public static int SozlukVer = 0;
        public static int paddLen = 20;


        void ProcessString(string txt_raw)
        {   
            if (string.IsNullOrEmpty(txt_raw))
                return;

            WR.listWords.Clear();

            for (int i = 0; i < WR.DictBindingLists.Length; i++)
            {
                WR.DictBindingLists[i].Clear();
            }

            if (WR.list_words_orig != null)
                WR.list_words_orig.Clear();

            if (WR.list_words_history != null)
                WR.list_words_history.Clear();


            int index_ilk_satir_sonu = txt_raw.IndexOf('\n');
            if (index_ilk_satir_sonu < 1)
                return;

            int SozlukVer = 0;

            if (txt_raw[0] == '+')  // başlık varsa, önce onu al
            {
                SozlukVer = 1;
            }
            else
            {
                SozlukAdi = txt_raw.Substring(0, index_ilk_satir_sonu - 1).Trim();
                txt_raw = txt_raw.Substring(index_ilk_satir_sonu + 1);

                string DictVer = "| DICT_VER";
                if (SozlukAdi.Contains(DictVer))
                {
                    int indx = SozlukAdi.IndexOf("| DICT_VER") + DictVer.Length;

                    if (indx < SozlukAdi.Length)
                    {
                        if (SozlukAdi[indx] == '2')
                        {
                            SozlukVer = 2;
                        }
                        else if (SozlukAdi[indx] == '3')
                        {
                            SozlukVer = 3; // future version of dict
                        }
                    }
                }
            }

            //---------------------
            if (SozlukAdi == null || SozlukAdi.Contains("ENG") || SozlukAdi.Contains("ING") || SozlukAdi.Contains("İNG"))
            {
                sozluk = Sozluk.ENGLISH;
            }
            else if (SozlukAdi.Contains("ALM") || SozlukAdi.Contains("DEU"))
            {
                sozluk = Sozluk.DEUTSCH;
            }
            else
            {
                sozluk = Sozluk.ENGLISH;  // 4NOW
            }
            //---------------------

            if (SozlukVer == 0)
            {
                SozlukAdi = "DICTIONARY";
            }
            else if (SozlukVer == 1)
            {
                int ind = txt_raw.IndexOf('\n');
                if (ind > 0)
                {
                    SozlukAdi = txt_raw.Substring(1, ind - 1).Trim();
                    txt_raw = txt_raw.Substring(ind + 1);
                }
                else
                {
                    SozlukAdi = "DICTIONARY";
                }

                labelSozlukAdi.Text = SozlukAdi;
            }
            else if (SozlukVer == 2 || SozlukVer == 3)
            {
                //int indx = SozlukAdi.IndexOf("| DICT_VER");

                if      (sozluk == Sozluk.ENGLISH) labelSozlukAdi.Text = Consts.SozlukAdiEnglish;
                else if (sozluk == Sozluk.DEUTSCH) labelSozlukAdi.Text = Consts.SozlukAdiDeutsch;
            }

            //=======

            if (SozlukVer == 1)
            {
                string[] txt_araw;
                int ind2 = txt_raw.IndexOf(">>>>>>");
                if (ind2 != -1)
                {
                    string[] stringSeparators1 = new string[] { ">>>>>>\r\n", ">>>>>>\n", ">>>>>>\r" };
                    txt_araw = txt_raw.Split(stringSeparators1, StringSplitOptions.None);  //  .RemoveEmptyEntries);

                    char[] charSplits = { '\n' };

                    string[] deyim_txt_raw = txt_araw[0].Split(charSplits, StringSplitOptions.RemoveEmptyEntries);

                    //WR.DictBindingLists[(int)Dict.WORDS_REVERSE].Clear();               //list_words_ters_orig.Clear();

                    //if (listWords[(int)Dict.WORDS_REVERSE] != null)
                    //{
                    //    listWords[(int)Dict.WORDS_REVERSE].Clear();               //list_words_ters_orig.Clear();
                    //}

                    foreach (string deyim in deyim_txt_raw)
                    {
                        Common1.AddDeyim(deyim);
                    }

                    txt_raw = txt_araw[1];
                }

                string[] words_raw;
                string[] stringSeparators = new string[] { "______\r\n", "______\n", "______\r" };
                words_raw = txt_raw.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                //Show(words_raw);

                /*   foreach (string word_ in words_raw)   // her bir words_raw[] öğesini parçalayıp ad, anlam ve not olarak list'in içerisine at!
                   {
                       listWords[(int).DictMode].Name.Add(word_);
                   }  */
                

                
                for (int i = 0; i < words_raw.Count(); i++)
                {
                    Common1.AddWordRaw(words_raw[i]);
                }
            }
            else if (SozlukVer == 2)
            {
                string[] words_raw;
                string[] stringSeparators = new string[] { "______\r\n", "______\n", "______\r" };
                WR.listWords.Clear();

                words_raw = txt_raw.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);  // Substring(index_ilk_satir_sonu + 1).

                for (int i = 0; i < words_raw.Length; i++)
                {
                    var encodedString = string.Join("\n", words_raw[i]);

                    Word wordToAdd = Word.Decode(encodedString);

                    string namescore = wordToAdd.Name;
                    string score_ = wordToAdd.Scores;
                    Dict dictx = wordToAdd.DictType;

                    var Sonuc = Common2.ScoreHsb(score_, namescore, dictx);
                    int xsc = Sonuc.Item1;
                    score_ = Sonuc.Item2;
                    wordToAdd.NameScore = Sonuc.Item3;

                    wordToAdd.Xscore = xsc;

                    wordToAdd.WeightRandom = Common1.CalcRandomWeight(xsc, score_);                    

                    WR.AddWord(wordToAdd);                    
                }
            }

            int xno = WR.listWords.Count();
            string strx = $"TOTAL : {xno} Elements";
            UpdateStatus3(strx);
        }
        //============================================
        //============================================

        // Example: Method to update a word in the BindingList and reflect changes in ListBox
        private void UpdateWord(Dict dictType, Word word, string newName, string newNameScore)
        {
            var index = WR.FindWordIndex(dictType, word);
            if (index >= 0)
            {
                var bindingList = WR.DictBindingLists[(int)dictType];
                bindingList[index].Name = newName;
                bindingList[index].NameScore = newNameScore;
                // If the ListBox is not updating automatically, you can force it to refresh
                listBox1.Refresh();
            }
        }

        //============================================
        //============================================
        // Example: Update the status strip text dynamically
        private void UpdateStatus(string message1)
        {
            toolStripStatusLabel1.Text = message1;
        }

        private void UpdateStatus(string message1, string message3)
        {
            toolStripStatusLabel1.Text = message1;
            toolStripStatusLabel3.Text = message3;
        }

        
        private void UpdateStatusRandomWeight(string message2)
        {
            toolStripStatusLabel2.Text = message2;
        }

        private void UpdateStatus3(string message3)
        {
            toolStripStatusLabel3.Text = message3;
        }

        //============================================
        //============================================        
        public void RefreshSearchList()
        {

            WR.list_words_search = WR.DictBindingLists[0];  // 4NOW VER7.2

            WR.list_words_search_tmp = WR.list_words_search
                        .OrderBy(item => item.Name)
                        .ThenBy(item => item.Xscore)
                        .ToList();
            WR.list_words_search = new BindingList<Word>(WR.list_words_search_tmp);

        }

        //============================================
        void InitLists()
        {
            //BuildRandomWList();
            CalcTRW();

            RefreshSearchList();
            //ShowListOrig(list_words_orig);
            if (WR.listWords.Count != 0)
            {
                label4.Text = WR.listWords.Count().ToString() + " Words in Main Dictionary" + '\n';

                WR.list_words_orig = WR.DictBindingLists[(int)Dict.WORDS];    //new BindingList<Word>();  // V7 4NOW DICTI.MAIN


                WR.list_words_orig
                        .OrderBy(item => item.Id);
                        //.ToList();


                WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderBy(item => item.Id)
                        .ToList();
                WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);  


            }
            else
            {
                label4.Text = "0 Kelime";
            }

        }

        //============================================
        void InitListBox()
        {
            Refresh_ListBox(WR.list_words_orig, 2);
            listBox1.SelectionMode = SelectionMode.One;

            if (listBox1.Items.Count > 0)
            {
                listBox1.SetSelected(0, true);

                getListBoxSelected(); // Ver7.2.16
                //nSelectedIndex = ((Word)listBox1.SelectedItem).Index; Ver7.2.16
                //currentWord = (Word)listBox1.SelectedItem;            Ver7.2.16

                Console.WriteLine("#01 InitListBox PROCESSED" );
                Console.WriteLine("#01 nSelectedIndex = " + nSelectedIndex + "\n");
            }
        }

        void InitListBoxP2()
        {
            Refresh_ListBox(WR.list_words_orig, 2);
            listBox1.SelectionMode = SelectionMode.One;

            if (listBox1.Items.Count > 0)
            {
                selListBoxFirstItem();
                // V7 SelectedIndex2 = listWords[(int)Dict.WORDS].IndexOf((Word)listBox1.SelectedItem);             //((Word)listBox1.SelectedItem).Index;
            }
            else
            {
                // V7 SelectedIndex2 = -1;
            }
        }

        //============================================
        Mod modus = new Mod();
        Sort sort = new Sort();

        void InitComboboxes()
        {   /*                                    // Sort Type
            comboBox_sort.Items.Add("Txt");    // Database
            comboBox_sort.Items.Add("Alfabe");  // Alphabet
            comboBox_sort.Items.Add("Ters_Alfabe"); // Reverse Alphabet
            comboBox_sort.Items.Add("Puan");        // Score
            comboBox_sort.Items.Add("Puan_Rastgele");  // Random Score 
            comboBox_sort.Items.Add("Puan_Agirlik");   // Random Score Weighted
            comboBox_sort.Items.Add("Rastgele");       // Random  
            comboBox_sort.Items.Add("History");        // History
            comboBox_sort.Items.Add("Etiket");         // Label

            comboBox_sort.SelectedItem = "Txt";  */

            /*
            for (int i = 0; i < WR.listWords.Count; i++)
            {
                WR.listWords[i] = new List<Word>();
            }    */

            sort = Sort.Database;
            SortChecked();

            /*
            comboBox_mode.Items.Add(GetMod(Mod.Dictionary));
            comboBox_mode.Items.Add(GetMod(Mod.Test));
            comboBox_mode.Items.Add(GetMod(Mod.Random_Test));
            comboBox_mode.Items.Add(GetMod(Mod.RandomW_Test));

            comboBox_mode.SelectedIndex = (int)Mod.Dictionary;                   //.SelectedItem = "Sozluk";
            */
            //comboBoxEtiket.Enabled = false;

            modus = Mod.Dictionary;

            DictMode = Dict.WORDS;
            boxPositions();
            DictChanged();            

            dictionaryToolStripMenuItem1.Checked = true;
            testToolStripMenuItem1.Checked = false;
            randomTestToolStripMenuItem.Checked = false;
            randomWeightedTestToolStripMenuItem.Checked = false;
        }
        //============================
        void InitMenuEtiket()
        {
            labelToolStripMenuItem.DropDownItems.Clear();
            //labelToolStripMenuItem.DropDownItems.Add("");
            //comboBox1.Items.Clear();
            //comboBox1.Items.Add("");
            foreach (ToolStripMenuItem childItem in labelToolStripMenuItem.DropDownItems)
            {
                childItem.Click -= ChildItemClickHandler;
            }

            //if (WR.DictBindingLists[(int)DictMode].Count == 0 ) return;

            for (int i = 0; i < WR.DictBindingLists[(int)DictMode].Count; i++)
            {
                bool found = false;
                string[] labelsArray = WR.DictBindingLists[(int)DictMode][i].GetLabels();

                for (int z = 0; z < labelsArray.Length; z++)
                {


                    if (string.IsNullOrEmpty(labelsArray[z])) continue;

                    ToolStripMenuItem childItemBackup = new ToolStripMenuItem();
                    foreach (ToolStripMenuItem childItem in labelToolStripMenuItem.DropDownItems)
                    {
                        if (childItem.Text.Equals(labelsArray[z], StringComparison.OrdinalIgnoreCase))
                        {
                            // Item found
                            found = true;
                            break;
                        }
                        childItemBackup = childItem;
                    }

                    if (!found)
                    {
                        // Perform actions when the item is not found
                        labelToolStripMenuItem.DropDownItems.Add(labelsArray[z]);
                        childItemBackup.CheckOnClick = true;
                        childItemBackup.Checked = false;

                        childItemBackup.Click += ChildItemClickHandler;

                    }
                }

            }

            /*

            for (int i = 0; i < listWords[(int)DictMode].Count; i++)
            {
                if (!labelToolStripMenuItem.DropDownItems.Contains(list_words[i].Etiket) && !string.IsNullOrEmpty(listWords[(int)DictMode][i].Etiket))
                {
                    comboBox1.Items.Add(listWords[(int)DictMode][i].Etiket);
                }
            }

            comboBoxEtiket.Items.Clear();
            comboBoxEtiket.Items.Add("");
            for (int i = 0; i < listWords[(int)DictMode].Count; i++)
            {
                if (!comboBoxEtiket.Items.Contains(listWords[(int)DictMode][i].Etiket) && !string.IsNullOrEmpty(list_words[i].Etiket))
                {
                    comboBoxEtiket.Items.Add(listWords[(int)DictMode][i].Etiket);
                }
            } */
        }

        //==============================
        void InitUserInfo()   
        {
            string strx = GetUserTxtFromFile();  // Ver7.2
            ProcessUserFileString(strx);            // Ver7.2 


        }


        void InitGenAsync()   // async Task
        {
            DictMode = Dict.WORDS;
                       
            listBox1.DataSource = null;
            listBox1.Items.Clear();

            //string apiUrl = "https://c362-93-234-112-43.ngrok-free.app/word-list"; // Replace with your API URL
            // await GetDataFromApi(apiUrl);

            string strx = GetTxtFromFile();  // Ver7.2
            ProcessString(strx);            // Ver7.2 
            //WR.LoadWordsFromFile(strfilename);  // Ver7.2

            InitLists();
            InitListBox();
            //InitListBoxP2();

            //label1.Text = "";
            richTextBox2.Text = "";
            textBox1.Text = "";
            //label8.Text = "";
            UpdateStatusRandomWeight("");
            this.Text = "OCTOPUS" + " V" + Consts.Version + "                  " + SozlukAdi;

            //anlam_goster = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            //DoubleBuffered = true;
            TextEditor(RichTextBox1);
            //RichTextBox1.ShortcutsEnabled = true;
            //RichTextBox1.AddContextMenu();

            InitMenuEtiket();

            testToolStripMenuItem.Checked = true;
            reverseDictionaryToolStripMenuItem.Checked = false;
            thirdDictionaryToolStripMenuItem.Checked = false;
            
            DictMode = Dict.WORDS;
            DictChanged();

            boxPositions();
            
            selListBoxFirstItem();
            //TersSozcukMod = false;

            // Bind the SelectedValueChanged event to our handler for it.
            //ListBox1.SelectedValueChanged +=
            //    new EventHandler(ListBox1_SelectedValueChanged);
        }

        //============================================
        //============================================
        /*
        public static class WR
        {
            public static List<Word>[] listWords = new List<Word>();  // [6];

            // list_words yerine listWords[x] var.
            public static BindingList<Word> list_words_orig = new BindingList<Word>();

            private List<Word> list_words_orig_tmp = new List<Word>();

            private BindingList<Word> list_words_search = new BindingList<Word>();
            private List<Word> list_words_search_tmp = new List<Word>();

            private BindingList<Word> list_words_search2 = new BindingList<Word>();

            private BindingList<Word> list_words_etiket = new BindingList<Word>();
            private List<Word> list_words_etiket_tmp = new List<Word>();

            private BindingList<Word> list_words_history = new BindingList<Word>();
        }  */
        //============================================

        //public static List<Word> list_words = new List<Word>();
        //public static BindingList<NameNameScoreIndex> list_words_orig = new BindingList<NameNameScoreIndex>();
        //private List<NameNameScoreIndex> list_words_orig_tmp = new List<NameNameScoreIndex>();

        //private BindingList<NameNameScoreIndex> list_words_search = new BindingList<NameNameScoreIndex>();
        //private List<NameNameScoreIndex> list_words_search_tmp = new List<NameNameScoreIndex>();

        //private BindingList<NameNameScoreIndex> list_words_search2 = new BindingList<NameNameScoreIndex>();        

        // private BindingList<NameNameScoreIndex> list_words_etiket = new BindingList<NameNameScoreIndex>();
        //private List<NameNameScoreIndex> list_words_etiket_tmp = new List<NameNameScoreIndex>();

        //private BindingList<NameNameScoreIndex> list_words_history = new BindingList<NameNameScoreIndex>();

        /*
        public static BindingList<NameNameScoreIndex> list_words_ters_orig = new BindingList<NameNameScoreIndex>();
        private List<NameNameScoreIndex> list_words_ters_orig_tmp = new List<NameNameScoreIndex>();

        private BindingList<NameNameScoreIndex> list_words_ters_search = new BindingList<NameNameScoreIndex>();
        private List<NameNameScoreIndex> list_words_ters_search_tmp = new List<NameNameScoreIndex>();
        */

        private Stack<string> undoStack;
        private Stack<string> redoStack;
        private bool isUndoing;

        //============================================
        //============================================
        //============================================

        public Form1()
        {
            InitializeComponent();


            undoStack = new Stack<string>();
            redoStack = new Stack<string>();
            isUndoing = false;
            /*
          roundButton = new RoundButton();
          //roundButton.Size = new Size(24, 24);  // Adjust the size as needed
          roundButton.Location = new Point(742, 4);
          //roundButton.BackColor = Color.Red;
          //roundButton.ForeColor = Color.White;
          //roundButton.Text = "X";
          //roundButton.Font = new Font("Arial", 12, FontStyle.Bold);
          ro undButton.Click += RoundButton_Click;
          this.Controls.Add(roundButton);

          roundButton.BringToFront();
          */

            //this.Controls.AddRange(new Control[] { Listbox2, label1, textBox1 });
            Console.WriteLine("Hello World!");

            InitComboboxes();

            /* 4SIM
            List<int> originalList = new List<int> { 1, 2, 3, 4, 5 };
            List<int> secondList = originalList; // Assigning the original list to the second list

            Console.WriteLine(string.Join(", ", originalList)); // Output: 1, 2, 3
            Console.WriteLine(string.Join(", ", secondList)); // Output: 2, 3, 10

            secondList[0] = 5; // Modifying an element in the second list
            Console.WriteLine(string.Join(", ", originalList)); // Output: 1, 2, 3
            Console.WriteLine(string.Join(", ", secondList)); // Output: 2, 3, 10

            secondList.Sort(); // Sorting the second list
            secondList[0] = 10; // Modifying an element in the second list

            Console.WriteLine(string.Join(", ", originalList)); // Output: 1, 2, 3
            Console.WriteLine(string.Join(", ", secondList)); // Output: 2, 3, 10
            */


            //Image newImage = (Image)WindowsFormsApp5_0.Properties.Resources.ResourceManager.GetObject("Oct4.png");               //Image.FromFile(Consts.ImagePath);
            //Image newImage = (Image)Octopus.Properties.ResourceManager.GetObject("Oct4.png");               //Image.FromFile(Consts.ImagePath);

            //System.Reflection.Assembly myAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            //Stream myStream = myAssembly.GetManifestResourceStream("Oct4");      //("EmbeddingExample.Oct4.png");
            //Bitmap image = new Bitmap(myStream);

            //Image newImage = (Image)Octopus.Properties.Resources.ResourceManager.GetObject("Oct4.png");
            //pictureBox1.Image = newImage;  //newImage;

            RichTextBox1.Select(0, 1);
            Common.rTextBoxFont = RichTextBox1.SelectionFont;

            // Create Point for upper-left corner of image.
            //Point ulCorner = new Point(100, 100);

            // Draw image to screen.
            //e.Graphics.DrawImage(newImage, ulCorner);

            this.Show();

            InitGenAsync();
        }
        //================================================
        public static int nSelectedIndex = -1; // nSelectedIndex_ex; Ver7.2.16
                                               //bool text_formatting;

        public static int n2SelectedIndex = -1;

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("#88 listBox1_SelectedIndexChanged STARTED!");
            Console.WriteLine("#400 isNavigatingListBox = " + isNavigatingListBox + "\n");

            if (isNavigatingListBox)
            {
                Console.WriteLine("#87 listBox1_SelectedIndexChanged ENDED! (not processed!)"); 
                return;
            }
            else if (isProcessingScoreTextChanged)
            {
                Console.WriteLine("#87b listBox1_SelectedIndexChanged ENDED!(not processed!)");
                return;
            }


            isNavigatingListBox = true;

            Console.WriteLine("#89 n2SelectedIndex = " + n2SelectedIndex);
            Console.WriteLine("#89 listBox1.SelectedItem = " + listBox1.SelectedIndex);

            if (modus == Mod.Random_Test || modus == Mod.RandomW_Test)
            {
                isNavigatingListBox = false;
                Console.WriteLine("#86 listBox1_SelectedIndexChanged ENDED!");
                return;
            }

            if (listBox1.SelectedIndex < 0)
            {
                isNavigatingListBox = false;
                Console.WriteLine("#86b listBox1.SelectedIndex = " + listBox1.SelectedIndex);
                Console.WriteLine("#86b listBox1_SelectedIndexChanged ENDED!");
                return;
            }
             

            getListBoxSelected();            
            sim1();

            if ( currentWord == null )  
            {
                isNavigatingListBox = false;
                Console.WriteLine("#88 listBox1_SelectedIndexChanged ENDED!" + "\n");
                return;
            }
            
            
                set_label1(0);
           
                score_eklendi = false;

                undoStack.Clear();
                redoStack.Clear();

                if (modus == Mod.Dictionary)
                {
                    //nSelectedIndex_prev = -1;

                    //string strLabelx0 = WR.DictBindingLists[(int)DictMode][nSelectedIndex].GetLabelByIndex(0);
                    //if (String.IsNullOrEmpty(strLabelx0) == false)
                    {
                        //labelEtiket.Text = '#' + listWords[(int)DictMode][nSelectedIndex].Etiket;
                    }

                    RichTextBox1.Text = currentWord.Content0;      // WR.DictBindingLists[(int)DictMode][nSelectedIndex].Content0;
                    
                    comboBox1.SelectedItem = currentWord.GetLabelByIndex(0);            // WR.DictBindingLists[(int)DictMode][nSelectedIndex].GetLabelByIndex(0); 
                    //comboBox1.SelectedItem = null;

                    textBoxEtiket_new_txt_entered = true;
                    textBoxEtiket.Text = currentWord.GetLabelByIndex(0);            //WR.DictBindingLists[(int)DictMode][nSelectedIndex].GetLabelByIndex(0);

                    calc_show_ppm(currentWord);
                }
                else
                {
                    RichTextBox1.Clear();   //  Text = ""; Ver7.2.26

                
                richTextBox2.Clear();   // Ver7.2.26
                    comboBox1.SelectedItem = null;
                    //anlam_goster = false;

                    textBoxEtiket.Text = "";

                    //textBox_score_prev = "";
                    //textBox_score.Text = "";
                }

                textBox_score_prev = "";
                textBox_score_make();

                //nSelectedIndex_ex = nSelectedIndex;
               
                RichTextBox1.Select(0, 0);
                //RichTextBox1.Focus();
                RichTextBox1.ScrollToCaret();            

            listBox1.Focus();
                isNavigatingListBox = false;

            Console.WriteLine("#10 listBox1.SelectedItem = " + listBox1.SelectedIndex +"\n" );
            Console.WriteLine("#83 listBox1_SelectedIndexChanged ENDED!\n");

        }
        //================================================
        void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (!isUndoing)
            {
                undoStack.Push(RichTextBox1.Text);
                redoStack.Clear();
            }

            Console.WriteLine("richTextBox1_TextChanged() Executed\n");
            string str_tc = RichTextBox1.Text.ToString();
            if (!string.IsNullOrEmpty(str_tc))
            {
                //System.Timers.Timer timer1 = new System.Timers.Timer(5000);
                //timer1.AutoReset = false;
                //timer1.Enabled = true;
                //timer1.Elapsed += new ElapsedEventHandler(OnTimerEvent);

                /* Ver 7.2.20
                if (nSelectedIndex != nSelectedIndex_prev)
                {
                    TextEditor(RichTextBox1);
                }
                else
                {
                    string namex_ = "";
                    if (nSelectedIndex != -1)
                    {
                        namex_ = currentWord.Name;  // WR.DictBindingLists[(int)DictMode][nSelectedIndex].Name;           //.NameScore.Substring(0,17).Trim();
                    }

                    Common.PartialEditor2(RichTextBox1, namex_);
                    currentWord.Content0 = RichTextBox1.Text.ToString();                    // WR.DictBindingLists[(int)DictMode][nSelectedIndex].Content0 = RichTextBox1.Text.ToString();
                }  */

                if (nSelectedIndex != -1)  // Ver 7.2.20
                {
                    TextEditor(RichTextBox1);

                    Common.PartialEditor2(RichTextBox1, currentWord.Name);
                    currentWord.Content0 = RichTextBox1.Text.ToString();                    // WR.DictBindingLists[(int)DictMode][nSelectedIndex].Content0 = RichTextBox1.Text.ToString();                    
                }


                //text_formatting = false;
                // nSelectedIndex_prev = nSelectedIndex;
                //if(nSelectedIndex != nSelectedIndex_prev)  // && text_formatting != false) 4now
            }
        }
        //========================
        void OnTimerEvent(object source, EventArgs e)
        {


        }

        //========================
        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
            /* V7
            Console.WriteLine("richTextBox2_TextChanged() Executed\n");
            string str_tc = richTextBox2.Text.ToString();
            if (!string.IsNullOrEmpty(str_tc))
            {
                TextEditor2(richTextBox2);
                //listWords[(int)DictMode][nSelectedIndex].Meaning = richTextBox2.Text.ToString();
            }
            */
        }

        //========================
        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {
            /* V7
            Console.WriteLine("richTextBox2_TextChanged() Executed\n");
            string str_tc = richTextBox3.Text.ToString();
            if (!string.IsNullOrEmpty(str_tc))
            {
            TextEditor2(richTextBox3);
            //listWords[(int)DictMode][nSelectedIndex].Meaning = richTextBox2.Text.ToString();
            }
            */
        }

        //========================
        void comboBox_sort_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("comboBox_sort_SelectedIndexChanged() Executed\n");
            //ComboBoxSortPanel1();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBoxSortPanel2();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("comboBox1_SelectedIndexChanged() Executed\n");
            //MainComboBoxPanel1();
        }


        private void comboBoxMod2_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainComboBoxPanel2();
        }


        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //ComboBoxEtiket();
        }

        // Click event handler for child items
        private void ChildItemClickHandler(object sender, EventArgs e)
        {
            // Handle the click event of child items here
            ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
            string itemName = clickedItem.Text;

            // Perform actions based on the clicked item
            LabelxHandler(itemName);
        }

        public static bool dictFileOpened = false;
        public static string strfilename = Consts.DosyaYolu;
        void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Text Files |*.txt";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //richTextBox1.Text = File.ReadAllText(openFileDialog1.FileName);
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    strfilename = openFileDialog1.FileName;
                    myStream.Close();
                    this.Refresh();
                    //string filetext = File.ReadAllText(strfilename);
                    //richTextBox1.Text = filetext;
                    dictFileOpened = true;
                    InitGenAsync();
                }
                else
                {
                    myStream.Close();
                    string title = "Error 101";
                    string message = "Dictionary txt was not found! Please find and show it from File - Open Dictionary Menu!";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                }

            }
        }

        private bool isProgrammaticallyUpdated = false;
        private bool isTextSelected = false; // Metnin seçili olup olmadığını kontrol etmek için bayrak.


        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("toolStripTextBox1 clicked!");

            // Eğer metin seçiliyse (ve bayrak ayarlanmışsa), seçimi kaldır.
            if (isTextSelected)
            {
                toolStripTextBox1.SelectionLength = 0; // Seçimi kaldır.
                toolStripTextBox1.SelectionStart = toolStripTextBox1.Text.Length; // İmleci sona taşı.
                isTextSelected = false; // Seçim bayrağını sıfırla.
                Console.WriteLine("Text selection cleared!");
                return;
            }


            // Eğer metin boş değilse ve belirli bir uzunluğun üstündeyse metni seç.
            //!isProgrammaticallyUpdated &&
            if (!string.IsNullOrEmpty(toolStripTextBox1.Text) && toolStripTextBox1.Text.Length > 0)
            {                
                toolStripTextBox1.SelectAll();
                isTextSelected = true; // Seçim bayrağını ayarla.
                Console.WriteLine("toolStripTextBox1 SelectAll processed!");                                    
            }

        }

        //List<string> listcollection = new List<string>();
        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            Console.WriteLine("toolStripTextBox1_TextChanged() Executed\n");

            // Eğer bayrak aktifse, SearchBoxUpdate çağırmadan çık.
            if (isProgrammaticallyUpdated)
            {
                Console.WriteLine("Programmatic update detected, skipping SearchBoxUpdate().");
                //SearchBoxUpdate();

                return;
            }

            // Kullanıcı girişi varsa, seçim bayrağını sıfırla.
            isTextSelected = false;

            SearchBoxUpdate();
            toolStripTextBox1.Focus();

        }

        public static string KelimeSayisi, TersKelimeSayisi, YapilarSayisi, TersYapilarSayisi, CumlelerSayisi, TersCumlelerSayisi, ArtikelSayisi, NotlarSayisi, TotalSayi; 
        public static int TestSayisi;
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)  // Help About
        {
            KelimeSayisi        = "Words             : " + WR.DictBindingLists[(int)Dict.WORDS].Count().ToString();
            TersKelimeSayisi    = "Reverse Words     : " + WR.DictBindingLists[(int)Dict.WORDS_REVERSE].Count().ToString();
            YapilarSayisi       = "Structures        : " + WR.DictBindingLists[(int)Dict.STRUCTURES].Count().ToString();
            TersYapilarSayisi   = "Reverse Structures: " + WR.DictBindingLists[(int)Dict.STRUCTURES_REVERSE].Count().ToString();
            CumlelerSayisi      = "Sentences         : " + WR.DictBindingLists[(int)Dict.SENTENCES].Count().ToString();
            TersCumlelerSayisi  = "Reverse Sentences : " + WR.DictBindingLists[(int)Dict.SENTENCES_REVERSE].Count().ToString();
            ArtikelSayisi       = "Artikels          : " + WR.DictBindingLists[(int)Dict.ARTIKELS].Count().ToString();
            NotlarSayisi        = "Notes             : " + WR.DictBindingLists[(int)Dict.NOTES].Count().ToString();
            TotalSayi           = "TOTAL             : " + WR.listWords.Count.ToString();

            TestSayisi = 0;
            foreach (var lws in WR.listWords )                 // WR.DictBindingLists[(int)DictMode])
            {
                if (!String.IsNullOrEmpty(lws.Scores) && lws.Scores != "-")
                    TestSayisi += lws.Scores.Length;
            }

            FormAbout formAbout = new FormAbout();
            //formAbout.Parent = Form1;
            formAbout.StartPosition = FormStartPosition.CenterParent;

            //formAbout.FormClosing += new FormClosingEventHandler(ChildFormClosing);
            formAbout.ShowDialog();    // ShowForm();
        }

        //===================================================
        private void Button_save_Click(object sender, EventArgs e) // Save1
        {
            SaveProcess(true);
        }

        private void button2_Click(object sender, EventArgs e)  // Save2
        {
            SaveProcess(true);
        }

        //===================================================
        bool textBoxEtiket_new_txt_entered = false;
        private void textBoxEtiket_TextChanged(object sender, EventArgs e)
        {
            string etiket_ = textBoxEtiket.Text.ToString();

            if (String.IsNullOrEmpty(textBoxEtiket.Text.ToString())) { return; }

            if (textBoxEtiket_new_txt_entered != false)
            {
                textBoxEtiket_new_txt_entered = false;
                return;
            }

            currentWord.AddLabel(etiket_);   // WR.DictBindingLists[(int)DictMode][nSelectedIndex].AddLabel( etiket_);
        }

        //===================================================

        string textBox_score_prev = "";
        private void textBox_score_TextChanged(object sender, EventArgs e)
        {
            score_TextChanged();
        }
        //=======================================
        private void textBox_score_Click(object sender, EventArgs e)
        {
            Console.WriteLine("textBox_score_Click() Executed\n");
            cursor_to_end(textBox_score);
            textBox_score.Focus();
        }


        private void textBox_score_MouseClick(object sender, MouseEventArgs e)
        {
            Console.WriteLine("textBox_score_MouseClick() Executed\n");
            textBox_score_came();

        }



        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("toolStripButton1_Click() Executed\n");
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        //================================================

        private void textBox_score_KeyUp(object sender, KeyEventArgs e)
        {
            //Console.WriteLine("textBox_score_KeyUp() Executed\n");
        }

        private void textBox_score_KeyDown(object sender, KeyEventArgs e)
        {
            //Console.WriteLine("textBox_score_KeyDown() Executed\n");
        }

        private void textBox_score_KeyPress(object sender, KeyPressEventArgs e)
        {

        }


        private void listBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Console.WriteLine("listBox1_KeyPress() Executed\n");
            /*if(e.KeyChar == (char)Keys.Print)  // * tuşu
            {
                cursor_to_end(textBox_score);
            }  */
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Console.WriteLine("Form1_KeyPress() Executed\n");
            if (e.KeyChar == (char)Keys.Print)  // * tuşu
            {
                Console.WriteLine("Key : * \n");
                textBox_score_came();
                cursor_to_end(textBox_score);
            }
            //else if (e.KeyChar == (char)Keys.Right || e.KeyChar == (char)Keys.Left)
            //{
            //    Console.WriteLine("Key : Right or Left * \n");
            //    RandomW_Test_GetNextItem();                
            //}
        }
        //========================================

        //private bool ignoreNextSelectedIndexChange = false;
        private bool isKeyDownEventProcessing = false;
        private bool isNavigatingListBox = false;

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine("#70 Form1_KeyDown STARTED! " + e.KeyData.ToString() + "key pressed\n");

            //if (isNavigatingListBox)
            //{
            //    Console.WriteLine("#70 Form1_KeyDown ENDED! isNavigatingListBox = False\n");
            //    return;
            //}
            if(isKeyDownEventProcessing)
            {
                Console.WriteLine("#70b Form1_KeyDown ENDED! isKeyDownEventProcessing = False\n");
                return;
            }

            isKeyDownEventProcessing = true;
            //if (isKeyDownEventProcessing)
            //    return;

            Console.WriteLine(e.KeyData.ToString() + "key pressed\n");            
            /*
            if ( e.KeyData == Keys.Right 
              || e.KeyData == Keys.Left 
              || e.KeyData == Keys.Down
              || e.KeyData == Keys.Up)  // 
            {
                //RandomW_Test_GetNextItem();
            }  */

            bool richtextbox_not_focused = !RichTextBox1.Focused;
            bool listbox1_not_focused = !listBox1.Focused;

            if ((e.KeyData == (Keys.Control | Keys.Down) && listbox1_not_focused)
            || (e.KeyData == Keys.Down && listbox1_not_focused && richtextbox_not_focused))
            {
                Console.WriteLine("Listbox down command executed\n");
                listboxUpDown(0);  // down

            }
            else if ((e.KeyData == (Keys.Control | Keys.Up) && listbox1_not_focused)
                || (e.KeyData == Keys.Up && listbox1_not_focused && richtextbox_not_focused))
            {
                Console.WriteLine("Listbox up command executed\n");
                listboxUpDown(1);  // up
            }
            else if (e.KeyData == (Keys.Alt | Keys.Left))
            {
                Console.WriteLine("Control + Left\n");
                DictModeMinus();  //  Alt+Left
            }
            else if (e.KeyData == (Keys.Alt | Keys.Right))
            {
                Console.WriteLine("Control + Right\n");
                DictModePlus();  //  Alt+Right
            }

            else if (e.KeyData == Keys.PageUp)
            {
                Console.WriteLine("Form1_PageUp() pressed\n");
                RandomW_Test_GetNextItem();
            }
            else if (e.KeyData == Keys.PageDown)
            {
                Console.WriteLine("Form1_PageDown() pressed\n");
                RandomW_Test_GetNextItem();
            }

            isKeyDownEventProcessing = false;
            //isKeyDownEventProcessing = false;

            //    else if ((e.KeyData == (Keys.Control | Keys.Z))
            //            && RichTextBox1.Focused)
            //    {
            //        Undo();
            //    }
            /* else if (e.KeyData == Keys.Insert)
            {
                Console.WriteLine("Form1_PageDown() pressed\n");
                //if (modus == Mod.Test || modus == Mod.Random_Test || modus == Mod.RandomW_Test)
                {
                    textBox_score_came();
                    cursor_to_end(textBox_score);
                }
            }  */
            Console.WriteLine("#75 Form1_KeyDown ENDED!\n");
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Insert))
            {
                textBox_score_came();
                cursor_to_end(textBox_score);
                return true;

            }
            else if (!RichTextBox1.Focused
               && (  keyData == Keys.NumPad0 || keyData == Keys.D0
                  || keyData == Keys.NumPad1 || keyData == Keys.D1
                  || keyData == Keys.NumPad2 || keyData == Keys.D2
                  || keyData == Keys.NumPad3 || keyData == Keys.D3
                  || keyData == Keys.NumPad4 || keyData == Keys.D4
                  || keyData == Keys.NumPad5 || keyData == Keys.D5 ))
            {
                string x_key = "0";
                if (     keyData == Keys.NumPad0 || keyData == Keys.D0) x_key = "0";
                else if (keyData == Keys.NumPad1 || keyData == Keys.D1) x_key = "1";
                else if (keyData == Keys.NumPad2 || keyData == Keys.D2) x_key = "2";
                else if (keyData == Keys.NumPad3 || keyData == Keys.D3) x_key = "3";
                else if (keyData == Keys.NumPad4 || keyData == Keys.D4) x_key = "4";
                else if (keyData == Keys.NumPad5 || keyData == Keys.D5) x_key = "5";

                textBox_score.Text = x_key;

                /*                
                score_TextChanged();

                if (TersSozcukMod == false)
                    {
                        textBox_score_came();
                        cursor_to_end(textBox_score);
                    }
                    else
                    {
                        textBox_score2_came();
                        cursor_to_end(textBox_score2);
                    }
                */
                return true;

            }
            else if ((keyData == (Keys.Control | Keys.Z))
        && RichTextBox1.Focused)
            {
                Undo();
                return true;
            }
            else if ((keyData == (Keys.Control | Keys.Y))
        && RichTextBox1.Focused)
            {
                Redo();
                return true;
            }
            else
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        private void Form1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        //static int pos_x = 200;
        //static int pos_y = 200;
        private void buttonSozcukEkle_Click(object sender, EventArgs e)
        {
            //this.Hide();
            Form2 form2 = new Form2();
            form2.StartPosition = FormStartPosition.CenterParent;
            form2.FormClosing += new FormClosingEventHandler(ChildFormClosing);
            form2.Show();

            //form2.SetDesktopLocation(x, y);
        }

        private void btnSozcukEkleTers_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.StartPosition = FormStartPosition.CenterParent;
            form2.FormClosing += new FormClosingEventHandler(ChildFormClosing);
            form2.Show();
        }


        private void ChildFormClosing(object sender, FormClosingEventArgs e)
        {
            //InitGen();
            RefreshSearchList();
            DictChanged();

        }



        private void label_sort_Click(object sender, EventArgs e)
        {



        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateStatus("Application started successfully", "");            
            UpdateStatus3("TOTAL : 0 Elements");

        }

        //public static bool TersSozcukMod = false;

        /*
        public static int buttonTersCounter;
        Color ButtonTersColor;
        private void buttonTers_Click(object sender, EventArgs e)
        {
            buttonTersCounter++;

            if (buttonTersCounter % 2 == 0)
            {
                Start_DictReverse();
            }
            else
            {
                Start_DictMain();
            }

            Start_DictOrtak();
        }  */


        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            TurnOffFormLevelDoubleBuffering();
        }

        private void RichTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            //RichTextBox1.Select(0, 0);
            //RichTextBox1.Focus();
            //RichTextBox1.ScrollToCaret();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ensure that text is currently selected in the text box.   
            if (RichTextBox1.SelectedText != "")
                RichTextBox1.Cut(); // Cut the selected text in the control and paste it into the Clipboard.
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Determine if last operation can be undone in text box.   
            //if (RichTextBox1.CanUndo == true)
            {
                // Undo the last operation.
                //RichTextBox1.Undo();
                // Clear the undo buffer to prevent last action from being redone.
                //RichTextBox1.ClearUndo();
                //RichTextBox1.Text = GetLastChange(RichTextBox1);
            }
            Undo();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ensure that text is selected in the text box.   
            if (RichTextBox1.SelectionLength > 0)
                RichTextBox1.Copy();  // Copy the selected text to the Clipboard.
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Determine if there is any text in the Clipboard to paste into the text box.
            if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
            {
                // Determine if any text is selected in the text box.
                if (RichTextBox1.SelectionLength > 0)
                {
                    // Ask user if they want to paste over currently selected text.
                    //if (MessageBox.Show("Do you want to paste over current selection?", "Cut Example", MessageBoxButtons.YesNo) == DialogResult.No)
                    // Move selection to the point after the current selection and paste.
                    RichTextBox1.SelectionStart += RichTextBox1.SelectionLength;
                }
                // Paste current text in Clipboard into text box.
                RichTextBox1.Paste();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox1_KeyUp(object sender, KeyEventArgs e)
        {
            //if Enter (return) key is pressed
            if (e.KeyData == Keys.Enter)
            {
                if (string.IsNullOrEmpty(comboBox1.Text.ToString()) || nSelectedIndex < 0)
                    return;

                currentWord.AddLabel( comboBox1.Text.ToString());  // WR.DictBindingLists[(int)DictMode][nSelectedIndex]
                textBoxEtiket.Text = comboBox1.Text.ToString();
                comboBox1.ForeColor = Color.Maroon;

                for (int i = 0; i < comboBox1.Items.Count; i++)
                {
                    // exit event if text already exists
                    if (comboBox1.Text == comboBox1.Items[i].ToString())
                        return;
                }

                // add item to comboBox1
                comboBox1.Items.Add(comboBox1.Text);

            }
        }

        private void comboBox1_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            currentWord.AddLabel( comboBox1.Text.ToString());   // WR.DictBindingLists[(int)DictMode][nSelectedIndex]
            textBoxEtiket.Text = comboBox1.Text.ToString();
            comboBox1.ForeColor = Color.Maroon;

            listBox1.Focus();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            comboBox1.ForeColor = Color.Black;
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.ForeColor = Color.Black;
        }

        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)  // File-Save
        {
            SaveProcess(true);
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void addReverseWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addReverseWord();
        }

        private void exportToTxtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var lwx in WR.listWords)
            {
                //string str = lwx.Meaning;
                //lwx.Meaning = str.Replace("GİZLE", "");
                //lwx.Meaning = str.Replace("Fiil\n", "\n");
                for (int i = 0; i < Consts.TrimWords.GetLength(0); i++)
                {
                    //   lwx.Meaning.Replace(Consts.TrimWords[i, 0], Consts.TrimWords[i, 1]);
                    lwx.Content0 = Common.ReplaceStr(lwx.Content0, Consts.TrimWords[i, 0], Consts.TrimWords[i, 1]);
                }
            }

            SaveProcess(false);

            //string fullTextExport = "";
            //string separator = ">>>>>>\r\n";
            /*
            string tmpFileName = @Consts.DosyaYolu2;

            try
            {
                if (File.Exists(tmpFileName)){
                    File.Delete(tmpFileName);
                }

            }
            catch ( Exception Ex )
            {
                string title2 = "Error201";
                string message2 = "Error on Export!" + Ex.ToString();
                MessageBoxButtons buttons2 = MessageBoxButtons.OK;
                MessageBox.Show(message2, title2, buttons2, MessageBoxIcon.Stop);
            }       */

            string title2 = "Code 201";
            string message2 = "Trim process performed!";  // + Ex.ToString();
            MessageBoxButtons buttons2 = MessageBoxButtons.OK;
            MessageBox.Show(message2, title2, buttons2, MessageBoxIcon.Stop);

        }

        //int cursorPosEx = 0;
        //int cursorPos = 0;
        private void RichTextBox1_CursorChanged(object sender, EventArgs e)
        {
            //cursorPos = RichTextBox1.SelectionStart;
            //cursorPosEx = RichTextBox1.SelectionStart;            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveProcess(false);
        }

        private void clearAllScoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string title = "All Scores Clear";
            string message = "All Scores will be cleared! Are you sure?";
            MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
            var result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);

            if (result == DialogResult.OK)
            {
                foreach (var lwx in WR.listWords)
                {
                    lwx.Scores = "-";
                    lwx.Skors.Clear();
                }

                foreach (var lwo in WR.list_words_orig)
                {
                    string name_ = lwo.Name;
                    string namescore = name_;
                    string score_ = "-";
                    Dict dictx = lwo.DictType;

                    var Sonuc = Common2.ScoreHsb(score_, namescore, dictx);
                    int xsc = Sonuc.Item1;
                    //score_ = Sonuc.Item2;
                    namescore = Sonuc.Item3;
                    lwo.NameScore = namescore;
                    lwo.Xscore = xsc;
                    lwo.Skors.Clear();
                }

                SaveProcess(false);
                InitGenAsync();
            }
        }

        private void learnStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KelimeSayisi = WR.DictBindingLists[(int)Dict.WORDS].Count().ToString() + Dict.WORDS.ToString();
            TersKelimeSayisi = WR.DictBindingLists[(int)Dict.WORDS_REVERSE].Count().ToString() + Dict.WORDS_REVERSE.ToString();
            YapilarSayisi = WR.DictBindingLists[(int)Dict.STRUCTURES].Count().ToString() + Dict.STRUCTURES.ToString();
            TersYapilarSayisi = WR.DictBindingLists[(int)Dict.STRUCTURES_REVERSE].Count().ToString() + Dict.STRUCTURES_REVERSE.ToString();
            CumlelerSayisi = WR.DictBindingLists[(int)Dict.SENTENCES].Count().ToString() + Dict.ARTIKELS.ToString();
            TersCumlelerSayisi = WR.DictBindingLists[(int)Dict.SENTENCES_REVERSE].Count().ToString() + Dict.ARTIKELS.ToString();
            ArtikelSayisi = WR.DictBindingLists[(int)Dict.ARTIKELS].Count().ToString() + Dict.ARTIKELS.ToString();
            NotlarSayisi = WR.DictBindingLists[(int)Dict.NOTES].Count().ToString() + Dict.NOTES.ToString();

            TestSayisi = 0;
            foreach (var lws in WR.listWords )  //BURAYABAK DictMode
            {
                if (!String.IsNullOrEmpty(lws.Scores) && lws.Scores != "-")
                    TestSayisi += lws.Scores.Length;
            }

            FormAbout formAbout = new FormAbout();
            //formAbout.Parent = Form1;
            formAbout.StartPosition = FormStartPosition.CenterParent;

            //formAbout.FormClosing += new FormClosingEventHandler(ChildFormClosing);
            formAbout.ShowDialog();    // ShowForm();
        }


        public static (int, int) getWidthX(Dict DictMode)
        {
            int listboxWidth_ = 1;
            int paddLen_ = 20;

            if (DictMode == Dict.WORDS
              || DictMode == Dict.ARTIKELS)
            {
                listboxWidth_ = 1;
                paddLen_ = 20;
            }
            else if (DictMode == Dict.SENTENCES
                 || DictMode == Dict.SENTENCES_REVERSE)
            {
                listboxWidth_ = 2;
                paddLen_ = 48;
            }
            else if (DictMode == Dict.WORDS_REVERSE
                   || DictMode == Dict.STRUCTURES
                   || DictMode == Dict.STRUCTURES_REVERSE
                   || DictMode == Dict.NOTES)
            {
                listboxWidth_ = 3;
                paddLen_ = 34;
            }

            return (listboxWidth_, paddLen_);
        }

        private void boxPositions()
        {
            var (listboxWidth, paddLen) = getWidthX(DictMode);

            if ( listboxWidth == 1)
            {
                listBox1.Location = new System.Drawing.Point(6, 74);
                listBox1.Size = new System.Drawing.Size(212, 593);

                RichTextBox1.Location = new System.Drawing.Point(220, 112);
                RichTextBox1.Size = new System.Drawing.Size(863, 555);

                richTextBox2.Location = new System.Drawing.Point(220, 74);
                richTextBox2.Size = new System.Drawing.Size(863, 36);
            }
            else if(listboxWidth == 2 )
            {
                listBox1.Location = new System.Drawing.Point(6, 74);
                listBox1.Size = new System.Drawing.Size(471, 593);

                RichTextBox1.Location = new System.Drawing.Point(480,148);
                RichTextBox1.Size = new System.Drawing.Size(603, 519);

                richTextBox2.Location = new System.Drawing.Point(480, 74);
                richTextBox2.Size = new System.Drawing.Size(603, 72 );
            }
            else if (listboxWidth == 3)
            {
                listBox1.Location = new System.Drawing.Point(6, 74);
                listBox1.Size = new System.Drawing.Size(340, 593);         // (317, 555);

                RichTextBox1.Location = new System.Drawing.Point(348, 112);        // (326, 99);
                RichTextBox1.Size = new System.Drawing.Size(735, 555);                       // (757, 555);

                richTextBox2.Location = new System.Drawing.Point(348, 74);
                richTextBox2.Size = new System.Drawing.Size(735, 36 );
            }
        }


        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {   // Main Dictionary
            DictModeChanged(Dict.WORDS);
        }

        private void reverseDictionaryToolStripMenuItem_Click(object sender, EventArgs e)
        {   // Reverse Dictionary
            DictModeChanged(Dict.WORDS_REVERSE);
        }

        private void thirdDictionaryToolStripMenuItem_Click(object sender, EventArgs e)
        {  // Artikel Dictionary
            DictModeChanged(Dict.STRUCTURES);
        }


        private void structuresReverseToolStripMenuItem_Click(object sender, EventArgs e)
        {   // STRUCTURES_REVERSE
            DictModeChanged(Dict.STRUCTURES_REVERSE);
        }

        private void sentencesToolStripMenuItem_Click(object sender, EventArgs e)
        {   //    SENTENCES
            DictModeChanged(Dict.SENTENCES);
        }

        private void sentencesReverseToolStripMenuItem_Click(object sender, EventArgs e)
        {   //    SENTENCES_REVERSE
            DictModeChanged(Dict.SENTENCES_REVERSE);
        }

        private void artikelsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {   //    ARTIKELS
            DictModeChanged(Dict.ARTIKELS);
        }

        private void notesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {   // NOTES
            DictModeChanged(Dict.NOTES);
        }

        private void artikelsToolStripMenuItem_Click(object sender, EventArgs e)
        {    //    ARTIKELS
            DictModeChanged(Dict.ARTIKELS);
        }

        private void notesToolStripMenuItem_Click(object sender, EventArgs e)
        {    // NOTES
            DictModeChanged(Dict.NOTES);
        }
        
        //====================================

        private void dictionaryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DictModeChanged(Mod.Dictionary);
        }

        private void testToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DictModeChanged(Mod.Test);
        }

        private void randomTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DictModeChanged(Mod.Random_Test);
        }

        private void randomWeightedTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DictModeChanged(Mod.RandomW_Test);
        }

        //=============================

        private void databaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Database;
            SortChanged();
        }

        private void alphabetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Alphabet;
            SortChanged();
        }

        private void reverseAlphabetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Reverse_Alphabet;
            SortChanged();
        }

        private void scoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Score;
            SortChanged();
        }

        private void randomScoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Random_Score;
            SortChanged();
        }

        private void randomScoreWeightedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Random_Score_Weighted;
            SortChanged();
        }

        private void randomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Random;
            SortChanged();
        }

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.History;
            SortChanged();
        }

        private void labelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sort = Sort.Label;
            SortChanged();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void scoreBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {   // X Button
            //toolStripTextBox1.Text  = "";
            //MessageBox.Show("Button clicked!");
        }



        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void openUserFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(dictFileOpened == false)
            {
                string title = "Error 102";
                string message = "Dictionary txt bulunamadi! Önce File - Open Dictionary File menüsünden uygun Dictionary dosyasini aciniz!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                return;
            }

            Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Text Files |*.txt";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //richTextBox1.Text = File.ReadAllText(openFileDialog1.FileName);
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    strfilename = openFileDialog1.FileName;
                    myStream.Close();
                    this.Refresh();
                    //string filetext = File.ReadAllText(strfilename);
                    //richTextBox1.Text = filetext;
                    InitUserInfo();
                }
                else
                {
                    myStream.Close();
                    string title = "Error 101";
                    string message = "User Dictionary txt was not found! Please find it from File - Open User Dictionary Menu! again";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    MessageBox.Show(message, title, buttons, MessageBoxIcon.Stop);
                }

            }


        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            // X Button
            Console.WriteLine("X Button clicked!");
            isProgrammaticallyUpdated = true;

            toolStripTextBox1.Clear();
            //childItem.Click -= ChildItemClickHandler;
            //toolStripTextBox1.TextChanged -= toolStripTextBox1_TextChangedHandler; //  new System.EventHandler(this.);


            isProgrammaticallyUpdated = false;
         
            SearchBoxUpdate();

            toolStripTextBox1.Focus();
        }  */

        private void toolStripButtonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // X Button
            Console.WriteLine("X Button clicked!");
            isProgrammaticallyUpdated = true;

            toolStripTextBox1.Clear();
            //childItem.Click -= ChildItemClickHandler;
            //toolStripTextBox1.TextChanged -= toolStripTextBox1_TextChangedHandler; //  new System.EventHandler(this.);


            isProgrammaticallyUpdated = false;

            SearchBoxUpdate();

            toolStripTextBox1.Focus();
        }

        // Settings
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void statusStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }


        private void toolStripMenuItem1_Click(object sender, EventArgs e)  // Add Word-Phrase
        {
            if (DictMode == Dict.WORDS)
            {
                if (modus != Mod.Random_Test && modus != Mod.RandomW_Test)
                {
                   currentWord.Content0 = RichTextBox1.Text.ToString();        // WR.DictBindingLists[(int)DictMode][nSelectedIndex].Content0 = RichTextBox1.Text.ToString();
                }
            }

            Form2 form2 = new Form2();
            form2.StartPosition = FormStartPosition.CenterParent;
            form2.FormClosing += new FormClosingEventHandler(ChildFormClosing);
            form2.Show();
        }


        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Redo();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        //========================================
        private void RoundButton_Click(object sender, EventArgs e)
        {           // X Button
                    //toolStripTextBox1.Text = "";
                    //MessageBox.Show("Button clicked!");
        }
        //========================================
        //========================================
    }
}
