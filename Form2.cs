﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace WindowsFormsApp5_0
{
    public partial class Form2 : Form
    {

        // Variable to keep track of the selected button
        private Button selectedButton;
        //================================================
        //Font rTextBoxFont;
        void TextEditor(RichTextBox rTB)
        {
            string str = rTB.Text.ToString();

            if (string.IsNullOrEmpty(str))
                return;

            int curLoc = Common.InitEditor(rTB);

            Common.LineMarker(rTB, 0, str.Length);

            List<Textc> wordx = Common.WordPartitioner(rTB, 0, str.Length);

            Common.MarkTokens(rTB, wordx);

            string namex_ = richTextBox2.Text.ToString();           //.NameScore.Substring(0,17).Trim();

            Common.WordNameMarker(rTB, str, namex_, wordx);

            rTB.SelectionLength = 0;
            rTB.SelectionStart = curLoc; // Cursor home 
            rTB.ScrollToCaret();

            rTB.Refresh();
        }

        //============================================

        public Form2()
        {
            InitializeComponent();

            // Attach a Click event handler for all buttons
            button1.Click += Button_Click;
            button2.Click += Button_Click;
            button3.Click += Button_Click;
            button4.Click += Button_Click;
            button5.Click += Button_Click;
            button6.Click += Button_Click;
            button7.Click += Button_Click;
            button8.Click += Button_Click;
            //Form1 frm1 = new Form1();

            ResetButtonStyles();
            //RichTextBox1.Select(0, 1);
            //rTextBoxFont = RichTextBox1.SelectionFont;
            selectedButton = button1;
            selectedButton.BackColor = Color.LightBlue;   // Active background color
            selectedButton.ForeColor = Color.Blue; // Active text color

            richTextBox2.Focus();


        }
        //============================================

        //bool tersSozcukEkle;
        private void Form2_Load(object sender, EventArgs e)
        {

        }
        //===================================
        private void Button_Click(object sender, EventArgs e)
        {
            // Reset all buttons to default appearance
            ResetButtonStyles();

            // Highlight the clicked button
            selectedButton = sender as Button; // Store the clicked button in the variable
            if (selectedButton != null)
            {
                selectedButton.BackColor = Color.LightBlue;   // Active background color
                selectedButton.ForeColor = Color.Blue; // Active text color
            }
        }

        //===================================
        private void ResetButtonStyles()
        {
            // Reset all button appearances to default
            button1.BackColor = Color.LightGray;
            button1.ForeColor = Color.DarkGray;

            button2.BackColor = Color.LightGray;
            button2.ForeColor = Color.DarkGray;

            button3.BackColor = Color.LightGray;
            button3.ForeColor = Color.DarkGray;

            button4.BackColor = Color.LightGray;
            button4.ForeColor = Color.DarkGray;

            button5.BackColor = Color.LightGray;
            button5.ForeColor = Color.DarkGray;

            button6.BackColor = Color.LightGray;
            button6.ForeColor = Color.DarkGray;

            button7.BackColor = Color.LightGray;
            button7.ForeColor = Color.DarkGray;

            button8.BackColor = Color.LightGray;
            button8.ForeColor = Color.DarkGray;
        }

        //===================================
        private static Dict dictX;

        private bool buttonSelect()
        {
            if ( selectedButton != button1
              && selectedButton != button2
              && selectedButton != button3
              && selectedButton != button4
              && selectedButton != button5
              && selectedButton != button6
              && selectedButton != button7
              && selectedButton != button8)
            {
                string title1 = "Hata!";
                string message1 = "Sözlük tipi seciniz!";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                MessageBox.Show(message1, title1, buttons1, MessageBoxIcon.Stop);
                return false;
            }

            dictX = Dict.WORDS;
            if (selectedButton == button1)
                dictX = Dict.WORDS;
            else if (selectedButton == button2)
                dictX = Dict.WORDS_REVERSE;
            else if (selectedButton == button3)
                dictX = Dict.STRUCTURES;
            else if (selectedButton == button4)
                dictX = Dict.STRUCTURES_REVERSE;
            else if (selectedButton == button5)
                dictX = Dict.SENTENCES;
            else if (selectedButton == button6)
                dictX = Dict.SENTENCES_REVERSE;
            else if (selectedButton == button7)
                dictX = Dict.ARTIKELS;
            else if (selectedButton == button8)
                dictX = Dict.NOTES;

            return true;
        }

        //===================================
        private void ButtonSave_Click(object sender, EventArgs e)
        {
            string name, meaning;
            string etiket;
            //string full_txt;

            meaning = RichTextBox1.Text.ToString();
            string message3;

            buttonSelect();


            name = richTextBox2.Text.ToString().Trim();  //.ToLower();
            etiket = textBoxEtiket.Text.ToString();

            if (String.IsNullOrEmpty(name))
            {
                string title1 = "Hata!";
                string message1 = "Sözcük ismi boş olamaz!";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                MessageBox.Show(message1, title1, buttons1, MessageBoxIcon.Stop);
                return;
            }
            else if (String.IsNullOrEmpty(RichTextBox1.Text))
            {
                string title1 = "Hata!";
                string message1 = "Sözcügün anlami boş olamaz!";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                MessageBox.Show(message1, title1, buttons1, MessageBoxIcon.Stop);
                return;
            }
            else if (WR.WordExistsInBindingList(dictX, name))             // (WR.listWords[(int)Form1.DictMode].Exists(x => x.Name == name))
            {

                var element = WR.DictBindingLists[(int)dictX].FirstOrDefault(w => w.Name == name);

                if (element != null)
                {
                    if (element.Content0 != RichTextBox1.Text.ToString())
                    {
                        element.Content0 = RichTextBox1.Text.ToString();
                        // Access the found element here
                        Console.WriteLine($"Found element: {element.Name}");

                        string message5 = "Kelime Sözlükte degistirildi!";
                        string title5 = "Bilgi";
                        MessageBoxButtons buttons5 = MessageBoxButtons.OK;
                        MessageBox.Show(message5, title5, buttons5, MessageBoxIcon.Information);
                    }
                    else
                    {

                    }
                }
                else
                {
                    Console.WriteLine("No matching element found.");

                    string title1b = "Hata!";
                    string message1b = "Bir hata olustu! Kelime, Sözlükte bulunamadi!";   // "Bu sözcük, Sözlükte zaten mevcut!";
                    MessageBoxButtons buttons1b = MessageBoxButtons.OK;
                    MessageBox.Show(message1b, title1b, buttons1b, MessageBoxIcon.Stop);
                }

                RichTextBox1.Text = "";
                richTextBox2.Text = "";
                textBoxEtiket.Text = "";
                richTextBox2.Focus();
                return;
            }

            string content = RichTextBox1.Text.ToString();
            string relatedName_ = textBox1.Text.ToString();

            List<string> labels = new List<string>();

            string strlbl = textBoxEtiket.Text.ToString();

            if (!string.IsNullOrEmpty(strlbl))
            {
                labels = new List<string>(strlbl.Split(','));
            }

            //        Word(Dict dictType, string name, string content, string relatedName, List<string> labels)
            Word newWord = new Word(dictX, name, content, relatedName_, labels);

            WR.AddWord(newWord);
            message3 = "Kelime Sözlüğe eklendi!";


            string title3 = "Bilgi";
            MessageBoxButtons buttons3 = MessageBoxButtons.OK;
            MessageBox.Show(message3, title3, buttons3, MessageBoxIcon.Information);

            //Form1.RefreshSearchList();

            RichTextBox1.Text = "";
            richTextBox2.Text = "";
            textBoxEtiket.Text = "";
            richTextBox2.Focus();
        }

        string text_prev = "";
        private void RichTextBox1_TextChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Form2.RichTextBox1()_TextChanged Executed\n");
            string str_tc = RichTextBox1.Text.ToString();
            if (!string.IsNullOrEmpty(str_tc))
            {
                if (Math.Abs(str_tc.Length - text_prev.Length) > 2)
                {
                    TextEditor(RichTextBox1);
                }
                else
                {
                    string namex_ = richTextBox2.Text.ToString();
                    Common.PartialEditor2(RichTextBox1, namex_);
                }
            }

            text_prev = str_tc;
        }


        private void buttonKapat_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        /*
        private void textBox_name_Leave(object sender, EventArgs e)
        {
            string name = richTextBox2.Text.ToString().Trim();

            if (String.IsNullOrEmpty(name))
            {
                string title1 = "Hata!";
                string message1 = "Sözcük ismi boş olamaz!";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                MessageBox.Show(message1, title1, buttons1, MessageBoxIcon.Stop);
                return;
            }
            else if(WR.WordExistsInBindingList(Form1.DictMode, name))    //  (WR.DictBindingLists[(int)Form1.DictMode].Exists(x => x.Name == name))
            {
                string title1b = "Warning!";
                string message1b = "Bu sözcük, Sözlükte zaten mevcut! Anlam sözcükten otomatik olarak getirildi!";
                MessageBoxButtons buttons1b = MessageBoxButtons.OK;
                MessageBox.Show(message1b, title1b, buttons1b, MessageBoxIcon.Stop);
                return;
            }
        }       */

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_Leave(object sender, EventArgs e)
        {
            Console.WriteLine("RichTextBox odaktan çıktı (Leave).");
            // Odak kaybında yapılacak işlemler

            
            

        }

        private void RichTextBox1_Enter(object sender, EventArgs e)
        {
            Console.WriteLine("RichTextBox'a giriş yapıldı (Enter).");

            if (buttonSelect() != true) return;

            string name = richTextBox2.Text.ToString().Trim();

            if (String.IsNullOrEmpty(name))
            {
                string title1 = "Hata!";
                string message1 = "Sözcük ismi boş olamaz!";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                MessageBox.Show(message1, title1, buttons1, MessageBoxIcon.Stop);
                return;
            }

            if (WR.WordExistsInBindingList(dictX, name))             // (WR.listWords[(int)Form1.DictMode].Exists(x => x.Name == name))
            {
                var element = WR.DictBindingLists[(int)dictX].FirstOrDefault(w => w.Name == name);

                if (element != null)
                {
                    RichTextBox1.Text = element.Content0;
                    // Access the found element here
                    Console.WriteLine($"Found element: {element.Name}");

                    string title1b = "Warning!";
                    string message1b = "Bu sözcük, Sözlükte zaten mevcut! Sözcügün anlami getirildi!";
                    MessageBoxButtons buttons1b = MessageBoxButtons.OK;
                    MessageBox.Show(message1b, title1b, buttons1b, MessageBoxIcon.Information);
                    return;

                }
                else
                {
                    string title1c = "Error!";
                    string message1c = "Bir hata olustu! Error02";
                    MessageBoxButtons buttons1c = MessageBoxButtons.OK;
                    MessageBox.Show(message1c, title1c, buttons1c, MessageBoxIcon.Information);
                    return;
                }

            }

        }

        //========================================


    }
}
