﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Reflection;

namespace WindowsFormsApp5_0
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            InitializeComponent();
        }
/*
        public static DateTime GetLinkerTime(Assembly assembly)
        {
            const string BuildVersionMetadataPrefix = "+build";

            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            if (attribute?.InformationalVersion != null)
            {
                var value = attribute.InformationalVersion;
                var index = value.IndexOf(BuildVersionMetadataPrefix);
                if (index > 0)
                {
                    value = value[(index + BuildVersionMetadataPrefix.Length)..];
                    return DateTime.ParseExact(value, "yyyy-MM-ddTHH:mm:ss:fffZ", CultureInfo.InvariantCulture);
                }
            }
            return default;
        }   */

        private void FormAbout_Load(object sender, EventArgs e)
        {
            string Prog_Ver = "OCTOPUS " + Consts.Version;
            string DictFilePath = Form1.SozlukAdi;
            string KelimeNo = Form1.KelimeSayisi;
            string TersKelimeNo = Form1.TersKelimeSayisi;
            string YapilarNo = Form1.YapilarSayisi;
            string TersYapilarNo = Form1.TersYapilarSayisi;

            string CumlelerNo = Form1.CumlelerSayisi;
            string TersCumlelerNo = Form1.TersCumlelerSayisi;

            string ArtikelNo = Form1.ArtikelSayisi;
            string NotlarNo = Form1.NotlarSayisi;
            string TotalNo = Form1.TotalSayi;

            // KelimeSayisi, TersKelimeSayisi, YapilarSayisi, TersYapilarSayisi, ArtikelSayisi, NotlarSayisi

            int TestSayi = Form1.TestSayisi;

            label1.Text = "";
            label2.Text = Prog_Ver + "\r\n"
                        + DictFilePath + "\r\n\r\n"
                        + KelimeNo + "\r\n"
                        + TersKelimeNo + "\r\n"
                        + YapilarNo + "\r\n"
                        + TersYapilarNo + "\r\n"
                        + CumlelerNo + "\r\n"
                        + TersCumlelerNo + "\r\n"
                        + ArtikelNo + "\r\n"                        
                        + NotlarNo + "\r\n\r\n"
                        + TotalNo + "\r\n\r\n"
                        + "Vocabulary Tests  : " + TestSayi.ToString() + "\r\n\n";

            string tb1 = "";
            textBox1.Text = tb1;

            //textBox1.SelectAll();
            textBox1.ForeColor = Color.Black;           // Common.rTextBoxFont;
            //textBox1.SelectionColor = Color.Black;
            //rTB.SelectionBackColor = Color.White;
            //textBox1.DeselectAll();

            linkLabel1.Text = "www.youglish.com";


        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
