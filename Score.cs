﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.InteropServices;


namespace WindowsFormsApp5_0
{
    public static class Common2
    {
        //===================================================
        public static (int, string, string) ScoreHsb(string score_, string name_, Dict dictx)
        {
            int xsc_;
            string ns_temp;
            string namescore_;

            if (string.IsNullOrEmpty(score_))
            {
                score_ = "";
            }
            else if (score_[0] == '-' && score_.Length > 1)
            {
                score_ = score_.Remove(0, 1);
            }

            if (score_ == "" || score_ == "-")
            {
                xsc_ = 200;
                score_ = "-";
                ns_temp = score_;
            }
            else if (score_ == "0")  //|| score_ == "00" || score_ == "000" || score_ == "0000" || score_ == "00000")
            {
                xsc_ = 0;
                ns_temp = score_;
            }
            else if (score_ == "00") // || score_ == "000" || score_ == "0000" || score_ == "00000")
            {
                xsc_ = -1;
                ns_temp = score_;
            }
            else if (score_ == "000")
            {
                xsc_ = -2;
                ns_temp = score_;
            }
            else if (score_ == "0000")
            {
                xsc_ = -3;
                ns_temp = score_;
            }
            else if (score_ == "00000")
            {
                xsc_ = -4;
                ns_temp = score_;
            }
            else
            {
                xsc_ = Find_Score(score_);
                ns_temp = xsc_.ToString();
            }

            namescore_ = buildStringNameScore(name_, ns_temp, dictx);

            return (xsc_, score_, namescore_);
        }
        
        //===================================================
        public static string buildStringNameScore( string name_, string score_str, Dict dictx)
        {            
            var (dummyX, paddLenX) = Form1.getWidthX(dictx);

            string namescore_;
            int rightLen = score_str.Length + 2;
            int leftLen = paddLenX - rightLen;

            if (name_.Length > leftLen)
            {
                namescore_ = name_.Remove(leftLen);
                namescore_ += score_str.PadLeft(rightLen);
            }
            else
            {
                namescore_ = name_ + score_str.PadLeft(paddLenX - name_.Length);
            }

            return namescore_;
        }

        //===================================================
        public static int Find_Score(string str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return 0;
            }

            int[,] arr2d = new int[8, 8]
            {
                { 70,  0,  0,  0,  0,  0,  0,  0 },  // 1 değer
                { 45, 30,  0,  0,  0,  0,  0,  0 },  // 2 değer
                { 40, 33, 13,  0,  0,  0,  0,  0 },  // 3 değer
                { 40, 30, 20, 10,  0,  0,  0,  0 },  // 4 değer
                { 39, 28, 18, 10,  5,  0,  0,  0 },  // 5 değer
                { 35, 25, 17, 11,  8,  4,  0,  0 },  // 6 değer
                { 33, 24, 15, 10,  8,  6,  4,  0 },  // 7 değer
                { 30, 23, 14, 12, 10,  6,  3,  2 },  // 8 değer
            };

            int[] x_ = new int[8] { 255, 255, 255, 255, 255, 255, 255, 255 };
            int n;
            int xlen;
            int fark;
            int sum = 0;

            xlen = str.Length;

            fark = 0;
            if (xlen > 8)
            {
                fark = xlen - 8;
                xlen = 8;
            }

            for (n = 0; n < xlen; n++)
            {
                //var theNumber = theString.Where(x => char.IsNumber(x));
                x_[n] = ((byte)str[n + fark]) - 0x30;
                sum += 20 * arr2d[xlen - 1, xlen - n - 1] * x_[n];
            }

            return (sum) / 100;
        }

        //===================================================



    }

    public partial class Form1 : Form
    {

        void textBox_score_came()
        {
            if (modus == Mod.Test || modus == Mod.Random_Test || modus == Mod.RandomW_Test)
            {
                //anlam_goster = true;
                //Console.WriteLine("anlam_goster = {anlam_goster} textBox_score_came \n");
                //int nSelectedIndex = ((NameNameScoreIndex)listBox1.SelectedItem).Index;
                //text_formatting = true;
                undoStack.Clear();
                redoStack.Clear();

                RichTextBox1.Text = currentWord.Content0;
                TextEditor(RichTextBox1);  // RichTextBox1

                comboBox1.SelectedItem = currentWord.GetLabelByIndex(0);

                set_label1(1);

                textBox_score.Focus();
            }
        }


        //=============================================
        void textBox_score_make()
        {
            //textBox_score_prev = currentWord.Scores;

            textBox_score.Text = "";

            TextBox[] scoreBoxes = { scoreBox0, scoreBox1, scoreBox2, scoreBox3, scoreBox4, scoreBox5, scoreBox6, scoreBox7, scoreBox8, scoreBox9, scoreBox10, scoreBox11 };

            string str_scores = currentWord.Scores;

            if (str_scores == "-" || string.IsNullOrEmpty(str_scores))
            {
                for (int i = 0; i < scoreBoxes.Length; i++)
                {
                    scoreBoxes[i].Text = "";
                }

                textBox_score.Text = str_scores;
                return;
            }

            string[] strings = new string[12];
            int strlen = Math.Min(str_scores.Length, 12);

            for (int i = 11, z = str_scores.Length - 1, y = 0; y < strlen; i--, z--, y++)
            {
                strings[i] = str_scores[z].ToString();
            }

            /*
            for (int i = Math.Min(str_scores.Length, 10); i < 12; i++)
            {
                strings[i] = "";
            }   */


            for (int i = 0; i < scoreBoxes.Length; i++)
            {
                scoreBoxes[i].Text = strings[i];
            }
        }
        //=============================================
        /*  V7
        void ShowRtb2()
        {
            int indexP2;
            string rtb2 = "";
            string rtb3 = "";
            string wordname_ = "";

            if (listBox2.SelectedIndex != -1 && listBox2.SelectedItem != null
                        && listBox2.Items.Count > 0)
            {
                NameNameScoreIndex lw = (NameNameScoreIndex)listBox2.SelectedItem;

                if (lw != null)
                {
                    labelScore2.Text = lw.Xscore.ToString();
                    if (labelScore2.Text == "200")
                        labelScore2.Text = "-";

                    label9.Text = lw.Name;
                    //rtb2 = lw.Name + " : " + lw.Explanation + " : " + lw.RelatedName + "\n\n";
                    rtb2 = lw.Explanation + "\n";
                    label2.Text = lw.RelatedName;
                    wordname_ = lw.RelatedName;
                }


            }

           // if (indeks != -1)
           // {
           //     rtb2 = list_words_ters_orig[indeks].Name + " : " + list_words_ters_orig[indeks].Explanation + "\n";
           //     label9.Text = list_words_ters_orig[indeks].Name;
           //     textBox1.Text = list_words_ters_orig[indeks].StrScore;
           // }       

            indexP2 = getList2Index();
            if (indexP2 != -1)
            {
                //string wordname_ = list_words_ters_orig[indexP2].RelatedName;

                if (wordname_ != "")
                {
                    int indeks = listWords[(int)DictMode].FindIndex(r => r.Name == wordname_);

                    if (indeks != -1)
                        rtb3 = listWords[(int)DictMode][indeks].Meaning;
                }
                //textBox_score_new_txt_entered = true;
                //textBox_score.Text = listWords[(int)DictMode][nSelectedIndex].Scores;

                listBox2.Focus();

                richTextBox2.Select(0, 0);
                //richTextBox2.Focus();
                richTextBox2.ScrollToCaret();
            }

            richTextBox2.Text = rtb2;
            richTextBox3.Text = rtb3;
        }
        */

        //=========================================
        void cursor_to_end(TextBox txtBox)
        {
            //txtBox.SelectionLength = 0;
            //txtBox.SelectionStart = txtBox.Text.Length;
            txtBox.Focus();
            txtBox.ScrollToCaret();
        }
        //=========================================
        public static bool score_eklendi = false;
        private bool isProcessingScoreTextChanged = false;

        void score_TextChanged()
        {
            if (isProcessingScoreTextChanged)
            {
                return;
            }
            isProcessingScoreTextChanged = true; ;

            Console.WriteLine("score_TextChanged STARTED\n");
            string scorex_ = "";
            string score_ = textBox_score.Text.ToString();

            if (string.IsNullOrEmpty(score_) || score_ == textBox_score_prev)
            {
                textBox_score.Focus();
                cursor_to_end(textBox_score);
                isProcessingScoreTextChanged = false;

                Console.WriteLine("score_TextChanged ENDED #1\n");
                return;
            }

            if (score_.Length > 1)
            {
                score_ = score_[score_.Length - 1].ToString();
                textBox_score.Text = score_;
            }

            if (nSelectedIndex < 0)
            {
                isProcessingScoreTextChanged = false;
                Console.WriteLine("score_TextChanged ENDED #2\n");
                return;
            }


            if (score_eklendi != false)  // (&& )  // textBox_score_prev.Length != 0
            {
                string stros = currentWord.Scores;
                int index_letzte = stros.Length - 1;
                currentWord.Scores = stros.Remove(index_letzte, 1);

                if (currentWord.Skors.Count > 0)
                {
                    currentWord.Skors.RemoveAt(currentWord.Skors.Count - 1);
                }
            }

            textBox_score_prev = score_;

            if (Char.IsNumber(score_[0]))                   // girilen karakter geçerli bir rakam             
            {  
                string name_ = currentWord.Name;
                string namescore = name_;
                //int ind_ = list_words[nSelectedIndex].Index;
                scorex_ = currentWord.Scores + score_[0];
                
                var Sonuc = Common2.ScoreHsb(scorex_, name_, currentWord.DictType);
                int xsc_ = Sonuc.Item1;
                scorex_ = Sonuc.Item2;
                namescore = Sonuc.Item3;

                int rweight = Common1.CalcRandomWeight(xsc_, scorex_);

                score_eklendi = true;

                currentWord.Scores = scorex_;
                currentWord.AddSkor(score_[0] - '0', DateTime.Now);

                currentWord.NameScore = namescore;
                currentWord.WeightRandom = rweight;
                CalcTRW();
                calc_show_ppm(currentWord);

                currentWord.Xscore = xsc_;

                /*
                int indeks = WR.FindWordIndex(DictMode, currentWord);    // WR.DictBindingLists[(int)DictMode].FindIndex(r => r.Name == name_);  //V7 list_words.ToList()
                if (indeks != -1)
                {
                    WR.list_words_orig[indeks].Xscore = xsc_;
                    WR.list_words_orig[indeks].NameScore = namescore;
                }  */

                if (!string.IsNullOrEmpty(toolStripTextBox1.Text))
                {
                    foreach (var listx in WR.list_words_search)
                    {
                        int index = WR.FindWordIndex2(DictMode, listx);  //    WR.DictBindingLists[(int)DictMode].FindIndex(word => word.Name == listx.Name); // V7
                            if (index == nSelectedIndex)                                       // V7 
                            {
                                listx.NameScore = namescore;
                                listx.Xscore = xsc_;
                                break;
                            }
                    }

                    foreach (var listx in WR.list_words_search2)
                    {
                            int index = WR.FindWordIndex2(DictMode, listx);                // WR.DictBindingLists[(int)DictMode].FindIndex(word => word.Name == listx.Name);  // V7
                            if (index == nSelectedIndex)  // V7
                            {
                                listx.NameScore = namescore;
                                listx.Xscore = xsc_;
                                break;
                            }
                    }
                    Refresh_ListBox(WR.list_words_search2, 0);
                    Console.WriteLine("#20 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");
                }

                int indeks2 = WR.list_words_history.ToList().FindIndex(r => r.Name == name_);
                if (indeks2 == -1)
                {
                    WR.list_words_history.Add(currentWord);        //(WR.DictBindingLists[(int)DictMode][indeks]);
                }
            }

            //label1.Text = namescore;
            set_label1(1);

            if (modus != Mod.RandomW_Test)
            {
                //Refresh_ListBox1(list_words_orig, 0);      
                //text_formatting = true;
                //nSelectedIndex_prev = -1;  4NOW
                //TextEditor(listWords[(int)DictMode][nSelectedIndex].Meaning);
                //selListBoxFirstItem(listBox1);
            }
            else
            {
                score_ = "";   // score_.Remove(score_.Length - 1);                
            }

            //listBox1.Refresh();

            //WR.DictBindingLists[(int)DictMode][nSelectedIndex].Scores = scorex_;

            textBox_score_prev = score_;
            textBox_score.Text = score_;
            cursor_to_end(textBox_score);
            textBox_score.Focus();

            isProcessingScoreTextChanged = false;
            Console.WriteLine("score_TextChanged ENDED #3\n");

        }
        //=========================================
        void SearchBoxUpdate()
        {
            string search = toolStripTextBox1.Text.ToString().ToLower();

            if (DictMode == Dict.WORDS)
            {
                WR.list_words_search2.Clear();   // listBox1.Items.Clear();

                if (String.IsNullOrEmpty(search))
                {
                    Refresh_ListBox(WR.list_words_orig, 2);
                }
                else
                {

                    foreach (var listx in WR.list_words_search)
                    {
                        if (!checkBox1.Checked)
                        {
                            if (listx.NameScore.ToLower().StartsWith(search))
                            {
                                WR.list_words_search2.Add(listx);
                            }
                        }
                        else
                        {
                            if (toolStripTextBox1.Text.ToString().Length > 3)
                            {
                                string strM = listx.Content0.ToLower();

                                if (strM.Contains(search))
                                {
                                    int startindex = 0;
                                    //string str = rTB.Text.ToString();
                                    while (startindex < strM.Length)
                                    {
                                        int wordstartIndex = strM.IndexOf(search);
                                        if (wordstartIndex == -1)
                                            break;

                                        if (wordstartIndex == 0 || !Char.IsLetter(strM[wordstartIndex - 1]))
                                        {
                                            if ((wordstartIndex + search.Length) == strM.Length || !Char.IsLetter(strM[wordstartIndex + search.Length]))
                                            {
                                                WR.list_words_search2.Add(listx);
                                                break;
                                            }
                                        }

                                        startindex += wordstartIndex + search.Length;
                                    }

                                }
                            }
                            else
                            {

                            }
                        }
                    }

                    //if (list_words_search2.Count() != 0)                    
                    {
                        Refresh_ListBox(WR.list_words_search2, 1);
                        Console.WriteLine("#21 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");
                    }
                    //else if(toolStripTextBox1.Text.ToString().Length == 0)
                    {
                        //list_words_search2.Clear();
                    }

                }

            }
            else
            {       // TersSozcukModu
                WR.list_words_search.Clear();   // listBox1.Items.Clear();

                if (String.IsNullOrEmpty(search))
                {
                    Refresh_ListBox(WR.list_words_orig, 1);
                }
                else
                {

                    foreach (var listx in WR.list_words_orig)
                    {
                        if (listx.Name.ToLower().Contains(search))
                        {
                            WR.list_words_search.Add(listx);
                        }
                    }

                    Refresh_ListBox(WR.list_words_search, 1);
                }

            }

             
            //listBox1.Items.AddRange(items);

        }
        //=========================================
        /*
        void SearchListUpdate()
        {
            string search = toolStripTextBox1.Text.ToString().ToLower();

            if (TersSozcukMod == false)
            {
                if (String.IsNullOrEmpty(search) == false)
                {
                    list_words_search2.Clear();   // listBox1.Items.Clear();

                    foreach (var listx in list_words_search)
                    {
                        if (listx.NameScore.StartsWith(search))
                        {
                            list_words_search2.Add(listx);
                        }
                    }
                }
            }
            else
            {       // TersSozcukModu
                if (String.IsNullOrEmpty(search) == false)
                {
                    list_words_ters_search.Clear();   // listBox1.Items.Clear();

                    foreach (var listx in list_words_ters_orig)
                    {
                        if (listx.Name.Contains(search))
                        {
                            list_words_ters_search.Add(listx);
                        }
                    }
                }
            }

            toolStripTextBox1.Focus();
            //listBox1.Items.AddRange(items);

        }  */

        //=========================================




        //=========================================
        //=========================================
    }

}


