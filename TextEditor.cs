﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.InteropServices;


namespace WindowsFormsApp5_0
{

    public partial class Form1 : Form
    {
        void HighlightText(RichTextBox rTB, string strHigh)
        {
                int startindex = 0;
                //string str = rTB.Text.ToString();
                while (startindex < rTB.TextLength)
                {
                    int wordstartIndex = rTB.Find(strHigh, startindex, RichTextBoxFinds.None);
                    if (wordstartIndex != -1)
                    {
                        if (wordstartIndex == 0 || !Char.IsLetter(rTB.Text[wordstartIndex - 1])
                           && ((wordstartIndex + strHigh.Length) == rTB.TextLength || !Char.IsLetter(rTB.Text[wordstartIndex + strHigh.Length])) )
                        {
                            rTB.SelectionStart = wordstartIndex;
                            rTB.SelectionLength = strHigh.Length;
                            rTB.SelectionBackColor = Color.Yellow;
                        }
                    }
                    else
                    {
                        break;
                    }
                    startindex = wordstartIndex + strHigh.Length;
                }
        }

        //===================================
        //public int curLocEx = int.MaxValue;

        void TextEditor(RichTextBox rTB)
        {
            string str = rTB.Text.ToString();

            if (string.IsNullOrEmpty(str))
                return;

            int curLoc = Common.InitEditor(rTB);

            Common.LineMarker(rTB, 0, str.Length);

            int lenx = str.Length;                // V7
            if (Form1.DictMode == Dict.WORDS_REVERSE)
            {
                lenx = 0;
            }
            List<Textc> wordx = Common.WordPartitioner(rTB, 0, lenx);

            Common.MarkTokens(rTB, wordx);

            //*  V7 for TextEditor2
            int indo = str.IndexOf('\n');

            if (indo == -1)
                indo = str.Length - 1;

            rTB.Select(0, indo+1);
            rTB.SelectionFont = new Font(Common.rTextBoxFont, Common.rTextBoxFont.Style | FontStyle.Bold);

            //int nSelIndex = getList2Index();

            //
            if (nSelectedIndex == -1)
                return;

            //int x = (int)DictMode;        Ver7.2.16
            // Word word = WR.DictBindingLists[x][nSelectedIndex];  Ver7.2.16


            if (DictMode == Dict.WORDS)
                currentWord.RelatedName = currentWord.Name;

            string namex_ = currentWord.RelatedName;           //.NameScore.Substring(0,17).Trim();
            if (!string.IsNullOrEmpty(namex_))   // namex_ != ""
                Common.WordNameMarker(rTB, str, namex_, wordx);
            
            if(listBox1.DataSource == WR.list_words_search2 && checkBox1.Checked)
            {
                HighlightText(RichTextBox1, toolStripTextBox1.Text.ToString());
            }

            if (Form1.DictMode == Dict.WORDS_REVERSE)   // V7
            {
                str = rTB.Text.ToString();
                int indStart = str.IndexOf(':') + 1;
                string str2 = str.Substring(indStart);
                int indEnd = indStart + str2.IndexOf(':');

                if (indStart != -1 && indEnd != -1 && indStart < indEnd)
                {
                    rTB.Select(indStart, indEnd - indStart);
                    rTB.SelectionColor = Color.BlueViolet;
                }
            }

            rTB.SelectionLength = 0;
            rTB.SelectionStart = curLoc; // Cursor home 

            rTB.ScrollToCaret();
           
            rTB.Refresh();           
        }

        //============================================        
        void addReverseWord()
        {
            if (DictMode == Dict.WORDS_REVERSE)
                return;

            string str = RichTextBox1.SelectedText;

            if (string.IsNullOrEmpty(str))
            {
                string title1a = "Error 105";
                string message1a = "To Add Reverse Word, Please Select Text!";
                MessageBoxButtons buttons1a = MessageBoxButtons.OK;
                MessageBox.Show(message1a, title1a, buttons1a, MessageBoxIcon.Stop);
                return;
            }

            char[] charSplit = { ':' };
            string[] strx = str.Split(charSplit, StringSplitOptions.None);

            if (strx.Count() < 2)
            {
                string title3 = "Error 106";
                string message3 = "Ters Sozcuk eklemek için sozcuk ve anlamı, aralarında ':' ile seçili olmalıdır!";
                MessageBoxButtons buttons3 = MessageBoxButtons.OK;
                MessageBox.Show(message3, title3, buttons3, MessageBoxIcon.Stop);
                return;
            }

            string deyim_ad = strx[1].Trim();
            string deyim_aciklama = strx[0].Trim();
            string deyim_word_name;
            string deyim_name_score;
            string deyim_str_score = "";
            int deyim_xscore;

            int indeks = 0;  // getListIndex();
            if (indeks == -1)
            {
                string title2 = "Error 107";
                string message2 = "Hata! Ters Sözcük Kaydedilemedi!";
                MessageBoxButtons buttons2 = MessageBoxButtons.OK;
                MessageBox.Show(message2, title2, buttons2, MessageBoxIcon.Stop);
                return;
            }
            deyim_word_name = WR.DictBindingLists[(int)DictMode][indeks].Name;

            indeks = WR.list_words_orig.ToList().FindIndex(r => r.Name == deyim_ad);
            if (indeks != -1)
            {
                string title2 = "Error 108";
                string message2 = "Hata! Ters Sözcük zaten kayıtlı!";
                MessageBoxButtons buttons2 = MessageBoxButtons.OK;
                MessageBox.Show(message2, title2, buttons2, MessageBoxIcon.Stop);
                return;
            }

            var Sonuc2 = Common2.ScoreHsb(deyim_str_score, deyim_ad, Dict.WORDS_REVERSE);
            deyim_xscore = Sonuc2.Item1;
            deyim_str_score = Sonuc2.Item2;
            deyim_name_score = Sonuc2.Item3;

            int ind = WR.DictBindingLists[(int)DictMode].Count;
            
            // 4NOW Ver7.2                                             
            //list_words_orig.Add(new Word(deyim_ad, deyim_name_score, deyim_aciklama, deyim_word_name,  deyim_str_score, deyim_xscore, "", 0, ind)); // V7

            string title = "Bilgi";
            string message = "Ters Kelime txt'ye kaydedildi!";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            MessageBox.Show(message, title, buttons);
        }


    }

    
}