﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.Json;


namespace WindowsFormsApp5_0
{
    public partial class Form1 : Form
    {
        /*
        int GetNextWordIndex(string str, int ind_)
        {
            while (ind_ < str.Length)
            {
                if (str[ind_] == '\r' || str[ind_] == '\n' || str[ind_] == ' ' || str[ind_] == ',' || str[ind_] == '.' || str[ind_] == ':' || str[ind_] == '\t' || str[ind_] == '-')
                    break;

                ++ind_;
            }
            if (ind_ == str.Length)
            {
                ind_ = str.Length - 1;
            }

            return ind_;
        }  */
        //=====================================

        void calc_show_ppm(Word curWord)
        {
            ulong num = (ulong)curWord.WeightRandom * 1_000_000;  // WR.DictBindingLists[(int)DictMode][ind]

            if ((ulong)TotalRandomWeight != 0)
            {
                num /= (ulong)TotalRandomWeight;
            }

            UpdateStatusRandomWeight(num.ToString() + " ppm");
            //label8.Text = ;
        }


        //=====================================
        int TotalRandomWeight;

        void CalcTRW()
        {
            if (WR.DictBindingLists[(int)DictMode] == null) return;

            TotalRandomWeight = 0;
            foreach (var lws in WR.DictBindingLists[(int)DictMode])
            {
                TotalRandomWeight += lws.WeightRandom;
            }
        }

        //=====================================
        // currentWord = getListBoxSelectedWord();

        void getListBoxSelected()
        {
            /*
            if(n2SelectedIndex == -1)
            {
                nSelectedIndex = -1;
                currentWord = null;
                return;
            } */

            // listBox1.SetSelected(n2SelectedIndex, true);
            //nSelectedIndex = n2SelectedIndex;
            nSelectedIndex = listBox1.SelectedIndex;

            if (listBox1.SelectedIndex == -1 || listBox1.SelectedItem == null)     // (listBox1.Items.Count == 0)            
            {
                currentWord = null;
            }
            else
            {
                currentWord = (Word)listBox1.SelectedItem;
            }
            
        }
        //------------------------------------
        void getSelectedIndex()
        {
            nSelectedIndex = listBox1.SelectedIndex;
        }

        //=====================================
        /*
        void BuildRandomWList()
        {
            list_ind_randw.Clear();

            int indx, puan;
            int kere;
            for (int z = 0; z < list_words_orig.Count; z++)
            {
                indx = list_words_orig[z].Index;
                puan = list_words_orig[z].Xscore;

                if (puan > 100 || puan < 0)
                    puan = 0;

                string strScr = listWords[(int)DictMode][indx].Scores;
                int nIndex = strScr.Length + 1;

                while (nIndex-- > 0)
                {
                    kere = 100 - puan;
                    while (kere > 0)
                    {
                        list_ind_randw.Add(indx);
                        kere--;
                    }
                }
            }
        }           */
        //=====================================
        void RandomW_Test_GetNextItem()
        {
            if (modus != Mod.RandomW_Test)
                return;

            int randIndex;
            Random randW = new Random();

                //listBox1.Focus();
                //anlam_goster = false;
                //Console.WriteLine("anlam_goster start = {anlam_goster} RandomW_Test_GetNextItem * \n");

                //randIndex = randW.Next(list_ind_randw.Count-1);
                //nSelectedIndex = list_ind_randw[randIndex];

                randIndex = randW.Next(TotalRandomWeight - 1);
                int ind = 0;
                int total_val = 0;
                while (randIndex >= 0)
                {
                    total_val += WR.DictBindingLists[(int)DictMode][ind].WeightRandom;
                    if (randIndex < total_val)
                    {
                        break;
                    }

                    if (++ind >= WR.DictBindingLists[(int)DictMode].Count)
                    {
                        ind = WR.DictBindingLists[(int)DictMode].Count - 1;
                        break;
                    }
                }
                nSelectedIndex = ind;
                currentWord = WR.DictBindingLists[(int)DictMode][nSelectedIndex];

                //label1.Text = list_words[nSelectedIndex].NameScore;
                set_label1(0);

                calc_show_ppm(currentWord);

                textBox_score_prev = currentWord.Scores;
                textBox_score.Text = currentWord.Scores;

                //textBoxEtiket_new_txt_entered = true;
                //textBoxEtiket.Text = listWords[(int)DictMode][nSelectedIndex].Etiket;
                //comboBox1.SelectedItem = listWords[(int)DictMode][nSelectedIndex].Etiket;
                Console.WriteLine("#30 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");
                listBox1.Focus();
                Console.WriteLine("#31 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");

                undoStack.Clear();
                redoStack.Clear();

                RichTextBox1.Text = "";            

        }
        //=====================================

        //bool anlam_goster;

        public string GetMod(Mod modx)
        {
            if (modx == Mod.Dictionary)
                return "Sözlük";
            else if (modx == Mod.Test)
                return "Test";
            else if (modx == Mod.Random_Test)
                return "Random Test";
            else if (modx == Mod.RandomW_Test)
                return "RandomW Test";
            else
                return "";
        }



        //===================================================
        void ShowList(List<Word> lwords)
        {
            Console.WriteLine("Bu listWords[(int)DictMode] {lwords.Count} element icerir:\n");

            for (int i = 0; i < lwords.Count; i++)
            {
                Console.Write("\nNAME: {lwords[i].Name}\n");
                Console.Write("\nNAMESCORE: {lwords[i].NameScore}\n");
                Console.Write("SCORES: {lwords[i].Scores}\n");
                Console.Write("XSCORES: {lwords[i].Xscore}\n");
                Console.Write("MEANING: {lwords[i].Meaning}\n");

                Console.Write("=============================");
            }
            Console.Write("\n");
        }
        //===================================================
        /*
       // Display the array of separated strings using a local function
       void ShowTextc(List<Textc> wordes)
       {  
           Console.WriteLine("This list contains {wordes.Count} elements:\n");
           for (int i = 0; i < wordes.Count; i++)
           {
               string a = wordes[i].Ind.ToString();
               string b = wordes[i].Len.ToString();
               Console.Write("{a}|{b}|{wordes[i].Name}\n");
               //Console.Write("{wordes[i].Name}\n");
           }
           Console.Write("\n\n");  
    }   */
        //=====================================
        // Display the array of separated strings using a local function
        /*
            void Show(string[] entries)
            {   
                Console.WriteLine("The return value contains these {entries.Length} elements:\n");
                foreach (string entry in entries)
                {
                    Console.Write("<{entry}>");
                    Console.Write("\n");
                }
                Console.Write("\n\n");  
            }  */
        //=====================================

        int originalExStyle = -1;
        bool enableFormLevelDoubleBuffering = true;

        protected override CreateParams CreateParams
        {
            get
            {
                if (originalExStyle == -1)
                    originalExStyle = base.CreateParams.ExStyle;

                CreateParams cp = base.CreateParams;
                if (enableFormLevelDoubleBuffering)
                    cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                else
                    cp.ExStyle = originalExStyle;

                return cp;
            }
        }

        private void TurnOffFormLevelDoubleBuffering()
        {
            enableFormLevelDoubleBuffering = false;
            this.MaximizeBox = true;
        }
        //=====================================
        

        //=====================================
        void SortChecked()
        {
            if (sort == Sort.Database) databaseToolStripMenuItem.Checked = true;
            else databaseToolStripMenuItem.Checked = false;

            if (sort == Sort.Alphabet) alphabetToolStripMenuItem.Checked = true;
            else alphabetToolStripMenuItem.Checked = false;

            if (sort == Sort.Reverse_Alphabet) reverseAlphabetToolStripMenuItem.Checked = true;
            else reverseAlphabetToolStripMenuItem.Checked = false;

            if (sort == Sort.Score) scoreToolStripMenuItem.Checked = true;
            else scoreToolStripMenuItem.Checked = false;

            if (sort == Sort.Random_Score) randomScoreToolStripMenuItem.Checked = true;
            else randomScoreToolStripMenuItem.Checked = false;

            if (sort == Sort.Random_Score_Weighted) randomScoreWeightedToolStripMenuItem.Checked = true;
            else randomScoreWeightedToolStripMenuItem.Checked = false;

            if (sort == Sort.Random) randomToolStripMenuItem.Checked = true;
            else randomToolStripMenuItem.Checked = false;

            if (sort == Sort.History) historyToolStripMenuItem.Checked = true;
            else historyToolStripMenuItem.Checked = false;

            if (sort == Sort.Label) labelToolStripMenuItem.Checked = true;
            else labelToolStripMenuItem.Checked = false;

        }

        //=====================================
        void SortChanged()        // ComboBoxSortPanel1()
        {
            SortChecked();
            score_eklendi = false;
            // V7 if (DictMode == Dict.WORDS)
            {
                if (sort == Sort.Database)
                {
                    WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderBy(item => item.Id)  
                        .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);
                }
                else if (sort == Sort.Alphabet)
                {
                    WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderBy(item => item.Name)
                        .ThenBy(item => item.Xscore)
                        .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);
                }
                else if (sort == Sort.Reverse_Alphabet)
                {
                    WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderByDescending(item => item.Name)
                        .ThenBy(item => item.Xscore)
                        .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);
                }
                else if (sort == Sort.Score)
                {
                    WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderBy(item => item.Xscore)
                        .ThenBy(item => item.Name)
                        .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);
                }
                else if (sort == Sort.Random_Score)
                {
                    var rnd = new Random();
                    WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderBy(item => item.Xscore)
                        .ThenBy(item => rnd.Next())
                        .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);
                }
                else if (sort == Sort.Random_Score_Weighted)
                {
                    var rnd = new Random();

                    WR.list_words_orig_tmp = WR.list_words_orig
                        .OrderBy(item => item.WeightRandom)             // WeightRandom);     // list_words_orig[item].WeightRandom) 
                        .ThenBy(item => rnd.Next())
                        .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);
                }
                else if (sort == Sort.Random)
                {
                    var rnd = new Random();
                    WR.list_words_orig_tmp = WR.list_words_orig
                    .OrderBy(item => rnd.Next())
                    .ToList();
                    WR.list_words_orig = new BindingList<Word>(WR.list_words_orig_tmp);

                    //var randomized = list_words_orig.OrderBy(item => rnd.Next());
                }
                else if (sort == Sort.History)
                {
                    undoStack.Clear();
                    redoStack.Clear();

                    RichTextBox1.Text = "";
                    richTextBox2.Text = "";
                    textBox1.Text = "";
                    textBox_score.Text = "";
                }
                else if (sort == Sort.Label)
                {
                    //comboBoxEtiket.Enabled = true;
                    InitMenuEtiket();
                }

                /*
                if (sort != Sort.Label)
                {
                    comboBoxEtiket.SelectedItem = "";
                    comboBoxEtiket.ResetText();
                    comboBoxEtiket.Enabled = false;
                }  */

                if (listBox1.DataSource != null)
                {
                    if (sort == Sort.History)
                    {
                        Refresh_ListBox(WR.list_words_history, 1);
                    }
                    else
                    {
                        Refresh_ListBox(WR.list_words_orig, 1);
                    }
                }
            }

        }

        //===========================================
        void selListBoxFirstItem()
        {   /*
            if (listBox1.Items.Count > 1)
            {
                listBox1.SelectedIndex = 1;
                listBox1.SelectedIndex = 0;
            }
            else if (listBox1.Items.Count == 1)
            {
                listBox1.SelectedIndex = 0;
            }       */

            nSelectedIndex = 0;
            Console.WriteLine("#8 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");
        }

        //================================

        void Refresh_ListBox(BindingList<Word> list_words_x, int level)
        {
            listBox1.DataSource = null;
            listBox1.Items.Clear();
            listBox1.DataSource = list_words_x;
            listBox1.DisplayMember = "NameScore";
            //listBox1.ValueMember = "Index";   V7

            listBox1.Refresh();
            listBox1.Update();

            if (list_words_x == null || list_words_x.Count() == 0)
            {
                RichTextBox1.Clear();   // Ver7.2.26
                richTextBox2.Clear();   // Ver7.2.26
                textBox1.Text = "";
                textBox_score.Text = "";
            }

            if (level > 0)
                selListBoxFirstItem();

            if (level > 1)
                listBox1.TopIndex = 0;

            Console.WriteLine("#40 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");
        }

        //================================
        //Mod modusx = new Mod();
        void DictModeChanged(Mod modusx)
        {
            dictionaryToolStripMenuItem1.Checked = false;
            testToolStripMenuItem1.Checked = false;
            randomTestToolStripMenuItem.Checked = false;
            randomWeightedTestToolStripMenuItem.Checked = false;

            if(modusx == Mod.Dictionary) dictionaryToolStripMenuItem1.Checked = true;
            else if (modusx == Mod.Test) testToolStripMenuItem1.Checked = true;
            else if (modusx == Mod.Random_Test) randomTestToolStripMenuItem.Checked = true;
            else if (modusx == Mod.RandomW_Test) randomWeightedTestToolStripMenuItem.Checked = true;

            /* V7
            ListBox lb; //  = new ListBox();
            if (DictMode == Dict.WORDS)         lb = listBox1;
            else if (DictMode == Dict.WORDS_REVERSE) lb = listBox2;
            else                               lb = listBox1;   */

            Mod pre_modus = modus;
            modus = modusx;
            //modus = (Mod)comboBox_mode.SelectedIndex;
            //anlam_goster = false;
            //Console.WriteLine("anlam_goster = {anlam_goster} comboBox1_SelectedIndexChanged\n");
            score_eklendi = false;

            if (modus == Mod.Dictionary)
            {
                Refresh_ListBox(WR.list_words_orig, 2);
            }
            else if (modus == Mod.RandomW_Test)
            {
                listBox1.DataSource = null;
                listBox1.Items.Clear();
                listBox1.Refresh();
                listBox1.Update();
                //comboBox_sort.SelectedItem = "Txt";
            }
            else if (pre_modus == Mod.RandomW_Test)
            { 
                /*
                if (DictMode == Dict.WORDS)
                    Refresh_ListBox(list_words_orig, 2);
                else if (DictMode == Dict.WORDS_REVERSE)
                    Refresh_ListBox(list_words_orig, 2);  */
                Refresh_ListBox(WR.list_words_orig, 2);
            }

            if (modus == Mod.Test || modus == Mod.Random_Test || modus == Mod.RandomW_Test)
            {
                undoStack.Clear();
                redoStack.Clear();

                RichTextBox1.Text = "";

                //comboBox_sort.SelectedItem = "Txt";
                sort = Sort.Database;
                SortChecked();
            }
        }
        //================================
        void MainComboBoxPanel2()
        {

        }
        //================================

        void LabelxHandler(string itemName)
        {
            //if (sort != Sort.Label || comboBoxEtiket.SelectedIndex < 0)
            //    return;
            this.SuspendLayout();
            WR.list_words_etiket.Clear();

            foreach (var lwx in WR.list_words_orig)
            {
                string[] labelsArray = WR.DictBindingLists[(int)DictMode][lwx.Index].GetLabels();

                for (int i = 0; i < labelsArray.Length; i++)
                {
                    if (labelsArray[i] == itemName)
                    {
                        WR.list_words_etiket.Add(lwx);              //lwx.Name, lwx.NameScore, lwx.Xscore, lwx.Index));
                    }
                }
                
            }

            WR.list_words_etiket_tmp = WR.list_words_etiket
               .OrderBy(item => item.Name)
               .ThenBy(item => item.Xscore)
               .ToList();
            WR.list_words_etiket = new BindingList<Word>(WR.list_words_etiket_tmp);

            Refresh_ListBox(WR.list_words_etiket, 1);
            listBox1.TopIndex = 0;
            Console.WriteLine("#41 listBox1.SelectedItem = " + listBox1.SelectedIndex + "\n");
            this.ResumeLayout();
        }
        //==================================
        void SaveProcess(bool msg)
        {
            string path = strfilename;
            if (!File.Exists(strfilename))
            {
                path = Consts.DosyaYolu;
                strfilename = Consts.DosyaYolu;

                if (!File.Exists(path))
                {
                    File.Create(path);
                    labelSozlukAdi.Text = "DICTONARY V2.1";
                }
            }

            string title = "";
            if (labelSozlukAdi.Text != "")
            {
                title = labelSozlukAdi.Text + " | DICT_VER2";                               
            }

            if (File.Exists(path))
            {
                File.Delete(path);
                //File.Create(path);

                WR.SaveWordsToFile(path, title);

                /*
                File.WriteAllText(path, full_txt);
                full_txt = "";

                //public static List<Word> listWords[(int)DictMode] = new List<Word>();

                // JSON Serializer 
                /*
               var options = new JsonSerializerOptions { WriteIndented = true };
                string jsonString = JsonSerializer.Serialize(list_words, options);
                // Write the serialized string to a file with UTF-8 encoding
                File.WriteAllText("output.json", jsonString, Encoding.UTF8);   */

                //StringBuilder builderA = new StringBuilder("");
                /*   for(int z = 0; z < list_words_ters_orig.Count(); z++)
                   {

                   }       */
                /*
                                builderA.Append(">>>>>>\r\n");
                                builderA.Append("______\r\n");

                                full_txt += builderA.ToString();

                                StringBuilder builder = new StringBuilder("");

                                if (WR.listWords.Count() == 0)
                                {
                                    builder.Append("\n-\n");
                                }
                                else
                                {
                                    for (int i = 0; i < WR.DictBindingLists[(int)DictMode].Count(); i++)
                                    {
                                        builder.Append(WR.DictBindingLists[(int)DictMode][i].Name);

                                        if (!String.IsNullOrEmpty(WR.DictBindingLists[(int)DictMode][i].Scores))
                                        {
                                            builder.Append(" ");
                                            builder.Append(WR.DictBindingLists[(int)DictMode][i].Scores);
                                        }

                                        string[] labelsArray = WR.DictBindingLists[(int)DictMode][i].GetLabels();

                                        for (int z = 0; i < labelsArray.Length; z++)
                                        {
                                            if (!String.IsNullOrEmpty(labelsArray[z]))
                                            {
                                                builder.Append(" #");
                                                builder.Append(labelsArray[z]);
                                            }
                                        }


                                        builder.Append("\n");
                                        builder.Append(WR.DictBindingLists[(int)DictMode][i].Content0);
                                        builder.Append("______\r\n");
                                    }
                                }

                                full_txt += builder.ToString();
                                File.AppendAllText(Form1.strfilename, full_txt);

                                //using (var output = new StreamWriter(@"C:\Users\user\source\repos\temp.txt"));

                                // File.Delete(@"C:\Users\user\source\repos\sozluk1.txt");
                                //File.Move("temp.txt", @"C:\Users\user\source\repos\sozluk2.txt");
                                //File.WriteAllText(Path.Combine(docPath, "WriteFile.txt"), full_txt)
                             */


                if (msg)
                {
                    string titleX = "Bilgi";
                    string message = "Kelimeler txt'ye kaydedildi!";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    MessageBox.Show(message, titleX, buttons);
                }
            }

        }
        //========================================
        bool getFile()
        {
            if (!File.Exists(Form1.strfilename))
            {
                string title2 = "Error";
                string message2 = "Dictionary txt was not found! Please find and show it from File Menu!";
                MessageBoxButtons buttons2 = MessageBoxButtons.OK;
                MessageBox.Show(message2, title2, buttons2, MessageBoxIcon.Stop);
                return false;
            }
            else
            {
                return true;
            }
        }

        //========================================
        void set_label1(int input)
        {
            string restring;
            string strx_name;
            string strx_score;

            if (WR.DictBindingLists[(int)DictMode].Count() == 0)
                return;            
              
            if( nSelectedIndex < 0 )
            {
               return;
            }

            /*
            if (nSelectedIndex >= WR.DictBindingLists[(int)DictMode].Count())
            {
                nSelectedIndex = 0;
            }  */

            if (sozluk == Sozluk.DEUTSCH) 
            {
                string Artikel = currentWord.Content0;
                if (((modus != Mod.Test && modus != Mod.Random_Test && modus != Mod.RandomW_Test) || input == 1)
                    && (DictMode == Dict.WORDS || DictMode == Dict.ARTIKELS))
                {
                    if (!String.IsNullOrEmpty(Artikel) && Artikel.Length > 2)
                    {
                        Artikel = Artikel.Substring(0, 3);

                        if (Artikel != "der" && Artikel != "die" && Artikel != "das")
                        {
                            Artikel = "";
                        }
                    }
                    else
                    {
                        Artikel = "";
                    }
                }
                else
                {
                    Artikel = "";
                }

                if (Artikel != "" && !string.IsNullOrEmpty(currentWord.Name))
                {
                    string strx = currentWord.Name;
                    string res_str = strx.Substring(0, 1).ToUpper() + strx.Substring(1);
                    restring = Artikel + " " + res_str;
                }
                else
                {
                    restring = Artikel + " " + currentWord.Name;
                }

                strx_name = restring;

                string nmsc = currentWord.NameScore;

                var result = SplitStringByDoubleSpace(nmsc);                
                strx_score = result.part2.Trim();

                /*
                if (!String.IsNullOrEmpty(nmsc) && nmsc.Contains(" "))
                {   
                    char[] charSpl = { ' ' };
                    string[] resx = nmsc.Split(charSpl, StringSplitOptions.RemoveEmptyEntries);   //Substring(nmsc.Length - 2);
                    if (resx.Count() >= 2)
                    {
                        int last = resx.Count() - 1;
                        nmsc = resx[last];
                        strx_score = nmsc;

                        /*
                        //var Sonuc = Common2.ScoreHsb(nmsc, restring, 20);
                        //int xsc = Sonuc.Item1;
                        //score_ = Sonuc.Item2;
                        //restring = Sonuc.Item3;     

                        const int paddLen = 28;

                        if (paddLen > (restring.Length))
                        {
                            restring += nmsc.PadLeft(paddLen - restring.Length);
                        }
                        else
                        {
                            restring = restring.Remove(paddLen - 2);
                            restring += nmsc.PadLeft(paddLen - restring.Length);
                        }
                    }
                }   */

            }
            else
            {
                restring = currentWord.NameScore;

                var result = SplitStringByDoubleSpace(restring);

                strx_name = result.part1.Trim();
                strx_score = result.part2.Trim();
            }

            richTextBox2.Text = strx_name.Trim();   // restring;
            textBox1.Text = strx_score;

        }

        //========================================
        //========================================
        public static (string part1, string part2) SplitStringByDoubleSpace(string input)
        {
            if (string.IsNullOrEmpty(input) || !input.Contains("  "))
            {
                return (string.Empty, string.Empty);
            }

            var parts = input.Split(new[] { "  " }, 2, StringSplitOptions.None);

            if (parts.Length != 2)
            {
                return (string.Empty, string.Empty);
            }

            return (parts[0], parts[1]);
        }

        //========================================
        //========================================
        void DictModePlus()
        {
            Dict dicto = DictMode;

            if (++dicto > Dict.NOTES)
                dicto = Dict.WORDS;

            DictModeChanged(dicto);
        }

        //========================================
        void DictModeMinus()
        {
            Dict dicto = DictMode;
            if (dicto > 0)
                --dicto;
            else
                dicto = Dict.NOTES;

            DictModeChanged(dicto);
        }

        //========================================
        void DictModeChanged(Dict dictX)
        {
            DictMode = dictX;
            boxPositions();
            DictChanged();

        }

        //========================================
        void DictChanged()
        {
            testToolStripMenuItem.Checked = false;
            reverseDictionaryToolStripMenuItem.Checked = false;
            thirdDictionaryToolStripMenuItem.Checked = false;
            structuresReverseToolStripMenuItem.Checked = false;
            sentencesToolStripMenuItem.Checked = false;
            sentencesReverseToolStripMenuItem.Checked = false;
            artikelsToolStripMenuItem.Checked = false;
            notesToolStripMenuItem.Checked = false;

            label3.Text = "";
            richTextBox2.Text = "";

            scoreBox0.Text = "";
            scoreBox1.Text = "";
            scoreBox2.Text = "";
            scoreBox3.Text = "";
            scoreBox4.Text = "";
            scoreBox5.Text = "";
            scoreBox6.Text = "";
            scoreBox7.Text = "";
            scoreBox8.Text = "";
            scoreBox9.Text = "";
            scoreBox10.Text = "";
            scoreBox11.Text = "";
            textBox_score.Text = "";

            
            string label3x = "";

            if (DictMode == Dict.WORDS)
            {                
                testToolStripMenuItem.Checked = true;
                label3x = "WORDS";  //  Dictionary                
            }
            else if (DictMode == Dict.WORDS_REVERSE)
            {
                reverseDictionaryToolStripMenuItem.Checked = true;
                label3x = "WORDS - Reverse";  //  Dictionary
            }
            else if (DictMode == Dict.STRUCTURES)
            {
                thirdDictionaryToolStripMenuItem.Checked = true;
                label3x = "STRUCTURES";  //  Dictionary
            }
            else if (DictMode == Dict.STRUCTURES_REVERSE)
            {
                structuresReverseToolStripMenuItem.Checked = true;
                label3x = "STRUCTURES - Reverse";  //  Dictionary
            }
            else if (DictMode == Dict.SENTENCES)
            {
                sentencesToolStripMenuItem.Checked = true;
                label3x = "SENTENCES";  //  Dictionary
            }
            else if (DictMode == Dict.SENTENCES_REVERSE)
            {
                sentencesReverseToolStripMenuItem.Checked = true;
                label3x = "SENTENCES - Reverse";  //  Dictionary
            }
            else if (DictMode == Dict.ARTIKELS)
            {
                artikelsToolStripMenuItem.Checked = true;
                label3x = "ARTIKELS";  //  Dictionary 
            }
            else if (DictMode == Dict.NOTES)
            {
                notesToolStripMenuItem.Checked = true;
                label3x = "NOTES";   //  Dictionary
            }

            //WR.list_words_orig.Clear();   V7.2.4
            //if ((WR.DictBindingLists[(int)DictMode]) != null)   V7.2.4
            {
                WR.list_words_orig = new BindingList<Word>(WR.DictBindingLists[(int)DictMode]);
            }

            label3.Text = label3x;

            string _msg = WR.DictBindingLists[(int)DictMode].Count.ToString() + " " + label3x;
            UpdateStatus(_msg);

            int xno = WR.listWords.Count();
            string strx = $"TOTAL : {xno} Elements";
            UpdateStatus3(strx);

            modus = Mod.Dictionary;
            sort = Sort.Database;
            SortChecked();

            dictionaryToolStripMenuItem1.Checked = true;
            testToolStripMenuItem1.Checked = false;
            randomTestToolStripMenuItem.Checked = false;
            randomWeightedTestToolStripMenuItem.Checked = false;

            historyToolStripMenuItem.Enabled = false;
            labelToolStripMenuItem.Enabled = false;
            randomScoreToolStripMenuItem.Enabled = false;
            randomScoreWeightedToolStripMenuItem.Enabled = false;
            randomWeightedTestToolStripMenuItem.Enabled = false;

            checkBox1.Visible = true;

            Refresh_ListBox(WR.list_words_orig, 2);

            nSelectedIndex = -1;
            selListBoxFirstItem();

            RefreshSearchList();
        }

        //========================================
        //========================================


        //========================================
        //========================================




    }
}